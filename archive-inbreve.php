<?php
/**
 * Index.
 *
 */
get_header();

$current_page = $paged;
if ($current_page < 2):
    $current_page = 1;
endif;

?>

<section class="archive__body">
    <div class="grid">
        <div class="grid__item large--2-3">
            <div class="grid__box">
			<h1>News dalla politica</h1>
			 <section class="content">
					
					<?php
					if (have_posts()):
                        while (have_posts()): the_post();
					?>
							<article class="comunicato-stampa">
								<div class="title">
									<h2>
										<a href="<?php the_permalink(); ?>"><?php echo $post->post_title;?></a>
									</h2>
								</div>
								<div class="text">
									<?php the_excerpt(); ?>
								</div>
							</article>
					<?php
						endwhile;
						endif;
						
						
						
					?>
					
				</section>
		<br/>	
			<?php 
			$args_pagination = array(
                            'mid_size' => 4,
                            'prev_text' => '<',
                            'next_text' => '>'
                        );
                        echo str_replace('h2', 'p', get_the_posts_pagination($args_pagination));
			?>
			
			
			
			
			
                
            </div>
        </div>
        <div class="grid__item large--1-3">
            <div class="grid__box">
                <?php get_sidebar('home'); ?>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
