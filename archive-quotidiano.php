<?php
/**
 * Index.
 *
 */
get_header();

$current_page = $paged;
if ($current_page < 2):
    $current_page = 1;
endif;

?>

<section class="archive__body">
    <div class="grid">
        <div class="grid__item large--2-3">
            <div class="grid__box">
                <div class="grid">
                    <?php
                    if (!is_user_logged_in()):

                        ?>
                        <div class="grid__item">
                            <h1>Per vedere l'Edicola devi essere abbonato o sostenitore.</h1>
                            <p>
                                Per accedere all'archivio dei quotidiani <a href="<?php echo wp_login_url('/quotidiano/'); ?>">accedi</a> oppure <a href="<?php echo wp_registration_url(); ?>">registrati per poi abbonarti</a>.
                            </p>
                        </div>
                        <?php
                    else:
                        if (((current_user_can('complete_subscriber')|| current_user_can('full_subscriber')) && check_expiration(get_field('expiration_subscription', 'user_' . get_current_user_id()))) || current_user_can('administrator') || current_user_can('editor')):
                            if (have_posts()):
                                while (have_posts()): the_post();

                                    ?>
                                    <div class="grid__item large--1-2">
                                        <div class="grid__box">
                                            <?php get_template_part('parts/listing', 'downloads'); ?>
                                        </div>
                                    </div>
                                    <?php
                                endwhile;

                                $args_pagination = array(
                                    'mid_size' => 4,
                                    'prev_text' => '<',
                                    'next_text' => '>',
                                    //'screen_reader_text' => 'Pagina ' . $current_page . ' di ' . number_format($wp_query->max_num_pages, 0, ',', '.')
                                );
                                echo str_replace('h2', 'p', get_the_posts_pagination($args_pagination));

                            endif;
                        else:

                            ?>
                            <div class="grid__item">
                                <h1>Per vedere il quotidiano devi essere abbonato o sostenitore.</h1>
                                <p>
                                    Per vedere il quotidiano devi avere l'abbonamento completo oppure essere un "sostenitore fedelissimo". Scopri come sostenere il Secolo d'Italia <a href="/sostienici/">cliccando qui</a>.
                                </p>
                            </div>
                        <?php
                        endif;
                    endif;

                    ?>
                </div>
            </div>
        </div>
        <div class="grid__item large--1-3">
            <div class="grid__box">
                <?php get_sidebar('home'); ?>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
