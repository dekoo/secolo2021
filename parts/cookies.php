<div id="cookie-wrapper">
    <div id="cookie-message">

        <h3>Informativa Cookies</h3>
        <p>Questo sito utilizza solo cookies tecnici.<br/>
            Se vuoi saperne di più <strong><a href="#">clicca qui.</a></strong><br />
            Se chiudi questo banner acconsenti all’uso dei cookies.</p>

        <button id="cookie-close" class="btn btn-success ">Chiudi e continua la navigazione</button> 
    </div> 
</div>

<style type="text/css">

    #cookie-wrapper     { 
        position: fixed;
        overflow:hidden;
        display:none; 
        top: 0;
        bottom: 0px;
        left: 0px;
        right: 0px;
        background-color: rgba(0,0,0,0.5);
    }

    #cookie-message     { 
        padding: 20px;
        display:block; 
        width: 100%; 
        background-color: #000; 
        color:#eee; 
        z-index:9999; 
        text-align:left; 
        bottom:0; 
        left:0; 
        font-size: 14px; 

        position: absolute;
    } 

    #cookie-message a {
        color:#fff; 
    }
</style>

<script type="text/javascript">
/*
    jQuery(function ($) {
        if (getCookie("cookie-message-readed") != 1) {
            $("#cookie-wrapper").show();
        }
        $("#cookie-close").on("click", function () {
            setCookie("cookie-message-readed", 1, 365);
            $("#cookie-wrapper").hide();
            $('#modal-vito').modal('show');
        });
    });
*/
</script>