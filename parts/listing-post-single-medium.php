<?php
if (!isset($cpost)):
    global $post;
    $cpost = $post;
endif;
?>
<article>    
    <a href="<?php echo get_the_permalink($cpost); ?>" alt="<?php echo get_the_title($cpost); ?>" aria-label="<?php echo get_the_title($cpost); ?>">
        <?php
        if (has_post_thumbnail($cpost->ID)):
            echo get_the_post_thumbnail($cpost, 'listing-post-home2');
        else:
            ?>
            <img src="/images/post-placeholder.png">
        <?php endif; ?>
    </a>
    <div class="content">
		<div class="date">
            <span class="date"><?php
				if (date('Y') != get_the_date('Y', $cpost->ID)){
					echo get_the_date('j M Y G:i', $cpost->ID );
				} else {
					if ( date('Y-m-d') == get_the_date('Y-m-d') ){
						echo get_the_date('G:i', $cpost->ID );
					} else {
						echo get_the_date('j M G:i', $cpost->ID );
					}
				} ?> - di <strong><?php echo get_post_sign($cpost); ?></strong></span>
        </div>
        <a href="<?php echo get_the_permalink($cpost); ?>" alt="<?php echo get_the_title($cpost); ?>" aria-label="<?php echo get_the_title($cpost); ?>">
            <h3><?php echo get_the_title($cpost); ?></h3>
        </a>
    </div>
</article>    