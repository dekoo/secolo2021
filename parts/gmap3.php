<?php
//https://maps.google.it/?ll=42.512602,13.425293&spn=6.947687,16.907959&t=h&z=7


$args = array(
    'post_type' => get_post_type(),
    'posts_per_page' => -1,
    'post_status' => 'publish'
);


$geolocation = get_query_var("geolocation");
if ($geolocation != "") {



    $tax_query = array(
        array(
            'include_children' => false,
            'taxonomy' => 'geolocation',
            'field' => 'slug',
            'terms' => array( $geolocation )
        )
    );

    $args["tax_query"] = $tax_query;
    


    //echo "<pre>";
    //var_dump($args);
    //die();
}
    
    $stores = get_posts($args);

/*
  $the_query = new WP_Query();
  $the_query->query($args);
  $stores == $the_query->get_posts();

  var_dump($the_query->request);
  //var_dump($stores);
 */

//die();
?>

<div class="space-bottom hide-sm hidden-xs" >

    <div id="storelocator" style="width: 100%; height: 400px;">
    </div>

    <style type="text/css">
        .cluster {
            background-image: url('/images/cluster.png');
            width: 53px;
            height: 52px;
            line-height: 52px;
            text-align: center;
            color: #fff;

        }
    </style>
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?sensor=false">
    </script>
    <script type="text/javascript">

        jQuery(function($) {


            $("#storelocator").gmap3({
                map: {
                    options: {
                        scrollwheel: false,
                        xxcenter: [42.2412406, 13.3060755],
                        zoom: 6,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    }
                },
                marker: {
                    values: [
<?php
foreach ($stores as $store) {


    // campi cstom per coordinate ed altri dati
    $custom_fields = get_post_custom($store->ID);
    //var_dump( $custom_fields);
    //die();


    if ($custom_fields["_pronamic_google_maps_latitude"][0] != "" && $custom_fields["_pronamic_google_maps_longitude"][0] != "") {
        $brand = "";



        /*
         * 
          ["_pronamic_google_maps_latitude"]=>

          ["_pronamic_google_maps_longitude"]=>

          ["_pronamic_google_maps_zoom"]=>

          ["_pronamic_google_maps_title"]=>

          ["_pronamic_google_maps_description"]=>

          ["_pronamic_google_maps_address"]=>
         * 
         */

        /*
          $baloon = "<div style=\"width: 300px; height: 120px; \">" .
          $image .
          addslashes($custom_fields["_pronamic_google_maps_title"][0] . "<br/>" . $custom_fields["_pronamic_google_maps_address"][0]) .
          "<br /><a href=\"" .  get_permalink($store->ID) . "\">vai alla pagina del negozio</a></div>";
         * 
         */

        // $image .
        $baloon = "<div style=\"width: 300px; height: 140px; \">" .
                "<strong>" . addslashes(apply_filters("the_title", $store->post_title)) . "</strong>" .
                "<br/><small>" . $custom_fields["sl_address"][0] . "</small>" .
                "<br/><small>" . $custom_fields["sl_zip"][0] . " " . $custom_fields["sl_city"][0] . "</small>" .
                "<br/><small><i class=\"fa fa-phone\"></i> " . $custom_fields["phone"][0] . "</small>" .
                "<br/><small><i class=\"fa fa-envelope-o\"></i> " . $custom_fields["email"][0] . "</small>" .
                "<br /><a href=\"" . get_permalink($store->ID) . "\"><i class=\"fa fa-hand-o-right\"></i> Vedi scheda Cowo</a>";




        //addslashes(apply_filters("the_content", $store->post_content)) ;
        // "<br /><a href=\"" . get_permalink($store->ID) . "\">vai alla pagina della filiale</a></div>";
        //$icon = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
        $icon = "/images/marker-false.png";

        if ($custom_fields["bookable"][0] == 1) {
            $icon = "/images/marker-true.png";
        }




        echo "{ 
        data: '" . str_replace(array("\r\n", "\r", "\n"), null, $baloon) . "',
        latLng:[ " . $custom_fields["_pronamic_google_maps_latitude"][0] . ", " . $custom_fields["_pronamic_google_maps_longitude"][0] . "],
        options: {
                link: '" . get_permalink($store->ID) . "',
                icon: '" . $icon . "',
                content: '_'
               
            }    },";
    }
};
?>

                    ],
                    events: {// events trigged by markers
                        click: function(marker, event, context) {
                            console.log(marker);
                            console.log(marker.link);
                            //alert( marker );
                        },
                        mouseover: function(marker, event, context) {
                            var map = $(this).gmap3("get"),
                                    infowindow = $(this).gmap3({get: {name: "infowindow"}});
                            if (infowindow) {
                                infowindow.open(map, marker);
                                infowindow.setContent(context.data);
                            } else {
                                $(this).gmap3({
                                    infowindow: {
                                        anchor: marker,
                                        options: {content: context.data}
                                    }
                                });
                            }
                        },
                        mouseout: function() {
                            //var infowindow = $(this).gmap3({get: {name: "infowindow"}});
                            //if (infowindow) {
                            //    infowindow.close();
                            //}
                        }
                    },
                    cluster: {
                        radius: 8,
                        events: {// events trigged by clusters
                            mouseover: function(cluster) {
                                //$(cluster.main.getDOMElement()).css("border", "1px solid red");
                            },
                            mouseout: function(cluster) {
                                //$(cluster.main.getDOMElement()).css("border", "0px");
                            },
                            click: function(marker, event, context) {
                                //console.log( marker )
                                //console.log( context )
                                //console.log ( context.data.latLng )
                                var map = $(this).gmap3("get");
                                map.setZoom(map.getZoom() + 3);

                                latlng = context.data.latLng
                                map.setCenter(latlng);



                            },
                        },
                        5: {
                            content: "<div class='cluster '>CLUSTER_COUNT</div>",
                            width: 10,
                            height: 10
                        },
                        10: {
                            content: "<div class='cluster '>CLUSTER_COUNT</div>",
                            width: 53,
                            height: 52
                        }
                    }
                },
                autofit: {maxZoom: 12},
            });
        });

    </script>


</div>