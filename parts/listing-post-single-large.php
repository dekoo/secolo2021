<?php
if (!isset($cpost)):
    global $post;
    $cpost = $post;
endif;

$classes = 'listing-single-post-large';
if (isset($bordered)):
    $classes .= ' bordered';
endif;

?>
 <div class="grid">
<article class="<?php echo $classes; ?>">
<div class="editorial-title">
    L'EDITORIALE
</div>

<?php
if (!wp_is_mobile()):
?>                   
	<div class="grid">
	
	<div class="grid__item large--1-3 medium--1-3 small--1-3">
		<div class="grid__box"> 
    <a href="<?php echo get_the_permalink($cpost); ?>" alt="<?php echo get_the_title($cpost); ?>" aria-label="<?php echo get_the_title($cpost); ?>">
        <?php
        if (has_post_thumbnail($cpost->ID)):
            echo get_the_post_thumbnail($cpost, 'listing-post-medium');
        else:
            ?>
            <img src="/images/post-placeholder.png">
        <?php endif; ?>
    </a>
	</div>
	</div>
    <div class="grid__item large--2-3 medium--2-3 small--2-3 content">
        <a href="<?php echo get_the_permalink($cpost); ?>" alt="<?php echo get_the_title($cpost); ?>" aria-label="<?php echo get_the_title($cpost); ?>">
            <h3><?php echo get_the_title($cpost); ?></h3>
		</a>
		<?php the_excerpt(); ?>
	</div>
        <div class="author">
            <small>DI <strong><?php echo get_post_sign($cpost); ?></strong></small>
        </div>
    </div>
<?php
else:
?>
	<div class="grid__item large--3-3 medium--3-3 small--3-3">
	<div class="grid">
		<div class="grid__box"> 
    <a href="<?php echo get_the_permalink($cpost); ?>" alt="<?php echo get_the_title($cpost); ?>" aria-label="<?php echo get_the_title($cpost); ?>">
        <?php
        if (has_post_thumbnail($cpost->ID)):
            echo get_the_post_thumbnail($cpost, 'listing-post-medium');
        else:
            ?>
            <img src="/images/post-placeholder.png">
        <?php endif; ?>
    </a>
	</div>
	</div>
    <div class="content">
        <a href="<?php echo get_the_permalink($cpost); ?>" alt="<?php echo get_the_title($cpost); ?>" aria-label="<?php echo get_the_title($cpost); ?>">
            <h3><?php echo get_the_title($cpost); ?></h3>
		</a>
		
	</div>
        <div class="author">
            <small>DI <strong><?php echo get_post_sign($cpost); ?></strong></small>
        </div>
    </div>

<?php		
endif;
?>
    
</article>    
</div>