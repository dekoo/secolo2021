<?php $feedadnkronos = "https://www.adnkronos.com/NewsFeed/Ultimora.xml?username=secoloditalia&password=S3c0l0d!t4l14";
$array = @get_headers($feedadnkronos); 
$string = $array[0];
if(strpos($string, "200")) :
	$adnkronos = simplexml_load_file($feedadnkronos);?>
	<section class="adnKronos-block">
		<img class="logoAdnkronos" src="/images/adnkronos-logo.svg" height="42"/>
		<div class="block-slider">
			<div class="container">
				<div class="slider swiper-container-initialized swiper-container-horizontal">
					<ul class="wrapper" style="transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;">
						<?php $cnt = 0;
						$maxcnt = 12;
						foreach ($adnkronos->channel->item as $item):
							$cnt++;?>
							<li class="article">
								<?php if($item->enclosure['type'] == "image/jpeg" ||  $item->enclosure['type'] == "image/png"){ ?>
									<figure>
										<a href="<?php echo $item->link;?>" target="_blank">
											<img src="<?php echo $item->enclosure['url'];?>"/>
										</a>
									</figure>
								<?php } else { ?>
									<div class="placeholder-image"></div>
								<?php } ?>
								<a href="<?php echo $item->link;?>" target="_blank">
									<h3 class="titolo-blocco-articolo"><?php echo html_entity_decode ($item->title);?></h3>
								</a>
							</li>
							<?php if ( $cnt >= $maxcnt ) :
								break;
							endif;
						endforeach; ?>
					</ul>
					<div class="nav">
						<a href="#" class="prev" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="false">
							<svg viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
								<path d="M15.1698113,18.7616099 C14.8953688,19.0794634 14.4013722,19.0794634 14.0994854,18.7616099 L8.22641509,12.5490196 C7.9245283,12.2311662 7.9245283,11.7399381 8.22641509,11.4220846 L14.0994854,5.23839009 C14.4013722,4.92053664 14.8679245,4.92053664 15.1698113,5.23839009 L15.7735849,5.87409701 C16.0754717,6.19195046 16.0754717,6.68317853 15.7735849,7.00103199 L11.0531732,12 L15.7735849,16.998968 C16.0754717,17.3168215 16.0754717,17.8080495 15.7735849,18.125903 L15.1698113,18.7616099 Z"></path>
							</svg>
						</a>
						<a href="#" class="next" tabindex="0" role="button" aria-label="Next slide" aria-disabled="false">
							<svg viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
								<path d="M9.83018868,18.7616099 L9.22641509,18.125903 C8.9245283,17.8080495 8.9245283,17.3168215 9.22641509,16.998968 L13.9468268,12 L9.22641509,7.00103199 C8.9245283,6.68317853 8.9245283,6.19195046 9.22641509,5.87409701 L9.83018868,5.23839009 C10.1320755,4.92053664 10.5986278,4.92053664 10.9005146,5.23839009 L16.7735849,11.4220846 C17.0754717,11.7399381 17.0754717,12.2311662 16.7735849,12.5490196 L10.9005146,18.7616099 C10.5986278,19.0794634 10.1046312,19.0794634 9.83018868,18.7616099 Z"></path>
							</svg>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
<script>
	jQuery(function() {
		new Swiper('.block-slider .slider', {
			slidesPerView: 'auto',
			wrapperClass: 'wrapper',
			slideClass: 'article',
			navigation: {
				nextEl: '.block-slider .next',
				prevEl: '.block-slider .prev',
			}
		});
	});
</script>