<?php
if(!isset($class)):
    $class = '';
endif;
?>
<section class="social-shares <?php echo $class; ?>">
    <!--<strong class="share-label">Condividi su </strong>-->
    <a class="button button--facebook fb-share-button" href="http://www.facebook.com/sharer/sharer.php?s=100&display=popup&title=<?php echo $title; ?>&u=<?php echo $url; ?>">
        <img src="/wp-content/themes/secoloditalia/resources/img/icon-socials/icon-fb-white.png" width="23" height="23"/>
    </a>
    <a class="button button--twitter" href="https://twitter.com/intent/tweet?text=<?php echo $title; ?>&url=<?php echo $url; ?>">
        <img src="/wp-content/themes/secoloditalia/resources/img/icon-socials/icon-tw-white.png" width="23" height="23"/>
    </a>
    <a class="button button--linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url; ?>&title=<?php echo $title; ?>">
        <img src="/wp-content/themes/secoloditalia/resources/img/icon-socials/icon-in-white.png" width="23" height="23"/>
    </a>
</section>