<article  <?php post_class(""); ?> >

    <?php
    $custom_fields = get_post_custom($location_id);

    /*
      $args = array(
      'alt' => trim(strip_tags(get_the_title())),
      'class' => "img-responsive"
      );

      the_post_thumbnail("full", $args);
     * 
     */
    ?>

    <header class="page-header">
        <h2><?php
    if ($custom_fields["bookable"][0] == 1) {
        //echo "<small><strong>In questa sede è disponibile la prenotazione online</strong></small>";
        echo "<img class='' src='/images/marker-true.png'> ";
    }
    ?><a href="<?php the_permalink(); ?>"><?php the_title(); ?> <i class="fa fa-hand-o-right"></i></a></h2>

            <?php
            $args = array(
                    'orderby' => 'term_group', 
                    'order' => 'ASC'
                    );
            
            $geolocation_terms = wp_get_object_terms($post->ID, 'geolocation', $args);
            $term_links = array();
            
            if ($geolocation_terms) {
                
                    foreach ($geolocation_terms as $term) {
                        $term_links[] = '<a href="' . get_term_link($term, 'product') . '">' . $term->name . '</a>';
                    }
            }
            
            
            ?>
            <small><?php echo implode(", ", $term_links); ?><?php // echo the_terms($post->ID, "geolocation"); ?></small>
    
        
        <br/><small><?php echo $custom_fields["sl_address"][0]; ?></small>
        <br/><small><?php echo $custom_fields["sl_zip"][0] . " " . $custom_fields["sl_city"][0] //the_terms( $post->ID, "citta")?></small>

        <br/><small><i class="fa fa-phone"></i> <a href='tel:<?php echo $custom_fields["phone"][0]; ?>'><?php echo $custom_fields["phone"][0] //the_terms( $post->ID, "citta") ?></a></small>
        <br/><small><i class="fa fa-envelope-o"></i> <a href='mailto:<?php echo $custom_fields["email"][0]; ?>'><?php echo $custom_fields["email"][0] //the_terms( $post->ID, "citta") ?></a></small>


    </header>




</article>