<?php
global $post;
//richiama il filtro per aggiungere i correlati dentro al testo
//add_filter('the_content', 'wpse_ad_content');
/*if (isset($next_post)):
    $post = $next_post;
    setup_postdata($next_post);
endif;*/

/*$title = urlencode(get_the_title());
$url = get_the_permalink();
$actual_post_id = $post->ID;

if (!isset($_GET['noadv'])):
if (isset($next_post)):
    echo '<div class="banner text-center">';
    get_template_part('parts/banner/masthead');
    echo '</div>';
endif;
endif;*/
?>

<div class="grid__item large--2-3">
    <div class="grid__box principal__box">
        <article>
            <section class="body">
                <h1><?php the_title(); ?></h1>
				<?php
                get_template_part_parameterized('parts/post', 'share', array('title' => $title, 'url' => $url, 'class' => 'hidden-desktop'));
				?>
                <section class="content">
                    <?php the_content(); ?>
                </section>
				<?php
                get_template_part_parameterized('parts/post', 'share', array('title' => $title, 'url' => $url));
				if ((!current_user_can('full_subscriber')) AND (!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
				get_template_part('parts/banner/after-post');
                endif;
				?>
            </section>
        </article>		
	</div>
</div>
<?php if (!wp_is_mobile()): ?>
    <div class="grid__item large--1-3">
        <div class="grid__box">
            <?php
            if (!isset($sidebar_name)):
                $sidebar_name = 'post-small';
            endif;
            get_sidebar($sidebar_name);

            ?>
        </div>
    </div>
    <?php
endif;


?>