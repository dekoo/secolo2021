<?php
$ff = new FormFields();
global $wp_query;

$term = get_term_by('slug', get_query_var("geolocation"), 'geolocation');

if ($term) {

    if ($term->parent == 0) {
        $regione = $term;
        $provincia = "";
    } else {
        $term_parent = get_term_by("term_id", $term->parent, 'geolocation');

        $regione = $term_parent;
        $provincia = $term;
    }

    //echo "<pre>";
    //var_dump( $term , $term_parent );
}
  
?>
<div class="row ">
    <div class="<?php echo (is_tax("geolocation") ? "col-sm-6" : "col-sm-12" ); ?>">
        <strong>Scegli Regione </strong>
        <?php
        $args = array(
            "parent" => 0,
            "hide_empty" => 1,
            "orderby" => "slug",
        );

        $data = get_terms("geolocation", $args);

//$select = ( "" != get_query_var("regione") ) ? get_query_var("regione") : get_query_var("provincia");
//$select = get_query_var("geolocation");
        $select = $regione->slug;

        /*
          if ($wp_query->queried_object->parent > 0) {
          $term = get_term($wp_query->queried_object->parent, "geolocation");
          $select = $term->slug;
          }
         */
        
        
         $attr = array(
                    "class" => "form-control changelocation",
                    "placeholder" => "Scegli la regione",
                );

        echo $ff->combo_terms_slug("regione", $data, $select, $attr);
        ?>
    </div>

    <?php if (is_tax("geolocation")): ?>

        <div class="col-sm-6 ">
            <strong>Scegli Provincia </strong>
            <?php
            $args = array(
                "parent" => $regione->term_id,
                "hide_empty" => 1,
                "orderby" => "slug",
            );

            $data = get_terms("geolocation", $args);

            //$select = ( "" != get_query_var("regione") ) ? get_query_var("regione") : get_query_var("provincia");
            //$select = get_query_var("geolocation");
            $select = $provincia->slug;

            /*
              if ($wp_query->queried_object->parent > 0) {
              $term = get_term($wp_query->queried_object->parent, "geolocation");
              $select = $term->slug;
              }
             */

            $attr = array(
                    "class" => "form-control changelocation",
                    "placeholder" => "Scegli la provincia",
                );
            echo $ff->combo_terms_slug("provincia", $data, $select, $attr);
            ?>
        </div>

    <?php endif; ?>
</div>

