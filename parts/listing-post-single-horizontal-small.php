<?php
if (!isset($cpost)):
    global $post;
    $cpost = $post;
endif;

?>
<article class="listing-single-post-horizontal-small">
    <a href="<?php echo get_the_permalink($cpost); ?>">
        <h4><?php echo get_the_title($cpost); ?></h4>
    </a>
    <div class="date not-absolute">
        <?php echo get_the_time('l j F h:i', $cpost->ID); ?> - <small>DI <strong><?php echo get_post_sign($cpost); ?></strong></small>
    </div>
    <hr>
</article>    