<?php
if (!isset($cpost)):
    global $post;
    $cpost = $post;
endif;

?>
<article class="listing-single-download">    
    <a href="<?php echo get_the_permalink($cpost); ?>">
        <h5>Edizione del <?php the_title(); ?></h5>
        <?php echo get_the_post_thumbnail($cpost, 'full'); ?>
        <div><span>SCARICA</span></div>
    </a>
</article>    