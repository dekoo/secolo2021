<?php
global $post;
?>

<div class="grid__item large--2-3">
    <div class="grid__box principal__box">
		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php
			if ( !dek_secolo_app() ):
				if(function_exists('bcn_display'))
				{bcn_display();}
			endif;
			?>
		</div>
        <article>
            <section class="body">
                <h1><?php the_title(); ?></h1>
                <div class="date">
				<?php $sign=get_post_sign($post);?>
                    <span><?php	echo get_the_date('j M Y G:i');?>	- di <strong><?php echo $sign; ?></strong></span>
                </div>
                <?php
					if ( !dek_secolo_app() ):
						get_template_part_parameterized('parts/post', 'share', array('title' => $title, 'url' => $url, 'class' => 'hidden-desktop'));
					endif;
					if (has_post_thumbnail()):
						the_post_thumbnail('listing-post-medium', array('class' => 'single-thumbnail'));
					endif;
				?>
                <section class="content">
                    <?php the_content(); ?>
                </section>
                <?php
                if ( !dek_secolo_app() ):
					get_template_part_parameterized('parts/post', 'share', array('title' => $title, 'url' => $url, 'class' => 'hidden-mobile'));
									
					if ((!current_user_can('full_subscriber')) AND (!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
					get_template_part('parts/banner/after-post');
					endif;

					$show_comments = true;
					if (has_shortcode(get_the_content(), 'premium') && (!is_user_logged_in() || (!check_expiration(get_field('expiration_subscription', 'user_' . get_current_user_id()))) && !current_user_can('administrator'))):
						$show_comments = false;
					endif;

					$newsletter = filter_input(INPUT_GET, 'newsletter');

					if (!$show_comments && !empty($newsletter)):
						$fields = get_fields($post->ID);

						if ($newsletter == $fields['newsletter_code']):
							$show_comments = true;
						endif;
					endif;

					if ($show_comments):
						comments_template();
					endif;
				endif;
				?>
			<!-- Articoli correlati -->
				<?php
					$posts = get_field('articoli_correlati');
					if ($posts) :
				?>		
				<div class="correlati">
					<h4>ARTICOLI CORRELATI</h4>
					<div class="grid">
					<?php if( have_rows('articoli_correlati') ): ?>
						<?php while( have_rows('articoli_correlati') ): the_row();?>
							<?php
								$url = get_sub_field('url_correlato');
								$post_id = url_to_postid( $url );
								$getitle = get_sub_field('titolo');
								if (!$getitle) $getitle = get_post_field( 'post_title', $post_id, true );
								$autore = get_sub_field('autore');
								if (!$autore) $autore = get_post_field( 'sign', $post_id, true );
								if (!$autore) $autore = $sign;
							?>
							<div class="grid__item large--1-2 medium--1-2 small--1-2">
								<div class="grid__box">
									<article class="listing-single-post-medium">
										<a href="<?php echo $url; ?>">
											<?php echo get_the_post_thumbnail($post_id, 'small', $attr); ?>
										</a>
										<div class="content">
											<div class="date">
												<span><?php	echo get_the_date('j M Y G:i', $post_id ); ?> - di <strong><?php echo $autore; ?></strong></span>
											</div>
											<a href="<?php echo $url; ?>?utm_source=content&utm_medium=related&utm_campaign=bottom">
												<h3><?php echo $getitle; ?></h3>
											</a>

										</div>
									</article>
								</div>
							</div>					
						<?php endwhile;
					endif;?>
					</div>
				</div>
				<?php endif; ?>
				<!-- FINE Articoli correlati -->	
            </section>
		</article>
		<?php 
			if ((!current_user_can('full_subscriber')) AND (!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) : ?>
				<?php get_template_part('parts/banner/recommendation');
			endif;
		?>
	</div>
</div>
<?php if (!wp_is_mobile()): ?>
    <div class="grid__item large--1-3">
        <div class="grid__box">
			<aside>
				<?php if (!isset($sidebar_name)):
					$sidebar_name = 'post-small';
				endif;
				get_sidebar($sidebar_name); ?>
			</aside>
        </div>
    </div>
<?php endif; ?>