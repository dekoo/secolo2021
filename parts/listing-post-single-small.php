<?php
if (!isset($cpost)):
    global $post;
    $cpost = $post;
endif;

?>
<article class="listing-single-post-small">  
	<div>  
		<a href="<?php echo get_the_permalink($cpost); ?>" alt="<?php echo get_the_title($cpost); ?>" aria-label="<?php echo get_the_title($cpost); ?>">
			<?php
			if (has_post_thumbnail($cpost)):
				echo get_the_post_thumbnail($cpost, 'listing-post-small');
			else: ?>
				<img src="/images/post-placeholder.png">
			<?php endif; ?>
			<div class="content">
				<div class="date">
					<span class="date"><?php if ( date('Y-m-d') == get_the_date('Y-m-d') ) { echo get_the_date('G:i'); } else { echo get_the_date('j M G:i'); } ?> - di <strong><?php echo get_post_sign($cpost); ?></strong></span>
				</div>
				<h3><?php echo get_the_title($cpost); ?></h3>	
			</div>
		</a>
	</div>
</article>    