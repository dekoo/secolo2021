<?php
if (!isset($cpost)):
    global $post;
    $cpost = $post;
endif;

?>
<article class="listing-single-post-horizontal">
    <div class="grid">
        <div class="grid__item large--1-3 medium--1-3 small--1-3">
            <div class="grid__box">
                <a href="<?php echo get_the_permalink($cpost); ?>">
                    <?php
                    if (has_post_thumbnail($cpost)):
                        echo get_the_post_thumbnail($cpost, 'listing-post-small');
                    else:

                        ?>
                        <img src="/images/post-placeholder.png">
                    <?php endif; ?>
                </a>
            </div>
        </div>
        <div class="grid__item large--2-3 medium--2-3 small--2-3">
            <div class="grid__box">
                <a href="<?php echo get_the_permalink($cpost); ?>">
                    <h4><?php echo get_the_title($cpost); ?></h4>
                </a>
            </div>
        </div>
    </div>
    <div class="date">
        <small>DI <strong><?php echo get_post_sign($cpost); ?></strong></small>
    </div>
    <hr>
</article>    