<article <?php post_class(""); ?>   >

    <?php
    $args = array(
        'alt' => trim(strip_tags(get_the_title())),
        'class' => "img-responsive"
    );

    the_post_thumbnail("full", $args);
    ?>
    <header class="page-header">
        <h1><?php the_title(); ?></h1>
        <small><?php echo get_the_date("j F Y"); ?> / in <?php echo get_the_term_list( $post->ID, "category" ); ?></small>
        
    </header>
    <div class="entry-content">
        <?php the_content() ?>
    </div>
    
</article>