<!-- Google tag (gtag.js)
<script async src="https://www.googletagmanager.com/gtag/js?id=G-76H3GPG7K4"></script>
 -->
<script>
__tcfapi('addEventListener', 2, function(tcData, success) {
    if (success) {
        if (tcData.eventStatus == 'useractioncomplete' || tcData.eventStatus == 'tcloaded') {
            var hasStoreOnDeviceConsent = tcData.purpose.consents[1] || false;

            if (hasStoreOnDeviceConsent) {
                var adsbygoogle_script = document.createElement('script');
                adsbygoogle_script.src = 'https://www.googletagmanager.com/gtag/js?id=G-76H3GPG7K4';
                document.head.appendChild(adsbygoogle_script);
				
				 window.dataLayer = window.dataLayer || [];
				function gtag(){dataLayer.push(arguments);}
				gtag('js', new Date());

				gtag('config', 'G-76H3GPG7K4');
            }
        }
    }
});
</script>



