<?php
if (!isset($cpost)):
    global $post;
    $cpost = $post;
endif;

?>
<article class="listing-single-post">
    <a href="<?php echo get_the_permalink($cpost); ?>" alt="<?php echo get_the_title($cpost); ?>" aria-label="<?php echo get_the_title($cpost); ?>">
        <?php echo get_the_post_thumbnail($cpost, 'full'); ?>
	</a>
    <div class="content">
		<div class="date">
			<span><?php if ( date('Y-m-d') == get_the_date('Y-m-d') ) { echo get_the_date('G:i'); } else { echo get_the_date('j M G:i'); } ?> - di <strong><?php echo get_post_sign($cpost); ?></strong></span>
		</div>
        <a href="<?php echo get_the_permalink($cpost); ?>" alt="<?php echo get_the_title($cpost); ?>" aria-label="<?php echo get_the_title($cpost); ?>">
			<h1><?php echo get_the_title($cpost); ?></h1>
		</a>
    </div>
</article>  