<article <?php post_class(""); ?>>
    <div class="grid">
        <div class="grid__item large--1-3">
            <div class="grid__box">
                <a href="<?php the_permalink(); ?>">
                    <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'listing-post-medium'); ?>" class="img-responsive">
                </a>
            </div>
        </div>
        <div class="grid__item large--2-3">
            <div class="grid__box">
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <div class="entry-content">
                    <?php the_excerpt(); ?>
                </div>
                <div class="date not-absolute">
                    <small><?php echo get_the_time('l j F h:i', $cpost->ID); ?> - <small>DI <strong><?php echo get_post_sign($cpost); ?></strong></small></small>
                </div>
            </div>
        </div>
        <div class="grid__item">
            <div class="grid__box">
                <hr>
            </div>
        </div>
    </div>
</article>