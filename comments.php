<?php
$args = array('fields' => array(
'author' => '<p class="comment-form-author"><input id="author" name="author" type="text" placeholder="Nome*" size="30" required /></p>',
 'email' => '<p class="comment-form-email"><input id="email" name="email" type="email" placeholder="Email*" size="30" required /></p>',
 'url' => false,
));

echo '<a href="#" class="show-comment-form" data-form-id="comment-form-wrapper-' . $post->ID . '">Commenti <i class="fa fa-chevron-down"></i><i class="fa fa-chevron-up"></i></a>';

echo '<div id="comment-form-wrapper-' . $post->ID . '" class="comment-form-wrapper">';
comment_form($args);

if (have_comments()):

?>

<div class="comment-list">
    <?php
    wp_list_comments(array( 'callback' => 'ps_comments_callback' ));

    ?>
</div>

<?php comment_nav(); ?>

<?php
endif;
comment_nav();

echo '</div>';