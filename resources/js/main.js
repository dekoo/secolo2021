jQuery(function ($) {
 
    function setCookie(c_name, value, exdays)
    {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString()) + "; path=/";
        document.cookie = c_name + "=" + c_value;
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i].trim();
            if (c.indexOf(name) == 0)
                return c.substring(name.length, c.length);
        }
        return "";
    }

    $('.social-shares .button').on('click', function (e) {
        e.preventDefault();
        window.open($(this).attr('href'), 'sharer', 'toolbar=0,status=0,width=548,height=325');
    });

  //  $('.video').fitVids();

    $("body.single").on('click', ".show-comment-form", function (e) {
        e.preventDefault();
        var form_id = $(this).attr('data-form-id');
        $(this).toggleClass('active');
        $("#" + form_id).slideToggle("slow", function () {
        });
    });

    $(document).on('submit', '.comment-form', function (e) {
        var form = document.getElementById($(this).attr('id'));
        var isValidForm = form.checkValidity();
        if (!isValidForm) {
            alert('Alcuni campi sono incompleti o non corretti. Si prega di ricontrollare.');
            return false;
        }
    });
	$('#all').on('click', function(e) {
	e.preventDefault();
	document.getElementById('category_description').style.height = "auto";
	document.getElementById('all').style.display = "none";
	document.getElementById('chiudi').style.display = "block";
	});
	$('#chiudi').on('click', function(e) {
	e.preventDefault();
	document.getElementById('category_description').style.height = "80px";
	document.getElementById('all').style.display = "block";
	document.getElementById('chiudi').style.display = "none";
	});
	$(".navbar-toggle").on('click', function(e) {
		if( !$(".collapse").hasClass("in") ){
			$(".navbar-toggle").addClass("in");
		} else {
			$(".navbar-toggle").removeClass("in");
		}
	});
	if ( window.location.pathname == '/' ){
		setTimeout(() => {
			document.location.replace("https://www.secoloditalia.it/?refresh_cens");
		}, 180000);
	};
});