<?php
/*
if(!current_user_can('administrator')) { 
    header("HTTP/1.1 301 Moved Permanently");
	header("Location: /ultime-notizie/");
} 
*/
/**
 * Index.
 *
 */
get_header();


$current_page = $paged;
if ($current_page < 2):
    $current_page = 1;
endif;

?>
<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
    <?php
    if(function_exists('bcn_display'))
    {
            bcn_display();
    }?>
		</div>
		<section class="archive__body">
			<div class="grid">
				<div class="grid__item large--2-3">
					<div class="grid__box">
					<div class="grid">
					 <?php
							
						
						// Define custom query parameters
						$custom_query_args = array( 
						'post_type' => 'post',
						//'cat' => '-'.get_id_archivio().'',
						//'author' => get_the_author_meta('ID'),
						'meta_key'		=> 'sign',
						'meta_value'	=> 'Hoara Borselli',
						'nopaging'		=> 'true',
						'posts_per_page'	=> 20
						);

						echo"<p>Sto filtrando i contenuto per firma <strong> Hoara Borselli </strong><br/>";
						

						// Get current page and append to custom query parameters array
						//$custom_query_args['paged'] = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

						// Instantiate custom query
						$new_loop = new WP_Query( $custom_query_args );
						echo "Ho trovato ".$new_loop->found_posts." articoli";
					
						if ( $new_loop->have_posts() ) :
						while ( $new_loop->have_posts() ) : $new_loop->the_post(); 

                            ?>
                            <div class="grid__item large--1-2">
                                <div class="grid__box">
                                    <?php get_template_part('parts/listing-post', 'single-medium'); ?>
                                </div>
                            </div>
						
                            <?php
                        endwhile;
/*
                        $args_pagination = array(
                            'mid_size' => 4,
                            'prev_text' => '<',
                            'next_text' => '>',
                            //'screen_reader_text' => 'Pagina ' . $current_page . ' di ' . number_format($wp_query->max_num_pages, 0, ',', '.')
                        );
                        echo str_replace('h2', 'p', get_the_posts_pagination($args_pagination));
*/
                    endif;

                    ?>
					</div>
                </div>
            </div>
        
        <div class="grid__item large--1-3">
            <div class="grid__box">
                <?php get_sidebar('home'); ?>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
