<?php
/**
 * 
 * Template Name: Vetrina
 *
 */
get_header();

$page = explode("-",$_SERVER['REQUEST_URI']);
$nr_page = trim($page[count($page)-1],'/');
$nr_offset= $nr_page-1;



	$args = array(
		'post_type' => 'post',
        'post_status' => 'publish',
        'showposts' => 1,
		'offset'    => $nr_offset,
        'tax_query' => array(
            array(
                'taxonomy' => 'posizione-editoriale',
                'field' => 'slug',
                'terms' => 'homepage-primi-5',
            ),
        )
    );
	$query = new WP_Query($args);
	if ($query->have_posts()):
		while ($query->have_posts()): $query->the_post();?>
			<section class="post__body">
				<div class="grid">
					<div class="grid__item__wrapper">
						<div class="grid__item large--2-3 pagina-vetrina">
							<div class="banner-qrcode-home">
								<img src="/images/secolo-banner-968.png" style="width:100%;"/>
							</div>
							<div class="grid__box">
								<article>
									<?php if (has_post_thumbnail()):
										the_post_thumbnail('listing-post-medium', array('class' => 'single-thumbnail'));
									endif; ?>
									<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/" style="margin-top:15px;">
										<?php
										if ( !dek_secolo_app() ):
											if(function_exists('bcn_display'))
											{bcn_display();}
										endif;
										?>
									</div>
									<h1 style="margin:0px;"><?php the_title(); ?></h1>
									<div class="date">
										<?php $sign=get_post_sign($post);?>
											<span><?php
												if ( date('Y') != get_the_date('Y') ) {
													echo get_the_date('j M Y G:i');
												}else {
													if ( date('Y-m-d') == get_the_date('Y-m-d') ){
														echo get_the_date('G:i');
													} else {
														echo get_the_date('j M G:i');
													}
												} ?>
												- di <strong><?php echo $sign; ?></strong></span>
									</div>
									<section class="content ">
										<?php the_content(); ?>
									</section>
								</article>
							</div>
						</div>
						<div class="grid__item large--1-3">
							<div class="grid__box">
								<aside>
									<?php get_sidebar('post'); ?>
								</aside>
							</div>
						</div>
					</div>
				</div>
			</section>
		<?php endwhile;
	endif;