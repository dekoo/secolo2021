<?php
/**
 * 
 * Template Name: No Sidebar
 *
 */
get_header();

if (have_posts()):
    while (have_posts()): the_post();

        ?>

        <section class="post__body">
            <div class="grid">
                <div class="grid__item">
                    <div class="grid__box">
                        <h1><?php the_title(); ?></h1>
                        <section class="content">
                            <?php the_content(); ?>
                        </section>
                    </div>
                </div>
            </div>
        </section>

        <?php
    endwhile;
endif;
get_footer();
