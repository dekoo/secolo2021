<?php
/**
 * Template Name: Già Abbonato
 *
 */
get_header();

if (have_posts()):
    while (have_posts()): the_post();
        $type = '';
        switch (wp_get_current_user()->roles[0]):
            case 'web_subscriber':
                $type = 'Web';
                break;
            case 'complete_subscriber':
                $type = 'Completo';
                break;
        endswitch;

        $expiration = date_i18n('j F Y', strtotime(get_field('expiration_subscription', 'user_' . get_current_user_id())));

        ?>

        <section class="post__body">
            <div class="grid">
                <div class="grid__item large--2-3">
                    <div class="grid__box">
                        <h1>Sei già abbonato</h1>
                        <h3>Il tuo abbonamento <?php echo $type; ?> scade il <?php echo $expiration; ?></h3>
                        <section class="content">
                            <?php the_content(); ?>
                        </section>
                    </div>
                </div>
                <div class="grid__item large--1-3">
                    <div class="grid__box">
                        <?php get_sidebar('home'); ?>
                    </div>
                </div>
            </div>
        </section>

        <?php
    endwhile;
endif;
get_footer();
