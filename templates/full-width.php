<?php
/**
 * 
 * Template Name: Full Width
 *
 */
get_header();

?>


<div class="container">

    <div class="row ">
        <div class="col-sm-12">
            <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                    <?php get_template_part('parts/content', get_post_type()); ?>
                <?php endwhile; ?>

            <?php else : ?>
            <?php endif; ?>

        </div>


    </div>

</div>

<?php
get_footer();
