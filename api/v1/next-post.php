<?php
$actual_post_id = filter_input(INPUT_POST, 'actual');

$return = array(
    'status' => 'error',
    'message' => '',
    'data' => array()
);

if (isset($actual_post_id) && $actual_post_id !== FALSE):
    $args = array(
        'p' => $actual_post_id
    );
    $query = new WP_Query($args);

    if ($query->have_posts()):
        while ($query->have_posts()): $query->the_post();
            $next_post = get_previous_post();
            $return['data']['new'] = $post->ID;
        endwhile;
    endif;

    if (!empty($next_post)):
        /*
        if (function_exists('ai_content_hook')):
            add_filter('the_content', 'ai_content_hook', 10);
        endif;
        */
        ob_start();
        get_template_part_parameterized('parts/single', '', array('next_post' => $next_post));
        $return['data']['html'] = ob_get_clean();
        $return['data']['url'] = get_the_permalink($next_post);
        $return['data']['post_id'] = $next_post;
        $return['status'] = 'success';
    else:
        $return['message'] = 'No more posts';
    endif;
else:
    switch ($query_string):
        case FALSE:
            $return['message'] = 'Wrong params';
            break;
        case NULL:
            $return['message'] = 'Missing params';
            break;
    endswitch;
endif;

wp_send_json($return);
