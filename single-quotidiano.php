<?php
/**
 * Index.
 *
 */
get_header();

if (have_posts()):
    while (have_posts()): the_post();

        ?>

        <section class="post__body">
            <div class="grid">
                <div class="grid__item large--2-3">
                    <div class="grid__box principal__box">
                        <?php
                        if (!is_user_logged_in()):

                            ?>
                            <h1>Per vedere l'Edicola devi essere registrato.</h1>
                            <p>
                                Per vedere l'Edicola <a href="<?php echo wp_login_url(get_the_permalink()); ?>">accedi</a> oppure <a href="<?php echo wp_registration_url(); ?>">registrati</a>.
                            </p>
                            <?php
                        else:
                            if (((current_user_can('complete_subscriber')|| current_user_can('full_subscriber'))  && check_expiration(get_field('expiration_subscription', 'user_' . get_current_user_id()))) || current_user_can('administrator') || current_user_can('editor')):

                                ?>
                                <article>
                                    <section class="body">
                                        <h1><?php the_title(); ?></h1>
                                        <?php
                                        if (has_post_thumbnail()):
                                            the_post_thumbnail('full', array('class' => 'single-thumbnail'));
                                        endif;

                                        foreach (get_attached_media('application/pdf') as $file_id => $file):

                                            ?>
                                            <h3><a href="<?php echo wp_get_attachment_url($file_id); ?>">Scarica</a></h3>
                                            <?php
                                        endforeach;

                                        ?>
                                    </section>
                                </article>
                            <?php else: ?>
                                <h1>Per vedere l'Edicola devi essere abbonato.</h1>
                                <p>
                                    Per vedere l'Edicola devi avere l'abbonamento completo. Puoi abbonarti <a href="/abbonamenti/">cliccando qui</a>.
                                </p>
                            <?php
                            endif;
                        endif;

                        ?>
                    </div>
                </div>
                <div class="grid__item large--1-3">
                    <div class="grid__box">
                        <?php get_sidebar('home'); ?>
                    </div>
                </div>
            </div>
        </section>

        <?php
    endwhile;
endif;
get_footer();
