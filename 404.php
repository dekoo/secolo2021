<?php
/**
 * Index.
 *
 */
get_header();

?>


<section class="post__body">
    <div class="grid">
        <div class="grid__item large--2-3">
            <div class="grid__box">
                <h1>Pagina non trovata</h1>
				<section class="content">
                <p>
                    Siamo spiacenti, la pagina cercata non esiste oppure è stata rimossa
                </p>
				</section>
            </div>
		</div>
            <div class="grid__item large--1-3">
                <div class="grid__box">
                    <?php get_sidebar('home'); ?>
                </div>
            </div>
        </div>
</section>

<?php
get_footer();
