<?php
/**
 * Index.
 *
 */
get_header();

$fields = get_fields();

?>

<section class="home__head-top">
<?php
/*
$args = array(
	'post_type' => 'post',
	'post_status' => 'publish',
	'meta_key'		=> 'ultimora',
	'meta_value'	=> '1',
	'orderby' => 'date',
	'order' => 'DESC',
	'posts_per_page' => '1'
);
$query = new WP_Query($args);
if ($query->have_posts()):?>
	<div class="wrapper-ultimora">
		<div class="inner-ultimora">
			<span class="et-live">Ultim'ora</span>
			<?php while ($query->have_posts()): $query->the_post();?>
				<div>
					<span class="date"><?php the_modified_date('H:i'); ?></span>
					<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				</div>
			<?php endwhile;?>
		</div>
	</div>
<?php endif;
wp_reset_query();
*/
?>
<div class="grid">
	<div class="grid__item large--2-3">
		
		
			<div class="grid__box">
				<div class="grid">
					<?php // $transient = get_transient('si_homepage-primi-5');
					// if (!$transient):
						//ob_start();
						 $args = array(
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'showposts' => 5,
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'posizione-editoriale',
                                    'field' => 'slug',
                                    'terms' => 'homepage-primi-5',
                                ),
                            )
                        );
						$query = new WP_Query($args);

							if ($query->have_posts()):
								$first = true;
								while ($query->have_posts()): $query->the_post();
									if ($first):

										?>
										<div class="grid__item">
											<div class="grid__box">
												<?php get_template_part_parameterized('parts/listing-post', 'single', array('cpost' => $row['post'])); ?>
											</div>
										</div>
										<?php
										$first = false;
									else:

										?>
										<div class="grid__item large--1-2 medium--1-2 small--1-2">
											<div class="grid__box">
												<?php get_template_part_parameterized('parts/listing-post', 'single-medium', array('cpost' => $row['post'])); ?>
											</div>
										</div>
									<?php
									endif;
								endwhile;
							endif;

							wp_reset_query();

							// $content_transient = ob_get_flush();
							// set_transient('si_homepage-primi-5', json_encode($content_transient), get_timeout_transient());
						// else:
							// echo json_decode($transient);
						//endif; ?>
				</div>
			</div>
        
    </div>
    <div class="grid__item large--1-3">
	    <div class="grid__box">
            <div class="banner">
				
                    <?php
					if ((!current_user_can('full_subscriber')) AND (!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
						if (!wp_is_mobile()):
							get_template_part('parts/banner/top-right');
						else:
                        get_template_part('parts/banner/top-mobile');
						endif;
					endif
                    ?>
                </div>
            </div>
        </div>
    </div>
    <hr>
</section>
<!--<section class="home__head-editorial">
   
        <?php
      
            ob_start();
            $args = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'showposts' => 1,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'posizione-editoriale',
                        'field' => 'slug',
                        'terms' => 'editoriale-direttore',
                    ),
                )
            );

            $query = new WP_Query($args);

            if ($query->have_posts()):
                while ($query->have_posts()): $query->the_post();

                    ?>
                    <div class="grid__item large--3-3">
                        <div class="grid__box">
                            <?php get_template_part_parameterized('parts/listing-post', 'single-large', array('cpost' => $row['post'])); ?>
                        </div>
                    </div>
                    <?php
                endwhile;
            endif;

            wp_reset_query();


        ?>
 
</section>-->

<section class="home__head-bottom">
    <div class="grid">
		<div class="grid__item">
			<?php $transient = get_transient('si_homepage-secondi-8');
			if (!$transient):
				ob_start();
				$args = array(
					'post_type' => 'post',
					'post_status' => 'publish',
					'showposts' => 8,
					'tax_query' => array(
						array(
							'taxonomy' => 'posizione-editoriale',
							'field' => 'slug',
							'terms' => 'homepage-secondi-8',
						),
					)
				);

				$query = new WP_Query($args);

				if ($query->have_posts()):
					while ($query->have_posts()): $query->the_post();

						?>
						<div class="grid__item large--1-2">
							<div class="grid__box">
								<?php get_template_part_parameterized('parts/listing-post', 'single-medium', array('cpost' => $row['post'])); ?>
							</div>
						</div>
						<?php
					endwhile;
				endif;

				wp_reset_query();

				$content_transient = ob_get_flush();
				set_transient('si_homepage-secondi-4', json_encode($content_transient), get_timeout_transient());
			else:
				echo json_decode($transient);
			endif;

			if (wp_is_mobile()):

				?>
				<div class="grid__item">
					<div class="grid__box">
						<div class="banner">
						<?php
							if ((!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
								get_template_part('parts/banner/middle-mobile');
							endif;
						?>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
		<hr>
	</div>
</section>
<section class="home__body">
    <div class="grid">
		<div class="grid__item">
			<div class="grid__item large--2-3">
				<div class="grid__box">
					<div class="grid">
						<?php
						$transient = get_transient('si_homepage-homepage-secondari');
						if (!$transient):
							ob_start();
							$args = array(
								'post_type' => 'post',
								'post_status' => 'publish',
								'showposts' => 42,
								'tax_query' => array(
									array(
										'taxonomy' => 'posizione-editoriale',
										'field' => 'slug',
										'terms' => 'homepage-secondari',
									)
								)
							);

							$query = new WP_Query($args);

							if ($query->have_posts()):
								$i = 1;
								while ($query->have_posts()): $query->the_post();
									switch ($i):
										case 1:
										case 2:
										case 3:
										case 4:
										case 5:
										case 6:
										case 7:
										case 8:
										case 9:
										case 10:
										case 11:
										case 12:
										case 13:
										case 14:
										case 15:
										case 16:
										case 17:
										case 18:

											?>
											<div class="grid__item large--1-2 medium--1-2 small--1-2">
												<div class="grid__box">
													<?php get_template_part_parameterized('parts/listing-post', 'single-medium', array('bordered' => true)); ?>
												</div>
											</div>
											<?php
											if ($i == 18 && wp_is_mobile()):

												?>
												<div class="grid__item">
													<div class="grid__box">
														<div class="banner">
														<?php
															if ((!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
																get_template_part('parts/banner/bottom-mobile');
															endif;	
														?>
														</div>
													</div>
												</div>
												<?php
											endif;
											break;
										case 19:

											?>
											<div class="grid__item">
												<div class="grid grid-no-padding">
													<div class="grid__item large--2-3">
														<div class="grid__box">
															<div class="grid grid-no-padding">
															<?php
															case 20:
															case 21:
															case 22:
															case 23:
															case 24:
															case 25:
															case 26:
															case 27:
															case 28:
															case 29:
															case 30:
															case 31:
															case 32:
															case 33:
															case 34:
															case 35:

																?>
																<div class="grid__item">
																	<div class="grid__box">
																		<?php get_template_part('parts/listing-post', 'single-horizontal'); ?>
																	</div>
																</div>
																<?php
																break;
															case 36:

																?>
															</div>
														</div>
													</div>
													<div class="grid__item large--1-3">
														<div class="grid__box">
															<?php get_sidebar('home-secondary'); ?>
														</div>
													</div>
												</div>
											</div>
										<?php
										case 37:
										case 38:
										case 39:
										case 40:
										case 41:
										case 42:

											?>
											<div class="grid__item">
												<div class="grid__box">
													<?php get_template_part('parts/listing-post', 'single-horizontal-small'); ?>
												</div>
											</div>
											<?php
											break;
									endswitch;
									$i++;
								endwhile;
							endif;

							wp_reset_query();

							$content_transient = ob_get_flush();
							set_transient('si_homepage-homepage-secondari', json_encode($content_transient), get_timeout_transient());
						else:
							echo json_decode($transient);
						endif;

						?>
					</div>
				</div>
			</div>

        <div class="grid__item large--1-3">
            <div class="grid__box">
                <section class="home__sidebar">
                    <?php get_sidebar('home'); ?>
                </section>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
