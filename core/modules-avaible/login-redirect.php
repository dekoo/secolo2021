<?php

/**
 * 
 * redirect to homepage after login
 */
function login_redirect($redirect_to, $request, $user) {

    if (!current_user_can('edit_posts')) {
        return HOMEURL;
    } else {
        return $redirect_to;
    }
}

//add_filter("login_redirect", "login_redirect", 10, 3);

/**
 * 
 * redirect to homepage if subscriber go to admin page
 */
function admin_redirect() {

    if (!current_user_can('edit_posts')) {
        wp_redirect(WP_HOME);
        exit;
    }
}

add_filter("admin_init", "admin_redirect");



// gestione redirect su login per autenticazione fallita o password vuota

add_action('wp_login_failed', 'atbv_loginfail');  // hook failed login

function atbv_loginfail($username) {
    
    wp_redirect(str_replace('/?login=failed', '', $_SERVER['HTTP_REFERER']) . '/?login=failed');
    exit;
}

add_filter('authenticate', 'check_login', 10, 3);

function check_login($user, $username, $password) {

    $exclusions = array(
        'wp-login.php',
        'wp-register.php',
        'wp-cron.php', // Just incase
        'wp-trackback.php',
        'wp-app.php',
        'xmlrpc.php',
    );

    if (!in_array(basename($_SERVER['PHP_SELF']),  $exclusions) || $_POST) {


        // check to see if user is allowed
        if ($password == '' || $username = '') {
            wp_redirect(str_replace('/?login=failed', '', $_SERVER['HTTP_REFERER']) . '/?login=failed');
            exit;
        }
    }

    return $user;
}
