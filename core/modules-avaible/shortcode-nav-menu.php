<?php
/**
 * Hook to implement shortcode logic inside WordPress nav menu items
 * Shortcode code can be added using WordPress menu admin menu in description field
 */
function shortcode_menu( $item_output, $item ) {

    if ( !empty($item->description)) {
         $output = do_shortcode($item->description);
         
         if ( $output != $item->description )
               $item_output = $output;
         
        }
 
    return $item_output;
      
}

add_filter("walker_nav_menu_start_el", "shortcode_menu" , 10 , 2);

