<?php

/*
 * Add taxonomies archive
 * 
 */


//add_filter('template_redirect', 'archive_taxonomies_template' , 0 );

/**
 * 
 * @global type $wp_query
 */
function archive_taxonomies_template() {

    global $wp_query;
    if ($wp_query->query["name"] != "") {

        $taxonomies = get_taxonomies('', 'names');
        foreach ($taxonomies as $taxonomy_name) {
            if ($taxonomy_name == $wp_query->query["name"]) {

                //locate_template("archive-" . $taxonomy_name . ".php", true);
                
                include get_template_directory() . "/archive-" . $taxonomy_name . ".php";
                exit;
                    
            }
        }
    }
}
