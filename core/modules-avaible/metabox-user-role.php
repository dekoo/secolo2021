<?php

//add_action('admin_menu', 'custom_author_meta_boxes');

function custom_author_meta_boxes() {

    remove_meta_box('authordiv', 'cowo-location', 'side');
    add_meta_box('authordiv', __('Cowo Manager'), 'cm_post_author_meta_box', 'cowo-location', 'side', 'high');
}

function cm_post_author_meta_box($post) {
    global $user_ID, $post;


    $users_data = new WP_User_Query(array('role' => 'cowo-manager'));


    $options = "";
    $options .= "<option value=''></option>";


    foreach ($users_data->get_results() as $user) {
        //pre( $user->ID , $post->post_author );
        $selected_attribute = ( $user->ID == $post->post_author ? " selected " : "" );
        $options .= "<option " . $selected_attribute . " value='" . $user->ID . "'>" . $user->display_name . "</option>";
    }


    echo "<label class='screen-reader-text' for='post_author_override'>Autore</label>
                        <select id='post_author_override' name='post_author_override' class='' >$options</select>";
}
