<?php

function get_transients($name) {
    global $wpdb;
    $sql = "SELECT * FROM {$wpdb->prefix}options WHERE option_name LIKE '_transient_" . $name . "%'";
    $results = $wpdb->get_results($sql);
    $return = array();
    foreach ($results as $result):
        $return[$result->option_name] = $result->option_value;
    endforeach;
    
    return $return;
}

function get_timeout_transient($timeout = 7200, $lapse = 600) {
    $max_lapse = intval($lapse - ($lapse / 2));
    $min_lapse = intval($max_lapse - $lapse);
    $current_lapse = intval(rand($min_lapse, $max_lapse));
    return  intval($timeout + $current_lapse);
}