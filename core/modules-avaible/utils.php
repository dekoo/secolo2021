<?php

/**
 * Ritorna una string rimuovendo le parole in eccesso
 * 
 * @param String $string
 * @param Int $word_limit
 * @return String
 */
function limit_words($string, $word_limit) {
    $words = explode(" ", $string);
    return implode(" ", array_splice($words, 0, $word_limit));
}
