<?php


require_once dirname(__FILE__) . '/vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;


/**
 * monolog singleton to use inside WordPress 
 * usage: 
 * $log = _mmonolog::getInstance();
 * $log->addInfo( "lof text"  );
 */
class _mmonolog {
    
  private static $singleton = null;
 

  static function getInstance(){
    if (_mmonolog::$singleton == null){

      _mmonolog::$singleton = new Logger("WordPressLog");

      $default = array(
        'FilePath' => TEMPLATEPATH . '/log.txt',
        );
      
      _mmonolog::$singleton->pushHandler(new StreamHandler( $default["FilePath"] ));      
      
      
    }
    return _mmonolog::$singleton;
  }

  function __construct(){
    
  }

    
    
}

