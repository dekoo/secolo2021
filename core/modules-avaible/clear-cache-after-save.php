<?php

//Pulizia della cache salvata nei due varnish
add_action('save_post', function() {
    if (defined('VARNISH_1') && defined('VARNISH_2')) {
        $url_purge = '/.*';
        $ip_array = array(VARNISH_1, VARNISH_2);

        $domains[] = $_SERVER['SERVER_NAME'];
        $domains[] = str_replace('www.', '', $_SERVER['SERVER_NAME']);

        $URL = xss_cleaner($url_purge);
        $host_names[] = xss_cleaner($domains[0]);
        $host_names[] = xss_cleaner($domains[1]);

        $debug = false;
        foreach ($ip_array as &$ipaddress) {
            purgeURL($host_names[0], $ipaddress, $URL, $debug);
            purgeURL($host_names[1], $ipaddress, $URL, $debug);
        }
    }
});

// Cross Site Script  & Code Injection Sanitization
function xss_cleaner($input_str) {
    $return_str = str_replace(array('<', ';', '|', '&', '>', "'", '"', ')', '('), array('&lt;', '&#58;', '&#124;', '&#38;', '&gt;', '&apos;', '&#x22;', '&#x29;', '&#x28;'), $input_str);
    $return_str = str_ireplace('%3Cscript', '', $return_str);
    return $return_str;
}

function purgeURL($hostname, $ip_address, $purgeURL, $debug) {

    $header = array
        (
        "Host:" . $hostname, // IMPORTANT
            //"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            //"Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.3",
            //"Accept-Encoding: gzip,deflate",
            //"Accept-Language: it-IT,it;q=0.8,en-US;q=0.6,en;q=0.4",
            //"Cache-Control: max-age=0",
            //"Connection: keep-alive",
    );

    $curlOptionList = array(
        CURLOPT_URL => 'http://' . $ip_address . $purgeURL,
        CURLOPT_HTTPHEADER => $header,
        CURLOPT_CUSTOMREQUEST => "BAN",
        CURLOPT_VERBOSE => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_NOBODY => true,
        CURLOPT_CONNECTTIMEOUT_MS => 2000,
    );

    $fd = false;
    if ($debug == true) {
        $fd = fopen("php://output", 'w+');
        $curlOptionList[CURLOPT_VERBOSE] = true;
        $curlOptionList[CURLOPT_STDERR] = $fd;
    }

    $curlHandler = curl_init();
    curl_setopt_array($curlHandler, $curlOptionList);
    curl_exec($curlHandler);
    curl_close($curlHandler);
    if ($fd !== false) {
        fclose($fd);
    }
}
