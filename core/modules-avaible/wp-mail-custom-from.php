<?php

/**
 * Impostano il mittente delle mail inviate dal sito
 * 
 */

add_filter('wp_mail_from_name', function ($original_email_from) {
    $original_email_from = 'FromName';
    return $original_email_from;
});

add_filter('wp_mail_from', function ($original_email_address) {
    $original_email_address = 'from@mail.xx';
    return $original_email_address;
});
