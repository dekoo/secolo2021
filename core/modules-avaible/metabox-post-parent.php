<?php

/**
 * parent metabox implementation
 */

// hook
add_action('add_meta_boxes', 'CustomMetabox_setup');

/**
 * callback from hook
 */
function CustomMetabox_setup() {

    add_meta_box(
            'customer_parent', //Id Metabox
            'MetaBox Title', //Title MetaBox
            'customer_parent_metabox', //Callback Output function
            'page', //(post,page,custom post type)
            'side', //admin area
            'high' //priority
    );
}

/**
 * metabox render function
 */
function customer_parent_metabox() {
    global $post;

    $out = "";

    $args = array(
        'post_type' => 'forum',
        'post_status' => 'publish',
        'orderby' => 'title',
        'order' => 'ASC',
    );


    $customer = get_transient('customer_parent_metabox');
    if (false == $customer) {
        $customer = get_posts($args);
        set_transient('customer_parent_metabox', $customer);
    }


    $out .= "<p><i>Seleziona l'azienda da associare</i></p>";
    $out .= wp_combo_posts("parent_id", $customer, $post->post_parent, "full_width");


    $out .= "<p><a href='post.php?post=" . $post->post_parent . "&action=edit'>vai alla scheda di dettaglio</a></p>";


    echo $out;
}
