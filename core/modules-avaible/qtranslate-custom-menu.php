<?php



add_action('admin_init', 'qtranslate_edit_taxonomies');


if (function_exists('qtrans_getLanguage')) {
    add_filter( 'walker_nav_menu_start_el', 'qtranslate_menulink' , 10 , 2);
}

/**
 * Hook per la gestione dei link custom all'interno dei menu
 * 
 * @global type $q_config 
 * 
 * @param type $item_output
 * @param type $item
 * @return type string 
 */
function qtranslate_menulink(  $item_output, $item ) {

    global $q_config;
    
    
    if ( $item->type == "custom" && $item->url != "#" && $q_config['default_language'] != $q_config['language'] ) {
        if(!preg_match('/^http:/', $item->url)) {
            $item_output = str_replace('href="' . $item->url . '"', 'href="/' . $q_config['language'] . $item->url . '"' , $item_output);
        }
    }

    return $item_output;
}