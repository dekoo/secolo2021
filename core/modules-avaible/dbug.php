<?php

function dbug($args, $die = false)
{
    echo "<pre>";
    var_dump($args);
    echo "</pre>";
    if ($die == true) {
        die();
    }
}
