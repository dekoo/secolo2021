<?php

/*
 * @maurizio | 07.10.2014
 * 
 * Check client country 
 * 
 * Idea from:
 * http://www.sitepoint.com/wordpress-security-plugins/
 * 
 */

add_action('login_init', function() {

    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    if (filter_var($ip, FILTER_VALIDATE_IP)) {

        $ctx = stream_context_create(array('http' =>
            array('timeout' => 5,)
        ));

        $data = file_get_contents('http://ipinfo.io/' . $ip, false, $ctx);
    }

    if ($data) {
        $obj = json_decode($data);

        switch (@$obj->country) {
            case "IT":
                // ok login allow

                $lastlogin = get_option( "lastlogin", array() );
                
                $thislogin = array(
                    "data" => date("F j, Y, g:i a"),
                    "ip" => $obj->ip,
                    "country" => $obj->country,
                    );
                
                $lastlogin[] = $thislogin;
                
                $lastlogin = array_slice($lastlogin, -5);
                update_option( "lastlogin", $lastlogin );

                
                break;

            case "US":
            case "SK":
            default:

                $messagge = "<h1>" . get_bloginfo() . "</h1>"
                        . "<p>Forbidden access. <br/> Please contact the administrator</p>";


                wp_die($messagge, get_bloginfo() . " | 403 Forbidden", array('response' => 403));
                break;
        }
    }
});
