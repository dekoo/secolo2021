<?php

/*
 * v1
 */

function get_attachments($post_id ) {

    $html = "<ul class='related_document'>"; 
            
    $args = array(
        'post_type' => 'attachment',
        'post_mime_type' => '',
        'post_parent' => $post_id,
        'showposts' => -1,
    );

    $attachments = get_posts($args);

    foreach ($attachments as $attachment) {
        $html .= "<li class='" . get_post_mime_type( $attachment->ID ) . "'><a href='" . wp_get_attachment_link( $attachment->ID ) . "'>" . $attachment->post_title . "</a><li>";
        
    }
    
    $html .= "</ul>";
    
    return $html;
}
