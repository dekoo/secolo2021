<?php


function get_googl( $permalink ) {
    
    $http = new WP_Http();
    $headers = array('Content-Type' => 'application/json');
    $result = $http->request('https://www.googleapis.com/urlshortener/v1/url', array('method' => 'POST', "sslverify" => false, 'body' => '{"longUrl": "' . $permalink . '"}', 'headers' => $headers));

    $body = json_decode($result["body"]);
    return $body->id;
}

