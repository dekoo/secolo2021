<?php
add_action('save_post', function ($post_id) {
    delete_transient('si_homepage-primi-5');
    delete_transient('si_homepage-secondi-4');
    delete_transient('si_homepage-homepage-secondari');
});

function dek_clean_editorial_position() {
$args = array(
						'post_type' => 'post',
						'post_status' => 'publish',
						'showposts' => 1,
						'tax_query' => array(
											array(
												'taxonomy' => 'posizione-editoriale',
												'field' => 'slug',
												'terms' => 'homepage-secondari',
												),
											),
						'order' => 'ASC'
							);
				$query = new WP_Query($args);
				$count = $query->found_posts;
				
				if ($query->have_posts()):
					if ($count > 50):
						while ($query->have_posts()): $query->the_post();
							$post_id = get_the_ID();
							wp_remove_object_terms($post_id, "homepage-secondari", 'posizione-editoriale');
						endwhile;
					endif;
				endif;
				wp_reset_query();
}

//add_action( 'save_post', 'dek_clean_editorial_position' );