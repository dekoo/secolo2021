<?php

$hooks[] = "generate_rewrite_rules";
//$hooks[] = "generate_rewrite_rules";
//$hooks[] = "generate_rewrite_rules";
//$hooks[] = "generate_rewrite_rules";

foreach ($hooks as $hook) {
    $path = WP_CONTENT_DIR . "/hook_" . $hook . ".log";
    @unlink($path);

    add_filter($hook, function( $param1, $param2 = null) use ($hook) {
                LogIt($hook, $param1, $param2);
                return $param1;
            }, 9999);
}

flush_rewrite_rules();

/**
 *
 * @param type $query
 */
function LogIt($hook, $param1, $param2 = null) {

    $line = date("c") . "\t" . $hook . "\t" . print_r($param1, true) . "\t" . print_r($param2, true);
    $path = WP_CONTENT_DIR . "/hook_" . $hook . ".log";

    //die( $path );

    $fp = @fopen($path, "a+");
    fwrite($fp, $line . ";\n");
    fclose($fp);



    return $param1;
}

add_filter("query", "QueryLogger");

function QueryLogger($query) {

    if (preg_match('/^\s*(insert|update|delete) /i', $query)) {

        //if ( preg_match( '/\b(wp_options|wp_terms|wp_usermeta|revision)\b/i', $query ) ) {
        if (preg_match('/\b(rewrite_rules)\b/i', $query)) {


            $path = WP_CONTENT_DIR . "/query.log";
            $fp = @fopen($path, "a+");
            fwrite($fp, $query . ";\n");
            fclose($fp);
        }
    }
    
    return $query;
}