<?php

if (class_exists("RW_Taxonomy_Meta")) {
    add_action('admin_init', 'as_taxonomy_meta_boxes');

    /**
     * Register meta boxes
     *
     * @return void
     */
    function as_taxonomy_meta_boxes() {

        $meta_sections = array();
// first meta section
        $meta_sections[] = array(
            'title' => 'Personalizzazione aspetto', // section title
            'taxonomies' => array('collezione-prodotto'), // list of taxonomies. Default is array('category', 'post_tag'). Optional
            'id' => 'collezione-prodotto-meta', // ID of each section, will be the option name
            'fields' => array(// list of meta fields
                array(
                    'name' => 'Stato collezione', // field name
                    'desc' => 'Se spuntato visualizza la collezione nella pagina elenco collezioni ', // field description, optional
                    'id' => 'term_active', // field id, i.e. the meta key
                    'type' => 'checkbox', // text box
                ),
                array(
                    'name' => 'Anno', // field name
                    'desc' => 'Anno di riferimento / Sottotitolo', // field description, optional
                    'id' => 'term_year', // field id, i.e. the meta key
                    'type' => 'text', // text box
                ),
                array(
                    'name' => 'Immagine Copertina', // field name
                    'desc' => 'Immagine visualizzata nella pagina elenco collezione', // field description, optional
                    'id' => 'term_image', // field id, i.e. the meta key
                    'type' => 'image', // text box
                ),
                array(
                    'name' => 'Prodotto in evidenza',
                    'desc' => 'Selezione il prodotto che sarà visualizzato in evidenza per questa collezione', // field description, optional
                    'id' => 'featured_psroduct',
                    'type' => 'select',
                    'options' => array(// Array of value => label pairs for radio options
                        'value1' => 'Nome prodotto #1',
                        'value2' => 'Nome prodotti #2',
                        'value3' => 'Nome prodotti #3'
                    ),
                ),
            )
        );

        foreach ($meta_sections as $meta_section) {
            new RW_Taxonomy_Meta($meta_section);
        }
    }

}