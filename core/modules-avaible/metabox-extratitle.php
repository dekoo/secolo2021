<?php

/**
 * metabox per gestioni info extra sotto il titolo
 * 
 */


add_action('edit_form_after_title', function() {
    global $post, $wp_meta_boxes;
    echo "<br />";
    do_meta_boxes(get_current_screen(), 'top', $post);

    });

add_action('add_meta_boxes', function () {

    add_meta_box(
            'extratitle', //Id Metabox
            'Occhiello Articolo', //Title MetaBox
            'render_extratitle', //Callback Output function
            'post', //(post,page,custom post type)
            'top', //admin area
            'high' //priority
    );
});

add_action('save_post', function ($post_id) {

    
    # Is the current user is authorised to do this action?
    if ((($_POST['post_type'] === 'post') )) { // If it's a page, OR, if it's a post, can the user edit it? 
        
        //&& current_user_can('edit_page', $post_id) || current_user_can('edit_post', $post_id)

        # Stop WP from clearing custom fields on autosave:
        if ((( ! defined('DOING_AUTOSAVE')) || ( ! DOING_AUTOSAVE)) && (( ! defined('DOING_AJAX')) || ( ! DOING_AJAX))) {

            # Nonce verification:
            if (wp_verify_nonce($_POST['_extranonce'], "extratitle")) {

                # Get the posted deck:
                $extratitle = sanitize_text_field($_POST['extratitle']);

                # Add, update or delete?
                if ($extratitle !== '') {

                    # Deck exists, so add OR update it:
                    update_post_meta($post_id, 'extratitle', $extratitle, true);

                } else {

                    # Deck empty or removed:
                    delete_post_meta($post_id, 'extratitle');

                }

            }

        }

    }

});

/**
 * metabox render function
 */
function render_extratitle() {
    global $post;

    $custom_fields = get_post_custom();


    $out .= "<textarea name='extratitle' rows='5' cols='50' id='extratitle' class='large-text '>" . $custom_fields["extratitle"][0] . "</textarea>";
    $out .= "<i>Occhiello (visibile solo nelle pagine interne)</i>";
    
    echo $out;
    
    # Display the nonce hidden form field:
    wp_nonce_field( "extratitle" , "_extranonce");

}
