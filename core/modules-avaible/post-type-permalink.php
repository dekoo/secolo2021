<?php

/*
 * Howto change custom taxonomy permastruct (alternative method)
 * 
 * 
 * WordPress way:
 * http://shibashake.com/wordpress-theme/add-custom-taxonomy-tags-to-your-wordpress-permalinks
 * http://shibashake.com/wordpress-theme/custom-post-type-permalinks-part-2
 * 
 * coworking - rewrite tag
 * cowo-location - custom post type name
 * geolocation - taxonomy name
 * 
 *  
 */

/*
 * add rewrite rules to match new permastrunct
 */
add_action('init', function() {
    add_rewrite_rule('^coworking/([^/]+)/([^/]+)/?$', 'index.php?cowo-location=$matches[2]&geolocation=$matches[1]', "top");

    });
    
    
/*
 * check if taxonomy term is 
 */
add_action('pre_get_posts', function ($query) {

    if ($query->query["post_type"] == "cowo-location" && get_query_var("geolocation") == "" && $query->is_main_query()) {

        //echo "<pre>";
        //var_dump( $wp_query, $queried_object );
        //die();

        $post = get_page_by_path($query->query["cowo-location"], OBJECT, "cowo-location");
        $terms = wp_get_object_terms($post->ID, 'geolocation', array("orderby" => "term_group"));

        if (!empty($terms)) {
            $taxonomy_slug = $terms[0]->slug;

            $permalink = '/coworking/' . $taxonomy_slug . "/" . $query->query["cowo-location"];
            wp_redirect($permalink);
            die();
        }
    }
});


add_filter('post_type_link', function ( $permalink, $post, $leavename ) {
    
    if (get_post_type( $post) == "cowo-location") {
    
        $terms = wp_get_object_terms($post->ID, 'geolocation' , array("orderby" => "term_group"));	
        
        if (!empty($terms)) {
            $taxonomy_slug = $terms[0]->slug;
            }
        
        //$permalink = str_replace('coworking', "coworking/" . $taxonomy_slug, $permalink);
        $permalink = '/coworking/' . $taxonomy_slug . "/" . $post->post_name;
    }
    
    return $permalink;
}, 10, 3);