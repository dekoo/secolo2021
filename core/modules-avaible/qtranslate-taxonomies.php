<?php


//Add qtranslate URL filter to custom post types
add_filter('post_type_link', 'qtrans_convertURL');

/**
 * Add qtraslate admin language interface to custom taxonomies.
 */

add_action('admin_init', function () {

    $args = array(
        'public' => true,
        '_builtin' => false
    );
    $output = 'object'; // or objects
    $operator = 'and'; // 'and' or 'or'

    $taxonomies = get_taxonomies($args, $output, $operator);

    if ($taxonomies) {
        foreach ($taxonomies as $taxonomy) {
            add_action($taxonomy->name . '_add_form', 'qtrans_modifyTermFormFor');
            add_action($taxonomy->name . '_edit_form', 'qtrans_modifyTermFormFor');
        }
    }
});
