<?php
/**
 * 
 * 22.02.2012 | maurizio
 * simple manteinance mode
 * first check if 503.php exist. if not use wp_die to terminate page
 */
	
add_action('get_header', 'maintenace_mode');

function maintenace_mode() {
	if ( !current_user_can( 'administrator' ) ) {
		
            if (file_exists( TEMPLATEPATH . "/503.php") ) {

                status_header( "503" );
                nocache_headers();
                header( 'Content-Type: text/html; charset=utf-8' );
                
                load_template( TEMPLATEPATH . "/503.php");
                
                die();
            } else {

                
                wp_die("<h1>Manteinance Mode</h1>" , "Manteinance Mode" , array( 'response' => 503 ));
            }
        }
}
	
