<?php

/**
 * @todo hook per aggiungere lang come campo hidden al form
 * @todo hook per aggiungere wrapper contentore errore
 */

/**
 * qtranslate comments error action filter
 */
add_action("pre_comment_on_post", function( $comment_post_ID) {

    $comment_author = ( isset($_POST['author']) ) ? trim(strip_tags($_POST['author'])) : null;
    $comment_author_email = ( isset($_POST['email']) ) ? trim($_POST['email']) : null;
    $comment_content = ( isset($_POST['comment']) ) ? trim($_POST['comment']) : null;

    global $q_config, $locale , $l10n;
    
    $q_config['language'] = ( isset($_POST['lang']) ) ? trim(strip_tags($_POST['lang'])) : null;
    $locale = $q_config['language'];
        
    unload_textdomain( "default" );
    load_default_textdomain();
    
    $comment_error = false;

    $user = wp_get_current_user();
    if (get_option('require_name_email') && !$user->exists()) {
        if (6 > strlen($comment_author_email) || '' == $comment_author){
            $comment_error = __('<strong>ERROR</strong>: please fill the required fields (name, email).');
        }
        if (!is_email($comment_author_email)) {
                $comment_error = __('<strong>ERROR</strong>: please enter a valid email address.');
        }
    }

    if ('' == $comment_content) {
        $comment_error = __('<strong>ERROR</strong>: please type a comment.');
    }
       
    if ($comment_error) {
        $location = get_permalink( $comment_post_ID ) . "?lang=" . qtrans_getLanguage() . "&e=" . urlencode($comment_error) . "#error_message";
        
        wp_safe_redirect($location);
        exit;
    }
});