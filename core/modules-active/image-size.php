<?php

add_action('init', function () {    
     add_image_size('listing-post-medium', 640, 267, true);
     add_image_size('listing-post-square', 100, 100, true);
	 add_image_size('listing-post-home2', 474, 198, true);
	 add_image_size('listing-post-small', 160, 67, true);
});
