<?php
if (!function_exists('comment_nav')) :

    function comment_nav()
    {
        // Are there comments to navigate through?
        if (get_comment_pages_count() > 1 && get_option('page_comments')) :

            ?>
            <nav class="navigation comment-navigation" role="navigation">
                <h2 class="screen-reader-text"><?php _e('Comment navigation', 'twentyfifteen'); ?></h2>
                <div class="nav-links">
                    <?php
                    if ($prev_link = get_previous_comments_link(__('Older Comments', 'twentyfifteen'))) :
                        printf('<div class="nav-previous">%s</div>', $prev_link);
                    endif;

                    if ($next_link = get_next_comments_link(__('Newer Comments', 'twentyfifteen'))) :
                        printf('<div class="nav-next">%s</div>', $next_link);
                    endif;

                    ?>
                </div><!-- .nav-links -->
            </nav><!-- .comment-navigation -->
            <?php
        endif;
    }
endif;

add_filter('xcomment_form_field_comment', function ( $comment_field ) {
    if (is_user_logged_in()):
        $fields = get_fields('user_' . get_current_user_id());

        $img_url = '/images/generic-placeholder.jpg';
        if (isset($fields['foto']['sizes']['c-medium']) && !empty($fields['foto']['sizes']['c-medium'])):
            $img_url = $fields['foto']['sizes']['c-medium'];
        endif;
    endif;
    $comment_field = '<div class="comment-form-comment grid">
                        <div class="grid__item large--1-12 medium--1-6 small--1-4">
                            <div class="grid__box">
                                <img src="' . $img_url . '" class="img-responsive">
                            </div>
                        </div>
                        <div class="grid__item large--11-12 medium--5-6 small--3-4">
                            <div class="grid__box">
                                <textarea required id="comment" name="comment" placeholder="' . 'Scrivi il tuo commento' . '" cols="45" rows="6" aria-required="true"></textarea>
                            </div>
                        </div>
                      </div>';

    return $comment_field;
});

function ps_comments_callback($comment, $args, $depth)
{
    $GLOBALS['comment'] = $comment;
    //dbug($comment);
    $author_data = get_userdata(get_user_by('email', $comment->comment_author_email)->ID);
    if ($author_data):
        $name = $author_data->first_name . ' ' . $author_data->last_name;
    else:
        $name = $comment->comment_author;
    endif;

    $date = date_i18n("j F Y", strtotime($comment->comment_date));

    ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
        <article id="comment-<?php comment_ID(); ?>" class="comment">
            <div class="grid">
                <div class="grid__item large--11-12 medium--5-6 small--3-4">
                    <div class="grid__box">
                        <strong><?php echo $name; ?></strong>
                        <small><?php echo $date; ?></small>
                    </div>
                </div>
                <div class="grid__item">
                    <div class="grid__box comment-content">
                        <?php comment_text(); ?>
                        <div class="reply">
                            <?php comment_reply_link(array_merge($args, array('reply_text' => 'Rispondi', 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </li>
    <?php
}
