<?php

function get_template_part_parameterized($slug, $name = null, $params = array()) {
    $templates = array();
    $name = (string) $name;

    if ('' !== $name) {
        $templates[] = "{$slug}-{$name}.php";
    }
    $templates[] = "{$slug}.php";

    extract($params, EXTR_SKIP);

    if (locate_template($templates)) {
        include(locate_template($templates));
    }
}