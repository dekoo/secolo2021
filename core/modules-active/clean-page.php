<?php

// disabilita jquery migrate
function dequeue_jquery_migrate( $scripts ) {
    if ( ! is_admin() && ! empty( $scripts->registered['jquery'] ) ) {
        $scripts->registered['jquery']->deps = array_diff(
            $scripts->registered['jquery']->deps,
            [ 'jquery-migrate' ]
        );
    }
}
add_action( 'wp_default_scripts', 'dequeue_jquery_migrate' );



// Rimuovere emoji da WordPress 4.2
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );



function dequeue_service_scripts() {
	if ( is_single() && 'post' == get_post_type() ) {
		//woocommerce
		wp_dequeue_script( 'wc-add-to-cart' );
		wp_deregister_script( 'wc-add-to-cart' );
		wp_dequeue_script( 'woocommerce' );
		wp_deregister_script( 'woocommerce' );
		wp_dequeue_script( 'wc-cart-fragments' );
		wp_deregister_script( 'wc-cart-fragments' );
		
				
		//paypal
		wp_dequeue_script( 'paypal-ipnpublic-bn' );
		wp_deregister_script( 'paypal-ipnpublic-bn' );
		//rss reader 
		wp_dequeue_script( 'jquery-easy-ticker' );
		wp_deregister_script( 'jquery-easy-ticker' );
		wp_dequeue_script( 'super-rss-reader' );
		wp_deregister_script( 'super-rss-reader' );
		
		
		//CSS
		//blocchi
		wp_dequeue_style( 'wp-block-library' );
		wp_deregister_style( 'wp-block-library' );
		
	
		//rss reader 
		wp_dequeue_style( 'super-rss-reader' );
		wp_deregister_style( 'super-rss-reader' );
		//woocommerce
		wp_dequeue_style( 'woocommerce-layout' );
		wp_deregister_style( 'woocommerce-layout' );
		wp_dequeue_style( 'woocommerce-smallscreen' );
		wp_deregister_style( 'woocommerce-smallscreen' );
		wp_dequeue_style( 'woocommerce-general' );
		wp_deregister_style( 'woocommerce-general' );
		wp_dequeue_style( 'woocommerce-inline' );
		wp_deregister_style( 'woocommerce-inline' );
		
		// swiper
		wp_dequeue_script( 'ir-swiper' );
		wp_deregister_script( 'ir-swiper' );
	}
}

add_action( 'wp_print_scripts', 'dequeue_service_scripts' );

function dequeue_service_styles() {
	if ( is_single() && 'post' == get_post_type() ) {
		
		//blocchi
		wp_dequeue_style( 'wp-block-library' );
		wp_deregister_style( 'wp-block-library' );
		wp_dequeue_style( 'wc-block-style' );
		wp_deregister_style( 'wc-block-style' );
		//rss reader 
		wp_dequeue_style( 'super-rss-reader' );
		wp_deregister_style( 'super-rss-reader' );
		//woocommerce
		wp_dequeue_style( 'woocommerce-layout' );
		wp_deregister_style( 'woocommerce-layout' );
		wp_dequeue_style( 'woocommerce-smallscreen' );
		wp_deregister_style( 'woocommerce-smallscreen' );
		wp_dequeue_style( 'woocommerce-general' );
		wp_deregister_style( 'woocommerce-general' );
		wp_dequeue_style( 'woocommerce-inline' );
		wp_deregister_style( 'woocommerce-inline' );
		wp_dequeue_style( 'wc-blocks-vendors-style-css' );
		wp_deregister_style( 'wc-blocks-vendors-style-css' );
		wp_dequeue_style( 'wc-blocks-style' );
		wp_deregister_style( 'wc-blocks-style' );
		
	}
}

add_action( 'wp_print_styles', 'dequeue_service_styles' );












function wpa54064_inspect_scripts() {
    global $wp_scripts;
    foreach( $wp_scripts->queue as $handle ) :
        echo "<!-- enzo ". $handle."-->   ";
    endforeach;
	
	
}
add_action( 'wp_print_scripts', 'wpa54064_inspect_scripts' );

function wpa54064_inspect_style() {
    global $wp_styles;
    foreach( $wp_styles->queue as $handle ) :
        echo "<!-- enzo ". $handle."-->   ";
    endforeach;
	
	
}
add_action( 'wp_print_styles', 'wpa54064_inspect_style' );


