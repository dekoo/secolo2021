<?php

function hex_to_rgb($hex)
{
    list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
    return array(
        'r' => $r,
        'g' => $g,
        'b' => $b
    );
}