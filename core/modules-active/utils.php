<?php

function get_post_sign($post)
{
    $sign = get_field('sign', $post->ID);
	//custom sign direttore storace
	/*if ($sign == "Il Cavaliere Nero") {$sign= "Il Cavaliere Nero 😎";}*/
	
    if (empty($sign)):
        $author = get_user_by('ID', $post->post_author);
        $sign = $author->first_name . ' ' . $author->last_name;
    endif;
	$sign = ucwords(strtolower($sign)); 
    return $sign;
}
/*
add_filter('login_url', function($login_url) {
    return home_url('/wp-login.php?loginkey=secoloditalia&redirect_to=' . home_url('/wp-admin/index.php'));
});*/

add_filter('register_url', function($register_url) {
    return home_url('/registrazione/');
});

add_filter('template_redirect', function($template) {
    if ((is_woocommerce() || is_checkout() || is_cart() || is_shop()) && is_user_logged_in()):
        $user_fields = get_fields('user_' . get_current_user_id());
    
        if (check_expiration($user_fields['expiration_subscription'])):
            wp_redirect('/gia-abbonato');
            exit;
        endif;
    endif;
    
    if(is_page('gia-abbonato') && is_user_logged_in()):
        $user_fields = get_fields('user_' . get_current_user_id());
    
        if (!check_expiration($user_fields['expiration_subscription'])):
            wp_redirect('/abbonamenti');
            exit;
        endif;
    endif;

    return $template;
}, 99);

remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);

add_filter( 'login_headerurl', 'custom_loginlogo_url' );
function custom_loginlogo_url($url) {
    return '/';
}


// trasforma il canonical url in https

add_filter( 'wpseo_canonical', function( $canonical ) {
  
  $canonical = str_replace ( "http:" , "https:" , $canonical );
  return $canonical;
});