<?php

/**
 * 
 * Make theme available for translation.
 * Translations can be filed in the /resources/lang/ directory.
 * 
 */
add_action('after_setup_theme', function () {
    load_theme_textdomain('secoloditalia', get_template_directory() . '/resources/lang');
});
