<?php

/* 
 * http://www.yourinspirationweb.com/2014/09/16/personalizzare-wordpress-per-i-tuoi-clienti-parte-2/
 * 
 */


add_action('login_head', function () {
    echo '<style type="text/css">
            h1 a {
                background-image:url(/images/logo.png) !important;
                background-size: contain !important;
                width: 300px !important;
                }
            body { 
                background-color: #fff !important
                }
                </style>';
});


add_action('admin_head', function () {
    echo '<style type="text/css"> #header-logo { background-image:url(/images/logo.jpg) !important; }  </style>';
});

add_filter( 'admin_footer_text', function( $default_text ) {
     return $default_text . ' | <span>Theme by <a href="http://www.mavida.com/">Mavida snc</a></span>';
});