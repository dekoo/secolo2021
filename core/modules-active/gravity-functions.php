<?php
//Add function for hide title of field
add_filter('gform_enable_field_label_visibility_settings', '__return_true');

add_filter('gform_pre_render', function($form) {

    ?>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            /* apply only to a textarea with a class of gf_readonly */
            jQuery("li.gf_readonly textarea").attr("readonly", "readonly");
        });
    </script>

    <?php
    return $form;
});
