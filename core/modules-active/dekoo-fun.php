<?php

 if (current_user_can('editor') ) {
	add_action( 'admin_menu' , 'dek_remove_post_fields' );
	}
/**
 * Remove meta boxes from page screen
 */
 
function dek_remove_post_fields() {
    
    remove_meta_box( 'authordiv' , 'post' , 'normal' ); //removes author 
}


/**
 * Rimuove il div che consente la modifica autore da quickedit
 *https://wordpress.stackexchange.com/questions/59871/remove-specific-items-from-quick-edit-menu-of-a-custom-post-type
 */

if (current_user_can('editor') ) {
	add_action( 'admin_head-edit.php', 'wpse_59871_script_enqueuer' );
	}
function wpse_59871_script_enqueuer() 
{    
    /**
       /wp-admin/edit.php?post_type=post
       /wp-admin/edit.php?post_type=page
       /wp-admin/edit.php?post_type=cpt  == gallery in this example
     */

    global $current_screen;
    if( 'edit-post' != $current_screen->id )
        return;
    ?>
    <script type="text/javascript">         
        jQuery(document).ready( function($) {
            
            $('.inline-edit-author').each(function (i) {
                $(this).remove();
            });
        });    
    </script>
    <?php
}

//rimuove da admin il box per inserire i tag
/*
add_action( 'admin_menu', 'myprefix_remove_meta_box');
function myprefix_remove_meta_box(){
   remove_meta_box( 'tagsdiv-post_tag','post','normal' );
}*/

//complime immagini in upload
add_filter( 'jpeg_quality', create_function( '', 'return 70;' ) );


function add_comscore_amp(){
	
	echo "
	<amp-analytics type=\"comscore\">
	<script type=\"application/json\">
	{
		\"vars\":{\"c2\":\"30439819\"},\"extraUrlParams\":{\"comscorekw\":\"amp\"}
	}
	</script>
	</amp-analytics>
	";
	
}
	add_filter('amp_post_template_footer','add_comscore_amp');

/**
 * Modify The Author Schema Markup in Schema Plugin
 *
 * @param array $author
 * @return array 
 */
 
 add_filter( 'schema_wp_author', 'schema_author_output_modify_8198771356' );
function schema_author_output_modify_8198771356( $author) {
//	$sign=get_post_sign($post);
//  	$sign = get_sign($post->ID);
	// Modify author name
	$author['name'] = "Redazione";
	
	// Modify description
	//$author['description'] = 'Some other description';
	
	// Remove Author URL from the markup
	
	unset($author['image']);
	unset($author['description']);
	unset($author['url']);
	
	// Return the array
	return $author; 
}

if (strpos($_SERVER['REQUEST_URI'], "/amp/") == false){
// amp found

add_filter( 'the_content', function( $content ) {
    return $content.wp_link_pages(array(
        'before' => '<div class="page-links">',
        'after' => '</div>',
        'link_before' => '<span>',
        'link_after' => '</span>',
		'next_or_number'   => 'next',
		'nextpagelink' => 'L\'articolo continua alla pagina successiva',
		'previouspagelink' => 'L\'articolo comincia alla pagina precedente',
		'pagelink' => '%',
        'echo' => 0
    ));
}, 1);
}




/* olt con articoli correlati tassonomia
function wpse_ad_content($content)
{
   
	
$post_id = get_the_ID();
    $cat_ids = array();
    $categories = get_the_category( $post_id );
 $nuovo_contenuto ="";
    if ( $categories && !is_wp_error( $categories ) ) {
 
        foreach ( $categories as $category ) {
 
            array_push( $cat_ids, $category->term_id );
 
        }
 
    }
$args = array(
                                        'post_type' => 'post',
                                        'post_status' => 'publish',
                                        'category__in' => $cat_ids,
										'post__not_in' => array( $post_id ),
                                        'posts_per_page' => 2,
                                        'orderby' => 'date',
                                        'order' => 'DESC'
                                    );

                                    $query = new WP_Query($args);

                                    if ($query->have_posts()):
                                        while ($query->have_posts()): $query->the_post();	
	
									$nuovo_contenuto.= "<div class ='correlati_content_row'><a href='".get_the_permalink()."'>".get_the_post_thumbnail($post->ID, 'listing-post-small' )."</a><a href='".get_the_permalink()."'><span>".get_the_title()."</span></a></div>";
	
		
									endwhile;
                                    endif;
                                    wp_reset_query();
	
	
    $paragraphAfter = 2; //Enter number of paragraphs to display ad after.
    $content = explode("</p>", $content);
    $new_content = '';
    for ($i = 0; $i < count($content); $i++) {
        if ($i == $paragraphAfter) {
            $new_content.= "<!--<h3>Potrebbe interessarti anche:</h3>--><div class='correlati_content'>" .$nuovo_contenuto. "</div>";
           
        }

        $new_content.= $content[$i] . "</p>";
    }

    return $new_content;
}
*/

function wpse_ad_content($content)
{
  
	$posts = get_field('articoli_correlati_meta_articolo');
		if ($posts) :	
			$nuovo_contenuto ="";
			if( have_rows('articoli_correlati_meta_articolo') ):
				while( have_rows('articoli_correlati_meta_articolo') ): the_row();
					$url = get_sub_field('url_correlato');
					$post_id = url_to_postid( $url );
					$getitle = get_sub_field('titolo');
					if (!$getitle) $getitle = get_post_field( 'post_title', $post_id, true );
					
					$nuovo_contenuto.= "<li><span>&nbsp;</span><a href='". $url ."?utm_source=content&utm_medium=related&utm_campaign=middle'>". $getitle ."</a></li>";
		
				endwhile;
			endif;
		endif;
		
		$paragraphAfter = 2; //Enter number of paragraphs to display ad after.
		$content = explode("</p>", $content);
		$new_content = '';
		for ($i = 0; $i < count($content); $i++) {
			if ($i == $paragraphAfter) {
				if( have_rows('articoli_correlati_meta_articolo') ){
					$new_content.= "<div class='correlati_content'><span>LEGGI ANCHE</span><ul>" .$nuovo_contenuto. "</ul></div>";
				}
			}
			$new_content.= $content[$i] . "</p>";
			}

		return $new_content;
   
}

add_filter('the_content', 'wpse_ad_content');

// Limit media library access
// se user loggato è diverso da e.maisto non permette di recuperare immagini dalla media gallery
add_filter( 'ajax_query_attachments_args', 'mrfx_show_current_user_attachments' );

function mrfx_show_current_user_attachments( $query ) {
	$imeges_admin = array (45,122);
    $user_id = get_current_user_id();
	if (!in_array($user_id, $imeges_admin)) {
   
        $query['author'] = 10000;
    }
    return $query;
}

function hook_css() {
	if (is_page() AND (!is_front_page())) {
    ?>
    <style>
	
	#itro_popup{
	z-index:-1 !important;
	}
	#itro_opaco {
	opacity: 0.0 !important;
	}
	</style>
    <?php
	}
}
add_action('wp_footer', 'hook_css');

// Disabilito la console di amministrazione
// agli utenti che non sono amministratori
/*
function my_disable_dashboard()
{
  if (is_admin() && is_user_logged_in() && !wp_doing_ajax())
  {
    if ($user_id != 45) {
         wp_redirect(home_url()); exit();
    }
  }
}

// Aggiungo azione su init() per il
// controllo degli accessi su dashboard

add_action('init','my_disable_dashboard');


<?php wp_deregister_script( 'script-handle' ); ?>