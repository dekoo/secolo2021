<?php
//controlla se lo user agent è di un'app mobile del Secolo d'Italia
function dek_secolo_app() {
    if ( empty( $_SERVER['HTTP_USER_AGENT'] ) ) {
        $is_myapp = false;
    } elseif ( strpos( $_SERVER['HTTP_USER_AGENT'], 'Secolo-Android' ) !== false // Many mobile devices (all iPhone, iPad, etc.)
        || strpos( $_SERVER['HTTP_USER_AGENT'], 'Secolo-iOS' ) !== false ) {
        $is_myapp = true;
    } else {
        $is_myapp = false;
    }
    return apply_filters( 'dek_secolo_app', $is_myapp );
}