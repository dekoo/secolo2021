<?php

/**
 * modules to force related link in content table and inside menu
 * Inspired by http://www.456bereastreet.com/archive/201010/how_to_make_wordpress_urls_root_relative/
 */
if (!is_admin() || in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'))) {

    $filters = array(
        'bloginfo_url',
        'the_permalink',
        'wp_list_pages',
        'wp_list_categories',
        'the_content_more_link',
        'the_tags',
        'get_pagenum_link',
        'get_comment_link',
        'month_link',
        'day_link',
        'year_link',
        'tag_link',
        'the_author_posts_link',
        'script_loader_src',
        'style_loader_src',
        'xxx home_url', // commentato in quanto genera una pagina bianca in HP qualora si usasse una pagina statica
        'xxxx site_url' // commentato in quanto visualizza un notice
    );

    // make relative link
    //    add_filter('home_url', 'wp_make_link_relative');
    //    add_filter('site_url', 'wp_make_link_relative');

    foreach ($filters as $filter) {
        add_filter($filter, "relative_link");
    }
}


function relative_link($input) {
    preg_match('|https?://([^/]+)(/.*)|i', $input, $matches);
    if (!isset($matches[1]) || !isset($matches[2])) {
        return $input;
    } elseif (($matches[1] === $_SERVER['SERVER_NAME']) || $matches[1] === $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT']) {
        return wp_make_link_relative($input);
    } else {
        return $input;
    }
}
