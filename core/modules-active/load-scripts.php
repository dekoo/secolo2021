<?php

add_action('wp_enqueue_scripts', 'load_scripts');
add_action('admin_enqueue_scripts', 'admin_scripts');

/**
 * Script Loading
 */
function load_scripts() {

    /* stylesheets */
	
	//wp_register_style('cls', get_home_url() . '/stylesheets/cls.css?ver=1', array(), null);
    //wp_enqueue_style('cls');

    wp_register_style('bootstrap', get_home_url() . '/libs/bootstrap/dist/css/bootstrap.min.css', array(), null);
    wp_enqueue_style('bootstrap');

	//google font
	wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap', false );

   // wp_register_style('font-awesome', get_home_url() . '/libs/font-awesome/css/font-awesome.min.css', array(), null);
   // wp_enqueue_style('font-awesome');

   // wp_register_style('bxslider', get_home_url() . '/libs/bxslider-4/dist/jquery.bxslider.css', array(), null);
   // wp_enqueue_style('bxslider');

   //wp_register_style('fancybox', get_home_url() . '/libs/fancybox/source/jquery.fancybox.css', array(), null);
   // wp_enqueue_style('fancybox');

    wp_register_style('app', get_home_url() . '/stylesheets/app.min.css?ver=20230217', array(), null);
    wp_enqueue_style('app');


    /* javascript */
    wp_enqueue_script('jquery', array(), null);

    // nel caso lo si gestisca direttamente
    //wp_deregister_script('jquery');
    
   
    wp_register_script('bootstrap', get_home_url() . '/libs/bootstrap/dist/js/bootstrap.min.js', array(), null, true);
    wp_enqueue_script('bootstrap');
   
   // wp_register_script('fitvids', get_home_url() . '/libs/fitvids/jquery.fitvids.js', array(), null, true);
   // wp_enqueue_script('fitvids');
    
    
    // versione sviluppo
    // wp_register_script('main', get_home_url() . '/javascripts/main.js', array(), null, true);
    // versione minificata - produzione
    wp_register_script('main', get_home_url() . '/javascripts/main.min.js?ver=20230217', array(), null, false);
    wp_enqueue_script('main');
	
	
	wp_enqueue_script('ir-swiper', get_home_url() . '/javascripts/swiper.min.js', array(), null, false);
    
}

function admin_scripts() {

    wp_register_style('admin', 'https://www.secoloditalia.it/stylesheets/admin.css', array(), null);
    wp_enqueue_style('admin');

    // versione sviluppo
    wp_register_script('admin', 'https://www.secoloditalia.it/javascripts/admin.js', array(), null, false);
    
    // versione minificata - produzione
    // wp_register_script('admin', get_home_url() . '/javascripts/admin.min.js', array(), null, false);
    
    wp_enqueue_script('admin');
}

//stampa inline le regole css per ottimizzare il cls
function inline_styles() {
	$cls = ".banner{background-color:#fff!important}.masthead_adv{width:970px;height:268px;border:1px solid #ccc}.masthead_adv div{display:flex;flex-direction:column;justify-content:center;align-items:center;height:100%}";
	echo "<style>". $cls ."</style>";
	}
add_action('wp_head', 'inline_styles', 100);