<?php
add_shortcode("premium", function( $atts, $content = null) {
    global $post;
    extract(shortcode_atts(array(
        'level' => '',
        'teaser' => '',
        'message' => ''
            ), $atts));

    $return = $content;
    $newsletter = filter_input(INPUT_GET, 'newsletter');
    $is_from_newsletter = false;

    if (!empty($newsletter)):
        $fields = get_fields($post->ID);

        if ($newsletter == $fields['newsletter_code']):
            $is_from_newsletter = true;
        endif;
    endif;
	
	if ( dek_secolo_app() ):
		$is_from_newsletter = true;
	endif;
	

    if (!$is_from_newsletter):
        if (!is_user_logged_in() || (!check_expiration(get_field('expiration_subscription', 'user_' . get_current_user_id()))) && !current_user_can('administrator')):
            $return = '<div class="block-premium" style="background:#21649c">'
                . '<div class="gradient"></div>'
                . '<div class="message">'
                . 'Per continuare a leggere l\'articolo <a href="/sostienici/" style="color:#fde200;">sostienici</a> oppure <a href="' . wp_login_url(get_the_permalink($post)) . '">accedi</a>'
                . '</div>'
                . '<a href="/sostienici/"><img src="/images/abbonamenti.png"></a>'
                . '</div>';
        endif;
    endif;

    return $return;
});
