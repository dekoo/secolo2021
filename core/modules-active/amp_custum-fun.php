<?php
global $post;

//* add sticky adv
add_filter('amp_post_template_head','amp_custom_sticky_adv_js');
function amp_custom_sticky_adv_js() { ?>
	<script async custom-element='amp-sticky-ad' src='https://cdn.ampproject.org/v0/amp-sticky-ad-1.0.js'></script>
	<?php 
}

add_filter('amp_post_template_above_footer','amp_custom_sticky_adv');
function amp_custom_sticky_adv() { ?>
	<amp-sticky-ad layout="nodisplay">
		<amp-ad data-block-on-consent width=320 height=100 type="doubleclick" data-slot="/220249732/Secolo/IT_Secolo_320x100_sticky_AMP" rtc-config='{ "vendors": {"criteo": { "NETWORK_ID": 6988 }, "indexexchange": {"SITE_ID": 599966 }}, "timeoutMillis": 1000}' data-enable-refresh="30"> </amp-ad>
	</amp-sticky-ad>
	<?php 
}

//** add consent GDPR 
add_filter('amp_post_template_head','amp_consent_js');
function amp_consent_js() { ?>
	<script async custom-element='amp-consent' src='https://cdn.ampproject.org/v0/amp-consent-0.1.js'></script>
	<?php 
}
add_filter('ampforwp_body_beginning','amp_consent_quantcast');
	
function amp_consent_quantcast() { ?>
	<!-- QUANTCAST CMP -->
   
    <amp-consent id="quantcast" layout="nodisplay">
      <script type="application/json">
        {
        "consentInstanceId": "quantcast",
        "checkConsentHref": "https://apis.quantcast.mgr.consensu.org/amp/check-consent",
        "consentRequired": "remote",
        "promptUISrc": "https://adv.rtbuzz.net/tcfv2/amp.html?v=1.0.2",
        "postPromptUI": "postPromptUI",
        "clientConfig": {
                "coreConfig": {
                        "quantcastAccountId": "u8c9pru4NG2Ps",
                        "privacyMode": [
                                "GDPR"
                        ],
                        "hashCode": "/udanMm103TzilQX5TY2Pw",
                        "publisherCountryCode": "IT",
                        "publisherName": "Secoloditalia",
                        "vendorPurposeIds": [
                                2,
                                3,
                                4,
                                5,
                                6,
                                7,
                                8,
                                9,
                                10,
                                1
                        ],
                        "vendorFeaturesIds": [
                                1,
                                3,
                                2
                        ],
                        "vendorPurposeLegitimateInterestIds": [
                                3,
                                5,
                                7,
                                8,
                                9,
                                2,
                                4,
                                10,
                                6
                        ],
                        "vendorSpecialFeaturesIds": [
                                1,
                                2
                        ],
                        "vendorSpecialPurposesIds": [
                                1,
                                2
                        ],
                        "googleEnabled": true,
                        "displayUi": "always",
                        "defaultToggleValue": "off",
                        "initScreenRejectButtonShowing": false,
                        "initScreenCloseButtonShowing": true,
                        "publisherLogo": "https://static.takerate.com/cmplogs/secolo_takerate.png?qc-size=353,138",
                        "vendorListUpdateFreq": 6,
                        "publisherPurposeIds": [
                                1,
                                2,
                                3,
                                4,
                                5,
                                6,
                                7,
                                8,
                                9,
                                10
                        ],
                        "initScreenBodyTextOption": 1,
                        "publisherConsentRestrictionIds": [],
                        "publisherLIRestrictionIds": [],
                        "publisherPurposeLegitimateInterestIds": [],
                        "publisherSpecialPurposesIds": [],
                        "publisherFeaturesIds": [],
                        "publisherSpecialFeaturesIds": [
                                1,
                                2
                        ],
                        "stacks": [],
                        "lang_": "it"
                },
                "coreUiLabels": {
                        "initScreenRejectButton": "NON ACCETTO",
                        "saveAndExitButton": "SALVA",
                        "agreeButton": "ACCETTA E CHIUDI"
                },
                "theme": {
                        "uxPrimaryButtonColor": "#007F0E",
                        "uxSecondaryButtonTextColor": "#888",
                        "uxToogleActiveColor": "#888",
                        "uxLinkColor": "#888"
                }
        }
}
      </script>
      <div id="postPromptUI">
      <button role="button" on="tap:quantcast.prompt()">
        <svg style="height:20px">
            <g fill="none">
              <g fill="#FFF">
                <path
                  d="M16 10L15 9C15 9 15 8 15 8L16 7C16 7 16 6 16 6 16 5 15 4 14 3 14 2 13 2 13 3L12 3C12 3 11 3 11 2L11 1C11 1 10 0 10 0 9 0 7 0 6 0 6 0 5 1 5 1L5 2C5 3 4 3 4 3L3 3C3 2 2 2 2 3 1 4 0 5 0 6 0 6 0 7 0 7L1 8C1 8 1 9 1 9L0 10C0 10 0 11 0 11 0 12 1 13 2 14 2 15 3 15 3 14L4 14C4 14 5 14 5 15L5 16C5 16 6 17 6 17 7 17 9 17 10 17 10 17 11 16 11 16L11 15C11 14 12 14 12 14L13 14C13 15 14 15 14 14 15 13 16 12 16 11 16 11 16 10 16 10ZM13 13L12 13C11 13 11 13 9 14L9 16C9 16 7 16 7 16L7 14C5 14 5 13 4 13L3 13C2 13 1 12 1 11L3 10C2 9 2 8 3 7L1 6C1 5 2 4 3 4L4 4C5 4 5 3 7 3L7 1C7 1 9 1 9 1L9 3C11 3 11 4 12 4L13 4C14 4 15 5 15 6L13 7C14 8 14 9 13 10L15 11C15 12 14 13 13 13ZM8 5C6 5 5 7 5 9 5 10 6 12 8 12 10 12 11 10 11 9 11 7 10 5 8 5ZM8 11C7 11 6 10 6 9 6 7 7 6 8 6 9 6 10 7 10 9 10 10 9 11 8 11Z" />
              </g>
            </g>
        </svg>
        PRIVACY
      </button>
    </div>
      </amp-consent>
      
<!--END QUANTCAST CMP -->

	<?php 
}



/*

// aggiunge il tracciamento analytics con dimensione autore
function add_customanalytics_amp(){
	global $post;
	
	
	$author = get_user_by('id', $post->post_author);
	$autore = $author->user_login;
	?>
	<amp-analytics  type="gtag" id="analytics1" data-credentials="include" >
	<script type="application/json">
	{
		"vars":{
			"gtag_id":"UA-11230173-1",
			"config":{
				"UA-11230173-1":{
				"groups":"default"
				}
			}
		},
		"triggers":{
			"trackPageview":{
				"on":"visible",
				"request":"pageview"
			}
		},
		"extraUrlParams":{
			"cd1":"<?php echo $autore; ?>"
			}
	}
	</script>
	</amp-analytics>
	<?php
	}
add_filter('ampforwp_global_after_footer','add_customanalytics_amp');
	
*/

// aggiunge il tracciamento plausible
function add_plausible_amp(){
	?>
	<amp-analytics>
    <script type="application/json">
        {
            "vars": {
                "dataDomain": "secoloditalia.it"
            },
            "requests": {
                "event": "https://realtime.secoloditalia.it/api/event"
            },
            "extraUrlParams": {
                "u": "${sourceUrl}",
                "r": "${documentReferrer}",
                "w": "${viewportWidth}",
                "d": "${dataDomain}"
            },
            "triggers": {
                "trackPageview": {
                    "on": "visible",
                    "request": "event",
                    "extraUrlParams": {
                        "n": "pageview"
                    }
                },
                "trackOutboundLinks": {
                    "on": "click",
                    "request": "event",
                    "selector": "a[href*='//']:not([href*='${sourceHost}'])",
                    "extraUrlParams": {
                        "n": "Outbound Link: Click",
                        "p": "{\"url\":\"${clickUrl}\"}"
                    }
                }
            },
            "transport": {
                "beacon": true,
                "xhrpost": true,
                "image": false,
                "useBody": true
            }
        }
    </script>
</amp-analytics>

<?php
}

	
	
add_filter('ampforwp_global_after_footer','add_plausible_amp');
	

