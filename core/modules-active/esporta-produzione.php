<?php

add_action( 'admin_menu', 'add_statistiche_giornalisti');
function add_statistiche_giornalisti() {
	add_menu_page('Statistiche giornalisti', 'Statistiche giornalisti', 'administrator', 'statistiche_giornalisti', 'statistiche_giornalisti');
}

function get_statistiche_giornalisti($data_inizio, $data_fine) {

	global $wpdb;
	$results_num_articoli = $wpdb->get_results($wpdb->prepare("
		SELECT COUNT(*) AS num_articoli, u.ID AS id_giornalista FROM {$wpdb->prefix}posts p LEFT JOIN {$wpdb->prefix}users u ON p.post_author = u.ID WHERE p.post_status = 'publish' AND p.post_type = 'post' AND DATE(p.post_date) BETWEEN %s AND %s GROUP BY u.ID
		",
		$data_inizio, $data_fine), ARRAY_A);

	$results_autori = $wpdb->get_results($wpdb->prepare("SELECT u.ID AS id, u.user_nicename AS giornalista FROM {$wpdb->prefix}users u"), ARRAY_A);

	$order = [];
	$results = [];
	for($i = 0; $i < count($results_autori); $i++) {
		foreach($results_num_articoli as $r):
			if($r['id_giornalista'] == $results_autori[$i]['id']) {
				if(!empty($r['num_articoli']) && $r['num_articoli'] > 0) {
					$results_autori[$i]['num_articoli'] = $r['num_articoli'];
					$results[] = $results_autori[$i];
					$order[] = $r['num_articoli'];
				}
				break;
			}
		endforeach;

		if(!isset($results_autori[$i]['num_articoli'])) {
			//$order[] = 0;
			//$results_autori[$i]['num_articoli'] = 0;
		}
	}

	array_multisort($order, SORT_DESC, $results);

	//echo $wpdb->last_query;
	//echo $wpdb->last_error;

	return $results;
}

function get_statistiche_articoli_giornalisti($data_inizio, $data_fine) {

	global $wpdb;
	$results_articoli = $wpdb->get_results($wpdb->prepare("
		SELECT p.*, u.ID AS id_giornalista, u.user_nicename AS giornalista, u.display_name AS display_name FROM {$wpdb->prefix}posts p LEFT JOIN {$wpdb->prefix}users u ON p.post_author = u.ID WHERE p.post_status = 'publish' AND p.post_type = 'post' AND DATE(p.post_date) BETWEEN %s AND %s
		",
		$data_inizio, $data_fine), ARRAY_A);

	$results = [];
	foreach($results_articoli as $r):

		$autore = get_field('sign', $r['ID']);
		if(empty($autore)) {
			$autore = $r['display_name'];
		}
		$permalink = get_permalink($r['ID']);
		
		$post_id = $r['ID'];
		$results_category = $wpdb->get_results($wpdb->prepare("
		SELECT name FROM wp_term_relationships INNER JOIN wp_term_taxonomy ON (wp_term_taxonomy.term_taxonomy_id=wp_term_relationships.term_taxonomy_id AND wp_term_taxonomy.taxonomy='category') INNER JOIN wp_terms ON (wp_terms.term_id=wp_term_taxonomy.term_id ) WHERE object_id=%s
		",
		$post_id), ARRAY_A);
		$cat= $results_category[0]['name'];
		
		$results[] = [
			'Data/Ora pubblicazione' => date('d-m-Y H:i', strtotime($r['post_date'])),
			'Data pubblicazione' => date('d-m-Y', strtotime($r['post_date'])),
			'Utente' => $r['giornalista'],
			'Utente visualizzato' => $autore,
			'Titolo' => $r['post_title'],
			'URL' => str_replace("http://www.secoloditalia.it", "", $permalink ),
			'CATEGORIA' => $cat
		];
	endforeach;

	return $results;
}


function statistiche_giornalisti() {

	$data_inizio = date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
	$data_fine = date('Y-m-d', mktime(0, 0, 0, date('m') + 1, 0, date('Y')));

	if(isset($_GET['data_inizio'])) {
		$data_inizio = $_GET['data_inizio'];
	}

	if(isset($_GET['data_fine'])) {
		$data_fine = $_GET['data_fine'];
	}

	$results_num_articoli = get_statistiche_giornalisti($data_inizio, $data_fine);

	?>

		<form method="get" id="statistiche-giornalisti-filtri">

			<label for="data_inizio">Da data</label>
			<input type="date" name="data_inizio" value="<?= $data_inizio ?>">
			<label for="data_fine">A data</label>
			<input type="date" name="data_fine" value="<?= $data_fine ?>">
			<input type="hidden" name="page" value="statistiche_giornalisti">
			<input type="submit" value="Invia">

		</form>

		<form method="get" action="admin-post.php" id="statistiche-giornalisti-export" target="_blank">

			<input type="hidden" name="data_inizio" value="<?= $data_inizio ?>">
			<input type="hidden" name="data_fine" value="<?= $data_fine ?>">
			<input type="hidden" name="action" value="statistiche_giornalisti">
			<input type="submit" value="Esporta statistiche numero articoli">

		</form>

		<form method="get" action="admin-post.php" id="statistiche-articoli-giornalisti-export" target="_blank">

			<input type="hidden" name="data_inizio" value="<?= $data_inizio ?>">
			<input type="hidden" name="data_fine" value="<?= $data_fine ?>">
			<input type="hidden" name="action" value="statistiche_articoli_giornalisti">
			<input type="submit" value="Esporta articoli pubblicati">

		</form>

		<table id="statistiche-giornalisti-table" class="wp-list-table widefat fixed striped posts">
			<thead>
				<tr>
					<th>Giornalista</th>
					<th>Num. articoli</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($results_num_articoli as $num_articoli): ?>
					<tr>
						<td><?= $num_articoli['giornalista'] ?></td>
						<td><?= $num_articoli['num_articoli'] ?></td>
					</tr>
				<?php endforeach ?>
			</tbody>
			<tfoot>
			 
			</tfoot>
		</table>

		<style>
			#statistiche-giornalisti-filtri {
				margin-bottom: 30px;
				margin-top: 30px;
			}
			#statistiche-giornalisti-export {
				margin-bottom: 10px;
			}
			#statistiche-articoli-giornalisti-export {
				margin-bottom: 30px;
			}
		</style>

	<?php
}

add_action( 'admin_post_statistiche_giornalisti', 'statistiche_giornalisti_csv' );
function statistiche_giornalisti_csv()
{

    if ( ! current_user_can( 'manage_options' ) )
        return;

	$data_inizio = date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
	$data_fine = date('Y-m-d', mktime(0, 0, 0, date('m') + 1, 0, date('Y')));

	if(isset($_GET['data_inizio'])) {
		$data_inizio = $_GET['data_inizio'];
	}

	if(isset($_GET['data_fine'])) {
		$data_fine = $_GET['data_fine'];
	}

	//header('Content-Type: application/csv');
	header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename='.$data_inizio.'_'.$data_fine.'.csv');
	header('Pragma: no-cache');

	$results_num_articoli = get_statistiche_giornalisti($data_inizio, $data_fine);

	echo array2csv($results_num_articoli);
}

add_action( 'admin_post_statistiche_articoli_giornalisti', 'statistiche_articoli_giornalisti_csv' );
function statistiche_articoli_giornalisti_csv()
{

    if ( ! current_user_can( 'manage_options' ) )
        return;

	$data_inizio = date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
	$data_fine = date('Y-m-d', mktime(0, 0, 0, date('m') + 1, 0, date('Y')));

	if(isset($_GET['data_inizio'])) {
		$data_inizio = $_GET['data_inizio'];
	}

	if(isset($_GET['data_fine'])) {
		$data_fine = $_GET['data_fine'];
	}

	//header('Content-Type: application/csv');
	header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename='.$data_inizio.'_'.$data_fine.'.csv');
	header('Pragma: no-cache');

	$results_articoli = get_statistiche_articoli_giornalisti($data_inizio, $data_fine);

	echo array2csv($results_articoli);
}

function array2csv(array &$array)
{
	if (count($array) == 0) {
		return null;
	}
	ob_start();
	$df = fopen("php://output", 'w');
	fprintf($df, chr(0xEF).chr(0xBB).chr(0xBF));
	fputcsv($df, array_keys(reset($array)), ',');
	foreach ($array as $row) {
		fputcsv($df, $row, ',');
	}
	fclose($df);
	return ob_get_clean();
}
