<?php

add_action( 'init', 'create_argomenti_taxonomies', 0 );


function create_argomenti_taxonomies() {

	$labels = array(
		'name'                       => _x( 'Argomenti', 'taxonomy general name', 'textdomain' ),
		'singular_name'              => _x( 'Argomenti', 'taxonomy singular name', 'textdomain' ),
		'search_items'               => __( 'Cerca Argomenti', 'textdomain' ),
		'popular_items'              => __( 'Popular Argomenti', 'textdomain' ),
		'all_items'                  => __( 'All Argomenti', 'textdomain' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Argomenti', 'textdomain' ),
		'update_item'                => __( 'Update Argomenti', 'textdomain' ),
		'add_new_item'               => __( 'Aggiungi nuovo tag per Argomenti', 'textdomain' ),
		'new_item_name'              => __( 'New Argomenti Name', 'textdomain' ),
		'separate_items_with_commas' => __( 'Separate Argomenti with commas', 'textdomain' ),
		'add_or_remove_items'        => __( 'Add or remove Argomenti', 'textdomain' ),
		'choose_from_most_used'      => __( 'Choose from the most used Argomenti', 'textdomain' ),
		'not_found'                  => __( 'No Argomenti found.', 'textdomain' ),
		'menu_name'                  => __( 'Argomenti', 'textdomain' ),
	);

	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'argomenti' ),
	);

	register_taxonomy( 'argomenti', 'post', $args );
}



//crea il blocco per la visualizzazione dei correlati nelle pagine AMP

function add_argomenticorelati_amp(){
	global $post;

	 $custom_taxterms = wp_get_object_terms( $post->ID, 'argomenti', array('fields' => 'ids') );
	 
	 // arguments
	$args = array(
	'post_type' => 'post',
	'post_status' => 'publish',
	'posts_per_page' => 5, // you may edit this number
	'orderby' => 'publish_date',
	'order' => 'DESC',
	'tax_query' => array(
		array(
			'taxonomy' => 'argomenti',
			'field' => 'id',
			'terms' => $custom_taxterms
		)
	),
	'post__not_in' => array ($post->ID),
	);

                    $query = new WP_Query($args);

                    if ($query->have_posts()):
						echo"
						<div class=\"related_posts\">
							<ol class=\"clearfix\">
							<span>SULLO STESSO ARGOMENTO</span>
							";
                        while ($query->have_posts()): $query->the_post();

                            ?>
								<li class="has_related_thumbnail">
								<div class="related_link">
									<a href="<?php the_permalink(); ?>amp/"><?php the_title(); ?></a>
			                        
								</div>
								</li>
							
                            <?php
                        endwhile;
                    endif;
                    wp_reset_query();
	
	echo "</ol>
			</div>
		</div>";
	
}
	add_filter('ampforwp_after_post_content','add_argomenticorelati_amp');

?>