<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


add_filter('manage_users_columns', function ( $column ) {
    $column['expiration'] = 'Scadenza Account';
    return $column;
});

add_filter('manage_users_custom_column', function ( $val, $column_name, $user_id ) {

    switch ($column_name) {
        case 'expiration':
            $date = get_field('expiration_subscription', 'user_' . $user_id);
            if (user_can($user_id, 'complete_subscriber') || user_can($user_id, 'web_subscriber')|| user_can($user_id, 'full_subscriber')):
                if (!empty($date)):
                    $color = 'red';
                    if ($date >= date_i18n('Ymd')):
                        $color = 'green';
                    endif;
                    return '<span style="background-color: ' . $color . '; width: 9px; height: 9px; border-radius: 200px; display: inline-block; margin-right: 5px;"></span>' . date_i18n('d/m/Y', strtotime($date));
                else:
                    return '<span style="border: 1px solid black; width: 7px; height: 7px; border-radius: 200px; display: inline-block; margin-right: 5px;"></span>Data mancante';
                endif;
            else:
                return '-';
            endif;
            break;
    }
    return $val;
}, 10, 3);

add_filter('manage_posts_columns', function ( $columns ) {

    /*$new_columns = array(
        'thumbnail' => 'Immagine'
    );*/
    
    $columns['thumbnail'] = 'Immagine';
    
    //array_splice($columns, 2, 0, $new_columns);

    return $columns;
});

add_action('manage_posts_custom_column', function ( $column, $post_id ) {
    global $post;

    switch ($column) {

        /* If displaying the 'duration' column. */
        case 'thumbnail':
            if (has_post_thumbnail($post_id))
                the_post_thumbnail('small', array('style' => 'max-width: 100%; height: auto;'));
            break;
    }
}, 10, 2);
