<?php
add_action('init', function() {
    add_role('web_subscriber', 'Abbonato Web', array('read' => true));
    add_role('complete_subscriber', 'Abbonato Completo', array('read' => true));
	add_role('full_subscriber', 'Abbonato Completo senza adv', array('read' => true));
});
