<?php
add_action( 'init', 'register_cpt_inbreve' );

function register_cpt_inbreve() {

    $labels = array( 
        'name' => _x( 'Comunicati stampa', 'inbreve' ),
        'singular_name' => _x( 'Comunicato stampa', 'inbreve' ),
        'add_new' => _x( 'Nuovo comunicato stampa', 'inbreve' ),
        'add_new_item' => _x( 'Aggiungi nuovo comunicato stampa', 'inbreve' ),
        'edit_item' => _x( 'Modifica comunicato stampa', 'inbreve' ),
        'new_item' => _x( 'Nuovo comunicato stampa', 'inbreve' ),
        'view_item' => _x( 'Visualizza comunicato stampa', 'inbreve' ),
        'search_items' => _x( 'Ricerca comunicato stampa', 'inbreve' ),
        'not_found' => _x( 'Comunicato stampa non trovato', 'inbreve' ),
        'not_found_in_trash' => _x( 'Comunicato stampa non trovato nel cestino', 'inbreve' ),
        'parent_item_colon' => _x( 'Comunicato stampa padre:', 'inbreve' ),
        'menu_name' => _x( 'Comunicati stampa', 'inbreve' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array( 'title', 'editor', 'excerpt', 'author' ),       
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
      	'rewrite' => array('slug' => 'comunicati-stampa', 'with_front' => false, 'pages' => true ),
	    'show_in_nav_menus' => false,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'inbreve', $args );
}

?>