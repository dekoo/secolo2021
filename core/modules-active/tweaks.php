<?php

/*
 * http://wp-snippets.com/articles/7-code-snippets-you-should-use-on-every-site/
 * http://www.gonzoblog.nl/2011/11/15-more-useful-wordpress-hacks-and-code-snippets/
 * http://www.catswhocode.com/blog/10-super-useful-wordpress-shortcodes
 * http://cms.html.it/articoli/leggi/3926/twitter-nellheader-wordpress-senza-plugin/9/
 * http://www.smashingmagazine.com/2010/07/01/10-useful-wordpress-security-tweaks/
 *
 */

// Originally from http://wpengineer.com/1438/wordpress-header/
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);


/**
 * Remove unnecessary dashboard widgets
 *
 * @link http://www.deluxeblogtips.com/2011/01/remove-dashboard-widgets-in-wordpress.html
 */
add_action('admin_init', function () {
    remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
    remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
    remove_meta_box('dashboard_primary', 'dashboard', 'normal');
    remove_meta_box('dashboard_secondary', 'dashboard', 'normal');
});

// login errors / message
add_filter('login_errors', '__return_false');
add_filter('login_messages', '__return_false');



global $user_ID;
if ($user_ID) {
    if (!current_user_can('administrator')) {
        if (strlen($_SERVER['REQUEST_URI']) > 255 ||
                strpos($_SERVER['REQUEST_URI'], "eval(") ||
                strpos($_SERVER['REQUEST_URI'], "CONCAT") ||
                strpos($_SERVER['REQUEST_URI'], "UNION+SELECT") ||
                strpos($_SERVER['REQUEST_URI'], "base64")) {

            @header("HTTP/1.1 414 Request-URI Too Long");
            @header("Status: 414 Request-URI Too Long");
            @header("Connection: Close");
            @exit;
        }
    }
}

/**
 * disable update notification  and admin bar
 * http://www.wpoptimus.com/626/7-ways-disable-update-wordpress-notifications/
 */
if (!current_user_can('administrator')) {
    
    remove_action('load-update-core.php', 'wp_update_plugins');
    
    add_action('init', create_function('$a', "remove_action( 'init', 'wp_version_check' );"), 2);
    add_filter('pre_option_update_core',  '__return_null');
    
    add_filter('pre_site_transient_update_plugins', '__return_null');
    add_filter('pre_site_transient_update_core', '__return_null');
    add_filter('pre_site_transient_update_themes', '__return_null');
    
    
    if (!current_user_can('journalist') && (!current_user_can('editor'))) {
    // disable the admin bar
    add_filter('show_admin_bar', '__return_false');
    }
}


// disable auto ping
function disable_self_ping(&$links) {
    foreach ($links as $l => $link)
        if (0 === strpos($link, get_option('home')))
            unset($links[$l]);
}

add_action('pre_ping', 'disable_self_ping');


/*
 *  disable auto update plugin/template and email 
 * http://make.wordpress.org/core/2013/10/25/the-definitive-guide-to-disabling-auto-updates-in-wordpress-3-7/
 * http://www.siteground.com/tutorials/wordpress/auto-update.htm
 */

add_filter('auto_update_plugin', '__return_false');
add_filter('auto_update_theme', '__return_false');
add_filter('auto_core_update_send_email', '__return_false');

