<?php
/* Carrello con solo un prodotto per volta */
add_action('woocommerce_add_to_cart_validation', function($passed, $product_id, $quantity, $variation_id = '', $variations = '') {
    global $woocommerce;

    if (sizeof($woocommerce->cart->get_cart()) > 0):
        $woocommerce->cart->empty_cart();
    endif;
    return $passed;
}, 99, 5);

/* Aggiornamento abbonamento */
add_action('woocommerce_order_status_completed', function($order_id) {
    $order = wc_get_order($order_id);
    $user_id = $order->get_user_id();
    $args = array('ID' => $user_id);

    foreach ($order->get_items() as $item_id => $item_data):
        $fields = get_fields($item_data->get_product_id());
    endforeach;

    switch ($fields['type_subscription']):
        case 'web':
            $args['role'] = 'web_subscriber';
            break;
        case 'complete':
            $args['role'] = 'complete_subscriber';
            break;
		case 'full':
            $args['role'] = 'full_subscriber';
            break;
    endswitch;

    $user_fields = get_fields('user_' . $user_id);

    if (check_expiration($user_fields['expiration_subscription'])):
        $expiration = date_i18n('Ymd', strtotime($user_fields['expiration_subscription']) + DAY_IN_SECONDS * $fields['time_subscription']);
    else:
        $expiration = date_i18n('Ymd', strtotime(date_i18n('Ymd')) + DAY_IN_SECONDS * $fields['time_subscription']);
    endif;

    wp_update_user($args);
    update_field('expiration_subscription', $expiration, 'user_' . $user_id);
}, 10, 1);

function check_expiration($date)
{
    $active = false;
    if (!empty($date)):
        $today = date_i18n('Ymd');

        if ($today <= $date):
            $active = true;
        endif;
    endif;
    return $active;
}
add_filter('woocommerce_add_to_cart_redirect', function () {
    global $woocommerce;
    $checkout_url = wc_get_checkout_url();
    return $checkout_url;
});

// prima del checkout stampa carrello
add_action('woocommerce_before_checkout_form', function ( ) {
    wc_get_template_part('cart/cart');
}, 9);

// Global redirect to check out when hitting cart page
add_action('template_redirect', function () {

    if (is_cart()) {
        if (WC()->cart->cart_contents_count > 0) {
            // Redirect to check out url
            wp_redirect(wc_get_checkout_url(), '301');
        }
    }return;
    exit;
});

add_filter('woocommerce_checkout_must_be_logged_in_message', function() {
    return ''; //"Per completare l'ordine devi <a href='" . wp_login_url('/') . "'>accedere</a> oppure <a href='" . wp_registration_url() . "'>registrarti</a>";
});

add_action('woocommerce_before_checkout_form', function() {
    if (!is_user_logged_in()):
        echo "Per completare l'ordine devi <a href='" . wp_login_url('/checkout/') . "'>accedere</a> oppure <a href='" . wp_registration_url() . "'>registrarti</a>";
    endif;
});

add_filter('wc_add_to_cart_message_html', '__return_null');

add_action('template_redirect', function () {
    if (is_cart() && WC()->cart->cart_contents_count == 0):
        wp_redirect('/abbonamenti/', '301');
        exit;
    endif;
}, 1);

function get_paypal_order($raw_custom)
{
    $custom = json_decode($raw_custom);
    if ($custom && is_object($custom)) {
        $order_id = $custom->order_id;
        $order_key = $custom->order_key;
    } else {
        return false;
    }
    $order = wc_get_order($order_id);
    if (!$order) {
        $order_id = wc_get_order_id_by_order_key($order_key);
        $order = wc_get_order($order_id);
    }
    if (!$order || $order->get_order_key() !== $order_key) {
        return false;
    }
    return $order;
}

function update_wc_order_status($posted)
{
    $order = !empty($posted['custom']) ? get_paypal_order($posted['custom']) : false;
    if ($order) {
        $posted['payment_status'] = strtolower($posted['payment_status']);
        if ('completed' === $posted['payment_status']) {
            $order->add_order_note(__('IPN payment completed', ''));
            $order->payment_complete(!empty($posted['txn_id']) ? wc_clean($posted['txn_id']) : '' );
        }
    }
}
add_action('paypal_ipn_for_wordpress_payment_status_completed', 'update_wc_order_status', 10, 1);

add_action('woocommerce_order_status_processing', function($order_id) {
    $order = wc_get_order($order_id);
    $order->update_status('completed');
});
