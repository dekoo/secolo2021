<?php

class SingletonSession
{

    private static $instance = null;
    private $theme_fields;

    private function __construct()
    {
        $this->theme_fields = get_fields('options');
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            $c = __CLASS__;
            self::$instance = new $c;
        }

        return self::$instance;
    }

    function getTheme_fields()
    {
        return $this->theme_fields;
    }
}

add_action('init', function() {
    SingletonSession::getInstance();
});
