<?php

function order_to_gest_completed($order_id)
{

    file_put_contents('log.txt', date('H:i:s') . 'order_to_gest_completed ' . $order_id, FILE_APPEND);

    $order = new WC_Order($order_id);
    $user = $order->get_user();
    $order_meta = get_post_meta($order_id);

    $data = [];
    $data['order'] = [];

    $data['order']['id'] = $order_id;
    $data['order']['_paid_date'] = $order->date_completed;
    $data['order']['_completed_date'] = $order->date_paid;
    $data['order']['_payment_method'] = $order_meta['_payment_method'][0];
    $data['order']['note'] = 'order_to_gest_completed';
    
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://gestionale.secoloditalia.it/ordini/woocommerce_completed',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array('data' => json_encode($data)),
    ));

    $response = curl_exec($curl);

    print_r($response);
    die();

    curl_close($curl);

    die();
}
//add_action('woocommerce_order_status_completed', 'order_to_gest_completed', 99, 3);

function order_to_gest($order_id, $old_status, $new_status)
{

    file_put_contents('log.txt', date('H:i:s') . 'order_to_gest ' . $order_id, FILE_APPEND);

    $order = new WC_Order($order_id);
    $user = $order->get_user();
    $order_meta = get_post_meta($order_id);

    $data = [];
    $data['order'] = [];
    $data['order_items'] = [];
    $data['user'] = [];

    $data['order']['id'] = $order_id;
    $data['order']['post_status'] = $new_status;
    $data['order']['post_date'] = $order->date_created;
    $data['order']['_paid_date'] = $order->date_completed;
    $data['order']['_completed_date'] = $order->date_paid;
    $data['order']['_payment_method'] = $order_meta['_payment_method'][0];
    $data['order']['note'] = 'order_to_gest ' . $old_status.' '.$new_status;

    $data['user']['id'] = $order_meta['_customer_user'][0];
    $data['user']['_billing_first_name'] = $order_meta['_billing_first_name'][0];
    $data['user']['_billing_last_name'] = $order_meta['_billing_last_name'][0];
    $data['user']['_billing_email'] = $order_meta['_billing_email'][0];
    $data['user']['_billing_phone'] = $order_meta['_billing_phone'][0];
    $data['user']['_billing_address_1'] = $order_meta['_billing_address_1'][0];
    $data['user']['_billing_city'] = $order_meta['_billing_city'][0];
    $data['user']['_billing_postcode'] = $order_meta['_billing_postcode'][0];
    $data['user']['_billing_state'] = $order_meta['_billing_state'][0];
    
    if(!empty($order_meta['_billing_company']))
    {
        $data['user']['_billing_company'] = $order_meta['_billing_company'][0];
    }

    $order_items = $order->get_items();
  
    foreach($order_items as $item_key => $item):
        $data['order_items'][] = [
            'order_item_name' => $item['name']
        ];
    endforeach;
    
    

    if($new_status == 'completed')
    {

        if($data['order']['_payment_method'] == 'paypal')
        {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://gestionale.secoloditalia.it/ordini/woocommerce_created',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('data' => json_encode($data)),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://gestionale.secoloditalia.it/ordini/woocommerce_completed',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('data' => json_encode($data)),
        ));
        $response = curl_exec($curl);
        curl_close($curl);

    } else if($new_status == 'processing' && $data['order']['_payment_method'] == 'paypal') {

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://gestionale.secoloditalia.it/ordini/woocommerce_created',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('data' => json_encode($data)),
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://gestionale.secoloditalia.it/ordini/woocommerce_completed',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('data' => json_encode($data)),
        ));
        $response = curl_exec($curl);
        curl_close($curl);

    } else {

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://gestionale.secoloditalia.it/ordini/woocommerce_created',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('data' => json_encode($data)),
        ));
        $response = curl_exec($curl);
        curl_close($curl);

    }

}
add_action('woocommerce_order_status_changed', 'order_to_gest', 99, 3);



function mysite_woocommerce_payment_complete( $order_id ) {
    
    $order = new WC_Order($order_id);
    $user = $order->get_user();
    $order_meta = get_post_meta($order_id);

    $data = [];
    $data['order'] = [];
    $data['order_items'] = [];
    $data['user'] = [];

    $data['order']['id'] = $order_id;
    $data['order']['post_status'] = $new_status;
    $data['order']['post_date'] = $order->date_created;
    $data['order']['_paid_date'] = $order->date_completed;
    $data['order']['_completed_date'] = $order->date_paid;
    $data['order']['_payment_method'] = $order_meta['_payment_method'][0];

    $data['user']['id'] = $order_meta['_customer_user'][0];
    $data['user']['_billing_first_name'] = $order_meta['_billing_first_name'][0];
    $data['user']['_billing_last_name'] = $order_meta['_billing_last_name'][0];
    $data['user']['_billing_email'] = $order_meta['_billing_email'][0];
    $data['user']['_billing_phone'] = $order_meta['_billing_phone'][0];
    $data['user']['_billing_address_1'] = $order_meta['_billing_address_1'][0];
    $data['user']['_billing_city'] = $order_meta['_billing_city'][0];
    $data['user']['_billing_postcode'] = $order_meta['_billing_postcode'][0];
    $data['user']['_billing_state'] = $order_meta['_billing_state'][0];
    
    if(!empty($order_meta['_billing_company']))
    {
        $data['user']['_billing_company'] = $order_meta['_billing_company'][0];
    }

    $order_items = $order->get_items();
  
    foreach($order_items as $item_key => $item):
        $data['order_items'][] = [
            'order_item_name' => $item['name']
        ];
    endforeach;
    
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://gestionale.secoloditalia.it/ordini/woocommerce_created',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array('data' => json_encode($data)),
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://gestionale.secoloditalia.it/ordini/woocommerce_completed',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array('data' => json_encode($data)),
    ));
    
    $response = curl_exec($curl);

    curl_close($curl);

}
add_action( 'woocommerce_pre_payment_complete', 'mysite_woocommerce_payment_complete', 10, 1 );