<?php

add_action('customize_register', function ( $wp_customize ) {


    //$wp_customize->remove_section('title_tagline');
    //$wp_customize->remove_section('blogdescription');

    $wp_customize->remove_section('header_image'); // solo se theme_support -> custom-header
    $wp_customize->remove_section('colors');
    $wp_customize->remove_section('background_image');

    $wp_customize->remove_section('nav');
    $wp_customize->remove_section('static_front_page');
    $wp_customize->remove_panel('widgets');



    $wp_customize->add_panel('pannello', array(
        'title' => "prova pannello",
        'description' => "", // Include html tags such as <p>.
        'priority' => 160, // Mixed with top-level-section hierarchy.
    ));


    /*
     * banner
     */

    $wp_customize->add_section("ads", array(
        "title" => "Banner",
        "priority" => 30,
    ));


    $wp_customize->add_setting("head_advscript", array(
        "default" => "",
    ));

    $wp_customize->add_setting("banner_top", array(
        "default" => "",
    ));

    $wp_customize->add_setting("banner_logo", array(
        "default" => "",
    ));

    $wp_customize->add_setting("banner_featured", array(
        "default" => "",
    ));

    $wp_customize->add_setting("banner_single_before", array(
        "default" => "",
    ));

    $wp_customize->add_setting("banner_single_after", array(
        "default" => "",
    ));

    $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize, "head_advscript", array(
        "label" => "Head ADV Script",
        "section" => "ads",
        "settings" => "head_advscript",
        "type" => "textarea",
            )
    ));



    $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize, "banner_top", array(
        "label" => "Banner TOP",
        "section" => "ads",
        "settings" => "banner_top",
        "type" => "textarea",
            )
    ));


    $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize, "banner_logo", array(
        "label" => "Banner LOGO",
        "section" => "ads",
        "settings" => "banner_logo",
        "type" => "textarea",
            )
    ));

    $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize, "banner_featured", array(
        "label" => "Banner Featured",
        "section" => "ads",
        "settings" => "banner_featured",
        "type" => "textarea",
            )
    ));


    $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize, "banner_single_before", array(
        "label" => "Banner Single Before",
        "section" => "ads",
        "settings" => "banner_single_before",
        "type" => "textarea",
            )
    ));


    $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize, "banner_single_after", array(
        "label" => "Banner Single After",
        "section" => "ads",
        "settings" => "banner_single_after",
        "type" => "textarea",
            )
    ));

    /*
     * tags
     */

    $wp_customize->add_section("fotogallery", array(
        "title" => "Foto Gallery",
        "description" => "Selezionare la pagina padre",
        "priority" => 50,
    ));

    $wp_customize->add_setting("parent_id", array(
        "default" => "",
    ));

    $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize, "parent_id", array(
        "label" => "Pagina Padre",
        "section" => "fotogallery",
        "settings" => "parent_id",
        "type" => "dropdown-pages",
            )
    ));



    /*
     * tags
     */

    $wp_customize->add_section("tags", array(
        "title" => "TAGS",
        "description" => "Inserire i tag separati da una virgola",
        "priority" => 50,
    ));

    $wp_customize->add_setting("tags", array(
        "default" => "",
    ));

    $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize, "tags", array(
        "label" => "Elenco Tags",
        "section" => "tags",
        "settings" => "tags",
        "type" => "textarea",
            )
    ));

    /*
     * social
     */



    $wp_customize->add_section("social", array(
        "title" => "Social",
        "priority" => 50,
    ));

    $wp_customize->add_setting("link_facebook", array(
        "default" => "",
    ));

    $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize, "link_facebook", array(
        "label" => "Facebook",
        "section" => "social",
        "settings" => "link_facebook",
        "type" => "url",
            )
    ));


    $wp_customize->add_setting("link_twitter", array(
        "default" => "",
    ));

    $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize, "link_twitter", array(
        "label" => "Twitter",
        "section" => "social",
        "settings" => "link_twitter",
        "type" => "url",
            )
    ));


    $wp_customize->add_setting("link_gplus", array(
        "default" => "",
    ));

    $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize, "link_gplus", array(
        "label" => "Google Plus",
        "section" => "social",
        "settings" => "link_gplus",
        "type" => "url",
            )
    ));
    
        $wp_customize->add_setting("link_youtube", array(
        "default" => "",
    ));

    $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize, "link_youtube", array(
        "label" => "YouTube",
        "section" => "social",
        "settings" => "link_youtube",
        "type" => "url",
            )
    ));


    $wp_customize->add_setting("link_pinterest", array(
        "default" => "",
    ));

    $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize, "link_pinterest", array(
        "label" => "Pinterest",
        "section" => "social",
        "settings" => "link_pinterest",
        "type" => "url",
            )
    ));


    /*     * * /
     * 
     * 
     */


    $wp_customize->add_section("featured", array(
        "title" => "Featured",
        "priority" => 50,
    ));

    $wp_customize->add_setting("featured_1", array(
        "default" => "",
    ));

    $wp_customize->add_control(new PostDropdown(
            $wp_customize, "featured_1", array(
        "label" => "Articoli in evidenza #1",
        "section" => "featured",
        "settings" => "featured_1",
            )
    ));

    $wp_customize->add_setting("featured_2", array(
        "default" => "",
    ));

    $wp_customize->add_control(new PostDropdown(
            $wp_customize, "featured_2", array(
        "label" => "Articoli in evidenza #2",
        "section" => "featured",
        "settings" => "featured_2",
            )
    ));

    $wp_customize->add_setting("featured_3", array(
        "default" => "",
    ));

    $wp_customize->add_control(new PostDropdown(
            $wp_customize, "featured_3", array(
        "label" => "Articoli in evidenza #3",
        "section" => "featured",
        "settings" => "featured_3",
            )
    ));



    /*
     * footer
     */

    $wp_customize->add_section("footer", array(
        "title" => "Footer",
        "priority" => 50,
    ));

    $wp_customize->add_setting("note", array(
        "default" => "",
    ));

    $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize, "facebook", array(
        "label" => "Note",
        "section" => "footer",
        "settings" => "note",
        "type" => "textarea",
            )
    ));


    $wp_customize->add_setting("copyright", array(
        "default" => "",
    ));

    $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize, "copyright", array(
        "label" => "Copyright",
        "section" => "footer",
        "settings" => "copyright",
        "type" => "textarea",
            )
    ));
});
