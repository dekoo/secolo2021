<?php
add_filter('the_author', function($name) {
    if (is_feed() && !is_admin()):
        global $post;
        $sign = get_field('sign', $post->ID);
        if ($sign):
            $name = $sign;
        endif;
    endif;
    return $name;
});
