<?php

add_action( 'rest_api_init', 'add_thumbnails_to_JSON' );
function add_thumbnails_to_JSON() {
    
    register_rest_field( 
        'post', 
        'featured_thumbnail_src', 
        array(
            'get_callback'    => 'get_image_src',
            'update_callback' => null,
            'schema'          => null,
             )
        );

    register_rest_field( 
        'post', 
        'featured_medium_src', 
        array(
            'get_callback'    => 'get_image_medium_src',
            'update_callback' => null,
            'schema'          => null,
             )
        );

    register_rest_field( 
        'post', 
        'category_name', 
        array(
            'get_callback'    => 'get_rest_category_name',
            'update_callback' => null,
            'schema'          => null,
             )
        );
}

function get_image_src( $object, $field_name, $request ) {
  $feat_img_array = wp_get_attachment_image_src(
    $object['featured_media'], 
    'thumbnail',  
    true 
  );
  return $feat_img_array[0];
}

function get_image_medium_src( $object, $field_name, $request ) {
  $feat_img_array = wp_get_attachment_image_src(
    $object['featured_media'], 
    'large',  
    true 
  );
  return $feat_img_array[0];
}



add_action( 'rest_api_init', 'add_author_name_to_JSON' );
function add_author_name_to_JSON() {
    
    register_rest_field( 
        'post', 
        'author_name', 
        array(
            'get_callback'    => 'get_rest_author_fullname',
            'update_callback' => null,
            'schema'          => null,
             )
        );
}

function get_rest_author_fullname( $object, $field_name, $request ) {
    //$author_name = get_the_author_meta('display_name',$object['author']);
	$author_name = get_post_meta($object['id'],'sign',true);
    return $author_name;
}

function get_rest_category_name( $object, $field_name, $request ) {
    $category_name = '';
    if (count($object['categories']))
        $category_name = get_cat_name($object['categories'][0]);
    return $category_name;
}
