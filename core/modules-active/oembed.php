<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

add_filter('embed_oembed_html', function ( $output, $data, $url ) {

    $return = '<div class="video">' . $output . '<br/></div>';
    return $return;
}, 99, 3);


add_filter('oembed_result', function ($data, $url, $args = array()) {
 $data = preg_replace('/(youtube\.com.*)(\?feature=oembed)(.*)/', '$1?' . apply_filters("hyrv_extra_querystring_parameters", "wmode=transparent&amp;") . 'rel=0$3', $data);
 return $data;
} , 10, 3);