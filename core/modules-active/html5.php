<?php

/*
 * Enable support for HTML5 markup.

 */

add_action('after_setup_theme', function() {

    add_theme_support('html5', array(
        'comment-list',
        'search-form',
        'comment-form',
        'gallery',
    ));
});
