<?php




// aggiunto filtro per rimozione slash    
// Returns the title based on what is being viewed

/** da verificare bug con wpseo + mqtranslate **/


add_filter("wp_title", function( $title) {
    
    if (is_single() || is_page()) { // single posts
        //$title = single_post_title("", false) . ' | ' . get_bloginfo('name');

        // The home page or, if using a static front page, the blog posts page.
    } elseif (is_home() || is_front_page()) {
        /*$title = get_bloginfo('name');
        if (get_bloginfo('description'))
            $title .= ' | ' . get_bloginfo('description');*/
    } elseif (is_search()) { // Search results
        $title = sprintf(__('Search results for %s'), '"' . get_search_query() . '"');
        ' | ' . get_bloginfo('name');
    } elseif (is_404()) {  // 404 (Not Found)
        $title = _('Not Found') . ' | ' . get_bloginfo('name');
    } else { // Otherwise:
    }
    
    return stripslashes($title);
}, 999);
