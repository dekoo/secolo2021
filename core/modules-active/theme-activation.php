<?php

/**
 * set default after themes activation
 */
if (is_admin() && isset($_GET['activated']) && $pagenow == 'themes.php') {

    update_option('siteurl', $_SERVER['HTTP_HOST']);
    update_option('home', $_SERVER['HTTP_HOST']);
    update_option('posts_per_page', 10);
    update_option('date_format', 'j F Y');
    update_option('permalink_structure', '/%postname%/');


    $menu_name = "main";
    // Check if the menu exists
    $menu_exists = wp_get_nav_menu_object($menu_name);

    if (!$menu_exists) {
        $menu_id = wp_create_nav_menu($menu_name);

        // Set up default menu items
        wp_update_nav_menu_item($menu_id, 0, array(
            'menu-item-title' => __('Home'),
            'menu-item-classes' => 'home',
            'menu-item-url' => "/",
            'menu-item-status' => 'publish'));

       
    }


    /*
     * custom upload options
     * update_option('uploads_use_yearmonth_folders', 0);
     * update_option('upload_path', WP_UPLOAD_DIR );
     * update_option('upload_url_path', '/files');

     */
}
    
