<?php
if (!function_exists('setup_post_types')) {

    /**
     *
     * @param string $typename custom type name
     * @param array $args optional
     * @param array $supports optional
     * @param array $labels  optional
     */
    function setup_post_type($typename, $args = null, $supports = null, $labels = null)
    {
        if (isset($typename)) {

            $defaults_labels = array(
                'name' => __(ucfirst($typename), ''),
                'singular_name' => __(ucfirst($typename)),
                'add_new' => __('Aggiungi Nuovo', ''),
                'add_new_item' => __('Aggiungi Nuovo ' . ucfirst($typename)),
                'edit_item' => __('Modifica ' . ucfirst($typename)),
                'new_item' => __('Nuovo ' . ucfirst($typename)),
                'view_item' => __('Visualizza ' . ucfirst($typename)),
                'search_items' => __('Cerca ' . ucfirst($typename)),
                'not_found' => __('Nessun ' . ucfirst($typename) . ' Trovato'),
                'not_found_in_trash' => __('Nessun ' . ucfirst($typename) . ' Trovato nel Cestino'),
                'parent_item_colon' => '',
                'menu_name' => ucfirst($typename)
            );

            $labels = wp_parse_args($labels, $defaults_labels);

            $defaults_supports = array(
                'title',
                'editor',
                'author',
                'thumbnail',
                'excerpt',
                'trackbacks',
                'custom-fields',
                'comments',
                'revisions',
                'page-attributes',
                'post-formats'
            );

            if (!isset($supports))
                $supports = $defaults_supports;

            $defaults_args = array(
                'labels' => $labels,
                'supports' => $supports,
                'description' => '',
                'public' => true,
                'publicly_queryable' => true,
                'exclude_from_search' => false,
                'show_ui' => true,
                'show_in_menu' => true,
                'menu_position' => 5,
                'menu_icon' => null,
                'capability_type' => 'post',
                'hierarchical' => true,
                'taxonomies' => array(),
                'has_archive' => true,
                'rewrite' => array(
                    'slug' => '',
                    'with_front' => true,
                    'feeds' => true,
                    'pages' => true
                ),
                'query_var' => true,
                'can_export' => true,
                'show_in_nav_menus' => true,
                '_edit_link' => 'post.php?post=%d'
            );

            $args = wp_parse_args($args, $defaults_args);

            if (isset($args->rewrite)):
                $args->rewrite = wp_parse_args($args->rewrite, $defaults_args['rewrite']);
            endif;

            register_post_type($typename, $args);
        }
    }
// end functin
} // end if