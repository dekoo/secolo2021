<?php
if (!function_exists('setup_theme_taxonomies')) {

    function setup_theme_taxonomiy($taxname, $types, $args = null, $labels = null)
    {
        $defaults_labels = array(
            'name' => __(ucfirst($taxname)),
            'singular_name' => __(ucfirst($taxname)),
            'search_items' => __('Cerca ' . ucfirst($taxname)),
            'all_items' => __('Tutti ' . ucfirst($taxname)),
            'parent_item' => __('Padre' . ucfirst($taxname)),
            'parent_item_colon' => __('Padre ' . ucfirst($taxname) . ':'),
            'edit_item' => __('Modifica' . ucfirst($taxname)),
            'update_item' => __('Aggiorna' . ucfirst($taxname)),
            'add_new_item' => __('Aggiungi nuovo' . ucfirst($taxname)),
            'new_item_name' => __('Nuovo ' . ucfirst($taxname)),
            'menu_name' => __(ucfirst($taxname))
        );

        $labels = wp_parse_args($labels, $defaults_labels);

        $defaults_args = array(
            'hierarchical' => true,
            'show_admin_column' => true,
            'labels' => $labels,
            'show_ui' => true,
            'query_var' => true,
            'has_archive' => true,
            'rewrite' => array(
                'slug' => $taxname
            )
        );

        if (isset($args->rewrite)):
            $args->rewrite = wp_parse_args($args->rewrite, $defaults_args['rewrite']);
        endif;

        $args = wp_parse_args($args, $defaults_args);

        register_taxonomy($taxname, $types, $args);
    }
// end funtion
} // end if
    
	


