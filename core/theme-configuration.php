{
    "autoload_module":          true,
    "autoload_css":             false,    
    "autoload_js":              false,    
    "autoload_widget":          true,

    
    "custom_type":              {
                                "vignette": {
                                                "labels": {
                                                    "name": "Vignette",
                                                    "singular_name": "Vignetta",
                                                    "menu_name": "Vignette",
                                                    "name_admin_bar": "Vignetta",
                                                    "add_new": "Aggiungi Nuova",
                                                    "add_new_item": "Aggiungi Nuova Vignetta",
                                                    "new_item": "Nuova Vignetta",
                                                    "edit_item": "Modifica Vignetta",
                                                    "view_item": "Vedi Vignetta",
                                                    "all_items": "Tutte le Vignette",
                                                    "search_items": "Cerca Vignette",
                                                    "parent_item_colon": "Vignette Genitori:",
                                                    "not_found": "Nessuna Vignetta trovata",
                                                    "not_found_in_trash": "Nessuna Vignetta trovata nel Cestino"
                                                },
                                                "args": {}
                                                },
                                "quotidiano": {
                                                "labels": {
                                                    "name": "Giornali",
                                                    "singular_name": "Giornale",
                                                    "menu_name": "Giornali",
                                                    "name_admin_bar": "Giornale",
                                                    "add_new": "Aggiungi Nuovo",
                                                    "add_new_item": "Aggiungi Nuovo Giornale",
                                                    "new_item": "Nuovo Giornale",
                                                    "edit_item": "Modifica Giornale",
                                                    "view_item": "Vedi Giornale",
                                                    "all_items": "Tutti i Giornali",
                                                    "search_items": "Cerca Giornali",
                                                    "parent_item_colon": "Giornali Genitori:",
                                                    "not_found": "Nessun Giornale trovata",
                                                    "not_found_in_trash": "Nessun Giornale trovato nel Cestino"
                                                },
                                                "args": {}
                                                },
                                "widget":   {}
                                },
    "custom_taxonomies":        {
                                "posizione-editoriale": {
                                                        "types": ["post"],
                                                        "labels": {},
                                                        "args": {}
                                                        },
                                "tassonomia-esempio-2": {
                                                        "types": ["tipo-esempio"]
                                                        }
                                },
    "nav_menu":                 [
                                ["top","Top Menu"],
                                ["main","Principale"],
                                ["footer","Footer"]                          
                                ],
    
    "widget_areas":             [
                                ["newsletter","Newsletter","Sezione newsletter delle sidebar",""],
                                ["bottom-sidebar","Pedice sidebar","Pedice delle sidebar, usato su tutte le pagine",""],
                                ["bottom-single","Pedice articolo","Pedice al fondo degli articoli",""]
                                ],
    
    "debug":                    true                                    
                                
}