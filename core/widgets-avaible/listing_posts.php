<?php
/*
  Plugin Name: Posts List
  Plugin URI: http://wordpress.org/extend/plugins/
  Description: Show the titles' list of the chosen post type.
  Author: Mavida S.n.c.
  Version: 1.0
  Author URI: http://www.mavida.com
 */

class listing_posts extends WP_Widget {

    /**
     * Construct the Widget
     */
    public function __construct() {
        parent::__construct(
			'listing_posts', // Base ID
			__('Posts title listing', 'secoloditalia'), // Name
			array( 'description' => __( 'Simple list using <ul><li> ', 'secoloditalia' ), ) // Args
		);
    }

    /**
     * Save widget settings
     * 
     * @param type $new_instance
     * @param type $old_instance
     * @return type
     */
    function update($new_instance, $old_instance) {

        $instance = $old_instance;
        $instance['post_type'] = strip_tags($new_instance['post_type']);
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['order_by'] = strip_tags($new_instance['order_by']);
        $instance['order'] = strip_tags($new_instance['order']);
        $instance['post_per_page'] = strip_tags($new_instance['post_per_page']);

        return $instance;
    }

    /**
     * Process widget's output
     * 
     * @param type $args
     * @param type $instance
     */
    public function widget($args, $instance) {


        $instance['post_type'] = !empty($instance['post_type']) ? $instance['post_type'] : false;
        $instance['title'] = !empty($instance['title']) ? $instance['title'] : false;
        $instance['order_by'] = !empty($instance['order_by']) ? $instance['order_by'] : false;
        $instance['order'] = !empty($instance['order']) ? $instance['order'] : false;
        $instance['post_per_page'] = !empty($instance['post_per_page']) ? $instance['post_per_page'] : false;

        if (!is_bool($instance['post_type'])) {
            $this->render_posts_list($args, $instance['post_type'], $instance['title'], $instance['order_by'], $instance['order'], $instance['post_per_page']);
        }
    }

    /**
     * Print the list
     * 
     */
    function render_posts_list($args, $post_type, $title, $order_by, $order, $post_per_page) {

        extract($args);

        $output = $before_widget;

        if ($title != '') {
            $output.= $before_title . $title . $after_title;
        }

        $output .= '<ul class="posts-list-widget">';
        
        $args = array(
            'post_type' => $post_type,
            'order_by' => $order_by,
            'posts_per_page' => $post_per_page
        );
        
        if($order != 'none'){
            $args['order'] = $order;
        }

        $the_query = new WP_Query($args);
        
        if ($the_query->have_posts()) {
            while ($the_query->have_posts()) {
                $the_query->the_post();

                $post_title = get_the_title();
                $post_permalink = get_the_permalink();

                $post_title = apply_filters('the_title', $post_title);
                $post_permalink = apply_filters('the_permalink', $post_permalink);
                
                $output.= '<li><a href="' . $post_permalink . '">' . $post_title . '</a></li>';
            }
        }

        wp_reset_postdata();

        $output .= '</ul>';
        $output .= $after_widget;

        echo $output;
    }

    /**
     * Widget's Admin options form
     * 
     * @param type $instance
     */
    function form($instance) {

        $post_type = !empty($instance['post_type']) ? esc_attr($instance['post_type']) : '';
        $title = !empty($instance['title']) ? esc_attr($instance['title']) : "";
        $order_by = !empty($instance['order_by']) ? esc_attr($instance['order_by']) : "none";
        $order = !empty($instance['order']) ? esc_attr($instance['order']) : "none";
        $post_per_page = !empty($instance['post_per_page']) ? esc_attr($instance['post_per_page']) : "-1";
        
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Inserisci il titolo della sezione'); ?></label>
            <input name="<?php echo $this->get_field_name('title'); ?>" id="<?php echo $this->get_field_id('title'); ?>" type="text" value="<?php echo $title; ?>" >
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('post_type'); ?>"><?php _e('Seziona il tipo'); ?></label>
            <select name="<?php echo $this->get_field_name('post_type'); ?>" id="<?php echo $this->get_field_id('post_type'); ?>" class="widefat">
                <?php
                $args = array(
                    'public' => true,
                    '_builtin' => false
                );

                $options = get_post_types($args, 'names');

                array_unshift($options, 'page');
                array_unshift($options, 'post');

                foreach ($options as $option) {
                    echo '<option value="' . $option . '" id="type-' . $option . '"', $post_type == $option ? ' selected="selected"' : '', '>', apply_filters('the_title', $option), '</option>';
                }
                ?>
            </select>
        </p>
        
        <p>
            <label for="<?php echo $this->get_field_id('order_by'); ?>"><?php _e('Scegli per che cosa ordinare'); ?></label>
            <select name="<?php echo $this->get_field_name('order_by'); ?>" id="<?php echo $this->get_field_id('order_by'); ?>" class="widefat">
                <?php
                $options = array('none', 'ID', 'author', 'title', 'name', 'date', 'modified', 'parent', 'rand', 'comment_count', 'menu_order');

                foreach ($options as $option) {
                    echo '<option value="' . $option . '" id="order-by-' . $option . '"', $order_by == $option ? ' selected="selected"' : '', '>', $option, '</option>';
                }
                ?>
            </select>
        </p>
        
        <p>
            <label for="<?php echo $this->get_field_id('order'); ?>"><?php _e('Scegli come ordinare'); ?></label>
            <select name="<?php echo $this->get_field_name('order'); ?>" id="<?php echo $this->get_field_id('order'); ?>" class="widefat">
                <?php
                $options = array('none', 'ASC', 'DESC');

                foreach ($options as $option) {
                    echo '<option value="' . $option . '" id="order-' . $option . '"', $order == $option ? ' selected="selected"' : '', '>', $option, '</option>';
                }
                ?>
            </select>
        </p>
        
        <p>
            <label for="<?php echo $this->get_field_id('post_per_page'); ?>"><?php _e('Scegli quanti titoli ordinare'); ?></label>
            <input name="<?php echo $this->get_field_name('post_per_page'); ?>" id="<?php echo $this->get_field_id('post_per_page'); ?>" type="number" value="<?php echo $post_per_page; ?>" min="-1" step="1" >
        </p>
        <?php
    }
}