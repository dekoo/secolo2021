<?php
/*
  Plugin Name: Widget Content
  Plugin URI: http://wordpress.org/extend/plugins/
  Description: Show widget CPT for some blog.
  Author: Mavida S.n.c.
  Version: 1.0
  Author URI: http://www.mavida.com
 */

class widgets_widget_content extends WP_Widget {

    /**
     * Construct the Widget
     */
    public function __construct() {
        parent::__construct(false, $name = 'Contenuto Tipo Widget');
    }

    /**
     * Save widget settings
     * 
     * @param type $new_instance
     * @param type $old_instance
     * @return type
     */
    function update($new_instance, $old_instance) {

        $instance = $old_instance;
        $instance['widget_id'] = strip_tags($new_instance['widget_id']);
        $instance['show_title'] = strip_tags($new_instance['show_title']);

        return $instance;
    }

    /**
     * Process widget's output
     * 
     * @param type $args
     * @param type $instance
     */
    public function widget($args, $instance) {

        extract($args);

        //echo $before_widget;
        $instance['widget_id'] = !empty($instance['widget_id']) ? $instance['widget_id'] : false;
        $instance['show_title'] = !empty($instance['show_title']) ? $instance['show_title'] : false;

        if (!is_bool($instance['widget_id'])) {
            $this->render_page_content($instance['widget_id'],$instance['show_title']);
        }

        //echo $after_widget;

        echo $widget;
    }

    /**
     * Get popular posts
     * 
     */
    function render_page_content($widget_id,$show_title) {

        $output = '';

		$args=array(
			'p' => $widget_id,
			'post_type' => 'widget'
		);
                
		$the_query = new WP_Query($args);
		
		if ($the_query->have_posts()){
			while ($the_query->have_posts()) {
				$the_query->the_post();

                                $title=get_the_title();
                                $title=$title = str_replace("Footer &#8211; ", "", $title);
                                $title=$title = str_replace("Sidebar &#8211; ", "", $title);
                                
				$title = apply_filters('the_title',$title);
				$content = apply_filters('the_content',get_the_content());
				
				if($content){
					$output.= "<div class='widget-content-wrapper content-".$widget_id."'>";
                                        
                                        if($show_title==true){$output.= "<h3 class='widget-content-title'>" . $title . "</h5>";}
					
					$output.= "<div class='widget-content-content'>" . $content . "</div>";
					$output.= "</div>";
				}
				
			}
		}

		wp_reset_postdata();
				
		echo $output;
	}

    /**
     * Widget's Admin options form
     * 
     * @param type $instance
     */
    function form($instance) {

        $page_id = !empty($instance['widget_id']) ? esc_attr($instance['widget_id']) : '';
        $checked_title = !empty($instance['show_title']) ? ' checked="checked" '  : "";
        
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('widget_id'); ?>"><?php _e('Seziona la pagina'); ?></label>
            <select name="<?php echo $this->get_field_name('widget_id'); ?>" id="<?php echo $this->get_field_id('widget_id'); ?>" class="widefat">
        <?php
        $options = get_posts(array('showposts' => -1, 'post_type' => 'widget', 'post_status' => 'publish'));

        foreach ($options as $option) {
            echo '<option value="' . $option->ID . '" id="blog-' . $option->ID . '"', $page_id == $option->ID ? ' selected="selected"' : '', '>', apply_filters('the_title', $option->post_title), '</option>';
        }
        ?>
            </select>
        </p>
        
        <p>
            <label for="<?php echo $this->get_field_id('show_title'); ?>"><?php _e('Visualizza titolo'); ?></label>
            <input name="<?php echo $this->get_field_name('show_title'); ?>" id="<?php echo $this->get_field_id('show_title'); ?>" type="checkbox" <?php echo $checked_title; ?> >
        </p>
                <?php
            }

        }
        