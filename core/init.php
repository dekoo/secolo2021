<?php

    function autoloader($class_name) {
        $class_path = TEMPLATEPATH . '/core/classes/' . $path = str_replace("_", "/", $class_name ) . ".php";
        if ( !strrpos( $class_path, "WP" ) && (file_exists( $class_path )) ) {
            //echo "<br />" . $class_path . '   ' . $class_name;
            @include $class_path;
        }
    }
    

    spl_autoload_register('autoloader'); 
    
    /**
        * URI to current active template
        */
    if (!defined('THEMEURI')) 
        define( 'THEMEURI', get_template_directory_uri()  ); 

    /**
        * URI to asset directory (js, css, images)
        */
    if (!defined('THEMEASSETS')) 
        define( 'THEMEASSETS', THEMEURI .'/core/assets' ); 

    /**
        * URI to site home 
        */
    if (!defined('HOMEURL')) 
        define( 'HOMEURL', home_url() ); 

    $filepath = dirname(__FILE__) . "/theme-configuration.php";
    if (file_exists($filepath))
        $options = json_decode( file_get_contents($filepath) );   

    
    /**
     * require all file in module 
     */
    if( is_bool( $options->autoload_module ) && $options->autoload_module ) {
        
        $lib_dir = dirname( __FILE__ ) . '/modules-active/';
        if( is_readable( $lib_dir ) ) {
            foreach( glob( $lib_dir . "*.php" , GLOB_NOSORT) as $file ) {
                if(file_exists($file)) {
                    require( $file );
                }else{                    
                    if( is_bool( $options->debug ) && $options->debug ) 
                        echo '</p>Failure access to <strong>'.$file.'</strong></p>';	
                }
            }
        }else{
            if( is_bool( $options->debug ) && $options->debug ) 
                echo '</p>Failure access to <strong>['.$lib_dir.']</strong>, directory is not readable!</p>'; 
                }
        }        
    
 /**
     * require all file in widgets 
     */
    if( is_bool( $options->autoload_widget ) && $options->autoload_widget ) {
        
        $options->widgets = array();
        $lib_dir = dirname( __FILE__ ) . '/widgets-active/';
        if( is_readable( $lib_dir ) ) {
            $files = array_values( preg_grep( '/^((?!index.php).)*$/', glob( $lib_dir . "*.php" ,  GLOB_NOSORT) ) );
            
            foreach( $files as $file ) {
                if(file_exists($file) ) {
                    require( $file );
                    
                      // per ogni file trovato valorizzo l'array
                    $options->widgets[] = preg_replace("/\-/", "_",  basename($file , ".php"));
                }else{                    
                    if( is_bool( $options->debug ) && $options->debug ) 
                        echo '</p>Failure access to <strong>'.$file.'</strong></p>';	
                }
            }
        }else{
            if( is_bool( $options->debug ) && $options->debug ) 
                echo '</p>Failure access to <strong>['.$lib_dir.']</strong>, directory is not readable!</p>'; 
                }
        }             
        
    
    /**
     * 
     * Init _m instance
     */
    $GLOBALS['_m'] = new loader( $options );
    
    
    
    
    