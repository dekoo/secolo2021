<?php

/**
 * 
 */
class MetaBox {

    const VERSION = '1.0';
    const DATE_APPROVED = '2012-06-01';

    function __construct() {
        add_action('add_meta_boxes', array(&$this, 'setupMetabox'));
        add_action('save_post', array(&$this, 'saveMetabox'));
    }

    /**
     * gestisco il salvataggio delle info dei metabox
     * @param type $post_id
     */
    function saveMetabox($post_id) {
        if (isset($_POST["post_type"]) && @$_POST['post_type'] == "page") {
           
            if (isset($_POST['page_menu'])) {
                update_post_meta($post_id, "page_menu", $_POST['page_menu']);

                
                // salvo le singole select per aver il dettaglio dei 3 elementi
                update_post_meta($post_id, "box_01", $_POST['box_01']);
                update_post_meta($post_id, "box_02", $_POST['box_02']);
                update_post_meta($post_id, "box_03", $_POST['box_03']);
                
                
                // imposto una query per prendere il contenuto dei 3 elementi (forzando l'ordinamento per post__in)
                $args = array( 
                    'post_type' => 'page', 
                    'orderby' => 'post__in',
                    'order' => 'ASC',
                    'post__in' => array( $_POST['box_01'], $_POST['box_02'],$_POST['box_03'] ) 
                    );
       
                // salvo un opzione nel db di wordpress in cui ci sono tutte le info dei 3 elementi
                add_option( "pagebottom-" . md5($post_id), get_posts( $args ) );
            }
        }
    }

    /**
     * 
     */
    function setupMetabox() {

        add_meta_box(
                'page-menu', 'Seleziona menu associato', array(&$this, 'pageMenu'), 'page', 'side', 'default'
        );

        add_meta_box(
                'page-bottom', 'Seleziona i box da visualizzare', array(&$this, 'pageBottom'), 'page', 'advanced', 'default'
        );
    }

    // gestione visualizzaione metabox pagemenu
    function pageBottom() {

        global $post;
        $custom_fields = get_post_custom($post->ID);

        $out = "<p><i>Seleziona le pagine da visualizzare nella parte inferiore di questa pagina. <br/>
            Se non è stato selezionato nulla il box non verrà visualizzato</i></p>";

        $args = array(
            'post_type' => 'page',
            'post_status' => 'publish',
            'orderby' => 'title',
            'order' => 'ASC',
        );

        $pages = new WP_Query();
        $pages->query($args);

        $out .= "<br /><label>Posizione 1</label> " . $this->comboPosts("box_01", $pages, $custom_fields["box_01"][0]);
        $out .= "<br /><label>Posizione 2</label> " . $this->comboPosts("box_02", $pages, $custom_fields["box_02"][0]);
        $out .= "<br /><label>Posizione 3</label> " . $this->comboPosts("box_03", $pages, $custom_fields["box_03"][0]);

        $out .= "<p><input style='float: right;' type='submit' value='salva' class='button-primary' >
					<br style='clear: both;' />
					</p>";

        echo $out;
    }

    // gestione visualizzaione metabox pagemenu
    function pageMenu() {

        global $post;
        $custom_fields = get_post_custom($post->ID);

        $html = "<p>Menu visualizzato</p>";

        $menus = get_terms('nav_menu');
        $html .= "<select name='page_menu'>
                    <option></option>";

        foreach ($menus as $menu) {
            $selected_attribute = "";
            if (@$custom_fields["page_menu"][0] == $menu->name)
                $selected_attribute = " selected ";

            $html .= "<option {$selected_attribute}>" . $menu->name . " </option>";
        }
        $html .= "</select>";

        echo $html;
    }

    /**
     * 
     * @global type $post
     * @param type $id
     * @param type $data
     * @param type $selected
     * @param type $class
     * @param type $return_value
     * @return type
     */
    function comboPosts($id, $data, $selected = '', $class = '', $return_value = "ID") {
        global $post;
        $temp_post = $post;

        /* carico una combo a partire da un array */
        $options = "";
        $options .= "<option value=''></option>";


        if ($data->have_posts()) : while ($data->have_posts()) : $data->the_post();

                //$options .= $link_before . "<a href='" . get_permalink( $post->ID) . "'>" . $post->post_title . "</a>" . $link_after; 
                // applico il filtro per gestione multilingua
                $title = apply_filters('the_title', $post->post_title);

                $selected_attribute = ( $post->ID == $selected ? " selected " : "" );
                $options_value = $post->ID;

                if ($return_value == "attachment_url") {
                    $options_value = wp_get_attachment_url($post->ID);
                    $selected_attribute = ( $options_value == $selected ? " selected " : "" );
                }

                if ($return_value == "permalink") {
                    $options_value = get_permalink($post->ID);
                    $selected_attribute = ( $options_value == $selected ? " selected " : "" );
                }



                $options .= "<option " . $selected_attribute . " value='" . $options_value . "'>" . $title . "</option>";



            endwhile;
        endif;

        wp_reset_query();
        $post = $temp_post;


        return "<select id='$id' name='$id' class='$class' >$options</select>";
    }

}