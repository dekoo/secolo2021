<?php

/**
 * 
 */
class CachePosts {

    private $_args;
    private $_posts;
    private $_global_query;
    private $_transient;
    private $_transient_name;
    private $_transient_expiration;
    private $_debug = false;

    function __construct($args, $transient_name = null, $transient_expiration = 7200) {

        $defaults = array(
            'set_transient' => true,
            'global_query' => false,
        );

        $args = wp_parse_args($args, $defaults);
        extract($args, EXTR_SKIP);

        if (defined(WP_DEBUG)) {
            if (WP_DEBUG == false)
                $set_transient = false;
        }

        if (is_null($this->_transient_name)) {
            // if null set md5
            $this->_transient_name = md5(serialize($this->_args));
        }


        $this->_args = $args;
        $this->_global_query = $global_query;
        $this->_transient = $set_transient;
        $this->_transient_name = $transient_name;
        $this->_transient_expiration = $transient_expiration;
    }

    public function setTransient($bool) {
        $this->_transient = $bool;
    }

    public function cleanCache() {
        delete_transient($this->_transient_name);
    }

    public function pprint($txt) {
        echo $txt;
    }

    public function get_posts() {

        if ($this->_transient) {
            $this->_posts = get_transient($this->_transient_name);
            }

        if (!isset($this->_posts)) {
            wp_reset_query();


            $the_query = new WP_Query();
            $the_query->query($this->_args);


            if ($this->_debug) {
                echo ">>>>dentro";
                var_dump($the_query);
            }

            if ($this->_global_query) {
                global $wp_query;
                $wp_query = $the_query;
            }

            $this->_posts = $the_query->posts;
            
            if ($this->_transient) 
                set_transient($this->_transient_name, $this->_posts, $this->_transient_expiration);
        }

        return $this->_posts;
    }

    public function dump() {

        $posts = $this->get_posts();
        var_dump($posts);
    }

    public function setDebug($debug = false) {
           echo ">>> imposto il debug = " . $debug;
        $this->_debug = $debug;
    }

    /*
     * Loop query result and get foreach post call get_template_part
     *
     * @param object $posts accept WP_Query->posts or get_posts() 
     * @param string $template_slug name of template 
     */

    public function load_template($template_slug, $get_post = true) {
        global $posts, $post, $wp_did_header, $wp_query, $wp_rewrite, $wpdb, $wp_version, $wp, $id, $comment, $user_ID;
        $post_temp = $post;

        $template_name = "{$template_slug}.php";
        if (file_exists(TEMPLATEPATH . '/' . $template_name)) {

            if ($get_post)
                $this->get_posts();

            foreach ($this->_posts as $post) :
                setup_postdata($post);

                require( TEMPLATEPATH . "/" . $template_name );

            endforeach;

            $post = $post_temp;
        } else {

            if (WP_DEBUG)
                echo "<pre>template not found: $template_name </pre>";
        }
    }

}

//end class