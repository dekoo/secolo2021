<?php
/**
RequirePlugins

es: new RequirePlugins( array( 'akismet/akismet.php' ) );
 
This class is inspired by HBNS_Always_Active_Plugins
http://markjaquith.wordpress.com/2012/05/15/how-i-built-have-baby-need-stuff/
 
   

 */


class RequirePlugins {
	
	private $require_plugins = array();
	private $miss_plugins = array();	
        private $found_plugins = array();		        

        /**
         * 
         * @param array $require_plugins
         */
	function __construct( $require_plugins = null ) {
            
            if (is_array( $require_plugins )) {
                
                if ( !function_exists( 'get_plugins' ) )
                    require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
                
		$this->require_plugins = $require_plugins;
                
		foreach ( $this->require_plugins as $p ) {
			
			if ( !$this->check_plugin( $p )) {
				$this->miss_plugins[] = $p;
			} else {
				$this->found_plugins[] = $p;
				add_filter( 'plugin_action_links_' . plugin_basename( $p ), array( $this, 'remove_deactivation_link' ) );
			}
                    }
		
		add_filter( 'option_active_plugins', array( $this, 'active_plugins' ) );
		add_action( 'admin_notices',  array( $this , 'user_notice' ));	
                add_action( 'plugin_action_links',  array( $this , 'disable_plugin_deletion' ));                
                                
                } else {
                    add_action( 'admin_notices',  array( $this , 'no_required' ));	
                    } 
		
	} // construct

        /**
         * 
         */
	function no_required(){

            echo "<div class='updated'>
                <p>No required plugin founds</p>
            </div>";

	}        
        
        /**
         * 
         */
	function user_notice(){

            foreach ( $this->miss_plugins as $plugin ) {
                    $plugin_array = split("/" , $plugin);

                    $link = site_url() . "/wp-admin/plugin-install.php?tab=plugin-information&plugin=" .  $plugin_array[0] . "&TB_iframe=true&width=640&height=503";			

                    echo "<div class='updated'>
                        <p><strong>$plugin is required.</strong> Please <a title='More information about $plugin_array[0]' class='thickbox' href='$link'>install now</a></p>
                    </div>";

            }
	}

        /**
         *
         * @param string $required_plugin
         * @return boolean 
         */
	function check_plugin( $required_plugin ) {


                //$plugins = get_option('active_plugins');
                $plugins = get_plugins();
                
                
                $check = false;
                foreach ($plugins as $plugin_name => $plugin_value) {
                    if ( $required_plugin == $plugin_name  ) {
                        $check = true; // Example for yes, it's active
                    }
                    
                }
                
		return $check;
		}

        /**
         *
         * @param array $actions
         * @return type 
         */
	function remove_deactivation_link( $actions ) {
		unset( $actions['deactivate'] );
		return $actions;
	}

        /**
         *
         * @param array $plugins
         * @return array 
         */
	function active_plugins( $plugins ) {
            
            foreach ( $this->found_plugins as $p ) {
                    if ( !array_search( $p, $plugins ) )
                            $plugins[] = $p;
            }
            return $plugins;
	}
        
        /**
         *
         * @param array $actions
         * @return array 
         */
        function disable_plugin_deletion( $actions ) {
                unset( $actions['delete'] );
                return $actions;
        }
        
        
} // end class



 

