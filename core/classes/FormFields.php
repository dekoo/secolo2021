<?php


/**
 * Mvd_FormFields
 * Helper Class to render html form element
 * NOTE: Designed for use with WordPress
 * @author Maurizio Pelizzone
 * @link http://maurizio.mavida.com
 * @license: GPLv2 or later
 * @License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * @version: 0.2
 * @todo:
 */

Class FormFields {

	/*
	 *
	 */
	function textarea($id, $value='', $class='' , $rows = 5 , $cols = 20, $name ) {
		
            
            
		if ( isset($_POST[$id]) )
			$value = ( $value == '' ? $_POST[$id] : $value);

                if ($name == '') 
                   $name = $id;
                
		return "<textarea id=\"$id\" name=\"$name\" class=\"$class\" rows=\"$rows\" cols=\"$cols\">$value</textarea>";
		}

	
	/*
	 *
	 */
	function input($id, $value='', $class='', $type='text' ) {
		
		if ( isset($_POST[$id]) )
			$value = ( $value == '' ? $_POST[$id] : $value);
		
		return "<input type=\"$type\" id=\"$id\" name=\"$id\" class=\"$class\" value=\"$value\">";
		}
		

	/*
	* submit input
	*/
	function submit($id, $value='', $class='' ) {
			return $this->input(  $id, $value, $class, "submit");
		}	

	/*
	* hidden short
	*/
	function hidden($id, $value='', $class='' ) {
			return $this->input(  $id, $value, $class, "hidden");
		}	



		
	/*
	 *
	 */

	function get_options( $data, $selected = '') {
		$options = "";
		foreach($data as $item) {
			$selected_attribute = "";
			if ( !is_array( $selected) ) {
				$selected_attribute = ( $item == $selected ? " selected " : "" );
				} else {
					foreach( $selected as $selected_item ){
						if ( $item == $selected_item ) {
							$selected_attribute = " selected ";
							break;
							}
						}
				
				}
				
				
			//$selected_attribute
			$options .= "<option " . $selected_attribute . " value='" . $item . "'>" . $item . "</option>";
		}
		
		return $options;
	}


	/*
	 *
	 */

	function select($id, $data, $selected = '' , $class='') {

		/* carico una combo a partire da un array */
		$options = "";
		$options .= "<option selected value=''></option>";
		
                if ( $selected == '' && isset($_POST[$id]) )
                    $selected = ( $selected == '' ? $_POST[$id] : $selected);
	
		$options .= $this->get_options( $data, $selected );
		 
		return "<select id='$id' name='$id' class='$class' >$options</select>";
		}
	

/*
         * 22.06.2012 | maurizio
	 * create checkbox button from array data
         */
	function checkbox($id, $data, $args = array() ) {

                $defaults = array(
			'selected'  => "", 
			'class'     => "", 	
                        'sep'       => " ",
                        'before'    => "",                    
                        'after'     => "",                                        

			);
	
		$args = wp_parse_args( $args, $defaults );			
		extract( $args, EXTR_SKIP );
            
            
                $radio = "";
                foreach($data as $item) {
                    
                    if ( is_array($item) ) {
                        $item_id    = (isset($item["id"])) ? $item["id"] : $this->slugerize( $id . "-" . $item);
                        $item_value = esc_attr($item["value"]);
                        $item_label = $item["label"];                        
                        
                    } else {
                        $item_id    = $this->slugerize( $id . "-" . $item);
                        $item_value = esc_attr($item);
                        $item_label = $item;
                        
                    }
                    
                    $radio .= "<input id='$item_id' class='$class' type='checkbox' name='" . $id . "[]' value='$item_value' /> $item_label $sep";
                }
		 
		return $before . $radio . $after;
		}                     
                
                
	/*
         * 13.06.2012 | maurizio
	 * create radio button from arraydata
         * data can be muldimensional array 
         */
	function radio($id, $data, $args = array() ) {

                $defaults = array(
			'selected'  => "", 
			'class'     => "", 	
                        'sep'       => " ",
                        'before'    => "",                    
                        'after'     => "", 
                        'value'     => "",
                        'name'      => ""

			);
	
		$args = wp_parse_args( $args, $defaults );			
		extract( $args, EXTR_SKIP );
            
            
                $radio = "";
                foreach($data as $item) {
                    
                    if ( is_array($item) ) {
                        $item_id    = (isset($item["id"])) ? $item["id"] : $this->slugerize( $id . "-" . $item);
                        $item_value = esc_attr($item["value"]);
                        $item_label = $item["label"];       
                        
                        
                    } else {
                        $item_id    = $this->slugerize( $id . "-" . $item);
                        $item_value = esc_attr($item);
                        $item_label = $item;
                        
                    }
                    
                    $checked = "";
                    if ( $value == $item_value )
                            $checked = " checked ";
                        
                    if ($name == "") 
                        $name = $id;
                    
                    $radio .= "<input $checked  id='$item_id' class='$class' type='radio' name='$name' value='$item_value' /> $item_label $sep";
                }
		 
		return $before . $radio . $after;
		}                
                
	/*
	 *
	 */

	function combo_terms($id, $data, $selected = '' , $class='') {
		
		/* carico una combo a partire da un array */
		$options = "";
		$options .= "<option value=''></option>";
		
		
		foreach ( $data as $term ) {
	
			//$options .= $link_before . "<a href='" . get_permalink( $post->ID) . "'>" . $post->post_title . "</a>" . $link_after; 
			
			$selected_attribute = ( $term->name  == $selected ? " selected " : "" );
			if ($selected_attribute == "") {
				$selected_attribute = ( $term->slug  == $selected ? " selected " : "" );
				}
			
	
			$options .= "<option " . $selected_attribute . " value='" . $term->name  . "'>" .  $term->name  . "</option>";
	
	
	
		}
	
	
		 
		return "<select id='$id' name='$id' class='$class' >$options</select><!-- " . $selected . " -->" ;
		}

	/*
	 *
	 */
	function combo_terms_slug($id, $data, $selected = '' , $class='') {
		
		/* carico una combo a partire da un array */
		$options = "";
		$options .= "<option value=''></option>";
		
		
		foreach ( $data as $term ) {
	
			//$options .= $link_before . "<a href='" . get_permalink( $post->ID) . "'>" . $post->post_title . "</a>" . $link_after; 
			
			$selected_attribute = ( $term->slug  == $selected ? " selected " : "" );
			
	
			$options .= "<option " . $selected_attribute . " value='" . $term->slug  . "'>" .  $term->name  . "</option>";
	
	
	
		}
	
	
		 
		return "<select id='$id' name='$id' class='$class' >$options</select><!-- " . $selected . " -->" ;
		}


	/*
	 *
	 */
	function combo_posts($id, $data, $selected = '' , $class='') {
		global $post;
		$temp_post = $post;			
	
		/* carico una combo a partire da un array */
		$options = "";
		$options .= "<option value=''></option>";
		
		
		if ($data->have_posts()) : while ($data->have_posts()) : $data->the_post();
	
			//$options .= $link_before . "<a href='" . get_permalink( $post->ID) . "'>" . $post->post_title . "</a>" . $link_after; 
			
			$selected_attribute = ( $post->ID == $selected ? " selected " : "" );
	
			// applico il filtro per gestione multilingua
			$title = apply_filters('the_title', $post->post_title);
			$options .= "<option " . $selected_attribute . " value='" . $post->ID . "'>" .  $title  . "</option>";
	
	
	
		endwhile;
		endif;
	
		wp_reset_query();
		$post = $temp_post;
	
		
		 
		return "<select id='$id' name='$id' class='$class' >$options</select>";
		}
	
                
        /*
	* support function
	*/
	function slugerize($phrase) {
		$result = strtolower($phrase);
		
		$result = preg_replace("/[^a-z0-9\s-]/", "", $result);
		$result = trim(preg_replace("/\s+/", " ", $result));
		$result = trim(substr($result, 0, 45));
		$result = preg_replace("/\s/", "-", $result);
		
		
		return $result;
	} 	                
                
	
	} // fine classe

