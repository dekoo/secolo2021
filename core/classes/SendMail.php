<?php

/**
 * Classe per l'impostazione e l'invio di mail
 * 
 */
class SendMail
{

    private $template;
    private $to;
    private $subject;
    private $body = '';
    private $headers = '';
    private $replyTo = '';

    /**
     * Impostazione parametri principali
     * 
     * @param String $to Mail del destinatario
     * @param String $subject Oggetto della mail
     * @param String $template Opzionale. Percorso relativo del template della mail
     * @param String $replyTo Opzionale. Mail a cui il destinatario risponde
     */
    public function __construct($to, $subject, $template = 'parts/email.php', $replyTo = '')
    {
        $this->to = $to;
        $this->subject = $subject;
        $this->template = $template;
        $this->replyTo = $replyTo;
    }

    /**
     * Imposta la mail del destinatario
     * 
     * @param String $to Mail del destinatario
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * Imposta l'oggetto della mail
     * 
     * @param String $subject Oggetto della mail
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * Imposta il template della mail
     * 
     * @param String $template Percorso relativo del template della mail
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * Imposta l'header della mail.
     * Aggiunge il replyTo qualora fosse già stato impostato e non fosse già presente nell'header
     * 
     * @param String|NULL $headers Opzionale. Header della mail
     */
    public function setHeaders($headers = NULL)
    {
        if (is_null($headers)):
            $headers = "MIME-Version: 1.0\r\n"
                . "Content-Type: text/html; charset=\"UTF-8\"\r\n";
        endif;
        if ($this->replyTo !== '' && strpos('Reply-To:', $headers) === FALSE):
            $headers .= "Reply-To: " . $this->replyTo;
        endif;
        $this->headers = $headers;
    }

    /**
     * Imposta la mail di risposta e rigenera l'header.
     * 
     * @param String $replyTo Mail di risposta
     */
    public function setReplyTo($replyTo)
    {
        $this->replyTo = $replyTo;
        $this->setHeaders();
    }

    /**
     * Renderizza il corpo della mail
     * 
     * @param Array|NULL $args Opzionale. Parametri utili al template della mail
     */
    public function renderBody($args = NULL)
    {
        if (locate_template($this->template)):
            if (!is_null($args) && is_array($args)):
                extract($args);
            endif;
            ob_start();
            include(locate_template($this->template));
            $this->body = ob_get_clean();
        endif;
    }

    /**
     * Invia la mail, impostando il corpo e l'header della mail qualora non lo fossero già
     * 
     * @param Array|NULL $args Opzionale. Parametri utili al template della mail
     * @param String|NULL $headers Opzionale. Header della mail
     * @return Bool True se la mail è stata inviata correttamente
     */
    public function send($args = NULL, $headers = NULL)
    {
        if ($this->body === '' || !is_null($args)):
            $this->renderBody($args);
        endif;

        if ($this->headers === '' || !is_null($headers)):
            $this->setHeaders($headers);
        endif;

        return wp_mail($this->to, $this->subject, $this->body, $this->headers);
    }
}
