<?php

/**
 * classe per la gestione delle tabelle 
 */
class TableFix {

    const VERSION = '2.0';
    

    function __construct() {


        $cpt = "custom_post_type_name";
        add_filter("manage_{$cpt}_post_columns", array(&$this, 'ColumnsHead'), 10);
        add_action("manage_{$cpt}_posts_custom_column", array(&$this, 'ColumnsContent'), 10, 2);


        // filtro per i link sotto il titolo
        //add_filter('post_row_actions', array(&$this, 'actionsRow'), 10, 2);

        // filtro per la gestione dei tendine di ricerca sopra la tabella
        //add_action('restrict_manage_posts', array($this, 'columnsFilters'));
    }

    /**
     * 
     * @param type $columns
     * @return string
     */
    function ColumnsHead($columns) {

        //var_dump($columns);
        //die();

        // svuoto tutto le colonne
        $columns = [];

        

        return $columns;
    }

    /**
     * 
     * @global type $post
     * @param type $column_name
     * @param type $post_ID
     */
    function ColumnsContent($column_name, $post_ID) {

        global $post;
        $custom_fields = get_post_custom($post_ID);

        switch ($column_name) {
            case "nomecolonna#1":
            case "nomecolonna#2":
                echo $custom_fields[$column_name][0];

                break;

            default:
                break;
        }
    }

    /**
     * 
     * @param type $actions
     * @param type $post
     * @return string
     */
    function actionsRow($actions, $post) {


        if ( "custom_post_type_name" == $post->post_type ) {


            // nascondo la modifica inline ed il visualizza
            unset($actions["view"]);
            unset($actions["inline hide-if-no-js"]);

            // aggiungo link dettaglio
            $actions['detail'] = "<a title='" . __("Detail") . " href='#'>" . __("Detail") . "</a>";
        }

        return $actions;
    }

    /**
     * 
     * @global type $typenow
     */
    function columnsFilters() {
        global $typenow;

        if ("custom_post_type_name" == $typenow) {


            /*
              $ff = new FormFields();
              $ff->setFistvalue("Tutte le categorie progetto");

              $args = array(
              'hide_empty' => false,
              'cache_domain' => "chf"
              );

              $data = get_terms("categoria-progetto", $args);
              $out = $ff->combo_terms_slug("categoria-progetto", $data, get_query_var("categoria-progetto"));
             * 
             * echo $out;
             * 
             */

            
            
        }
    }

}
