<?php

class PostType {

    /**
     *
     * @param string $typename custom type name
     * @param array $args optional
     * @param array $supports optional
     * @param array $labels  optional
     * 
     * 
     * es.
     * 
     * $cpt = new PostType();
     * $cpt->add("voti");
     * $cpt->add("voti-utente");
     * 
     */
    function add($typename, $args = null, $supports = null, $labels = null) {

        if (isset($typename)) {

            $defaults_labels = array(
                'name' => __($typename, ''),
                'singular_name' => __($typename),
                'add_new' => __('Aggiungi Nuovo', ''),
                'add_new_item' => __('Aggiungi Nuovo ' . $typename),
                'edit_item' => __('Modifica ' . $typename),
                'new_item' => __('Nuovo ' . $typename),
                'view_item' => __('Visualizza ' . $typename),
                'search_items' => __('Cerca ' . $typename),
                'not_found' => __('Nessun ' . $typename . ' Trovato'),
                'not_found_in_trash' => __('Nessun ' . $typename . ' Trovato nel Cestino'),
                'parent_item_colon' => '',
                'menu_name' => ucfirst($typename)
            );

            $labels = wp_parse_args($labels, $defaults_labels);

            $defaults_supports = array(
                'title',
                'editor',
                'author',
                'thumbnail',
                'excerpt',
                'trackbacks',
                'custom-fields',
                'comments',
                'revisions',
                'page-attributes',
                'post-formats'
            );

            if (!isset($supports))
                $supports = $defaults_supports;


            $defaults_args = array(
                'labels' => $labels,
                'supports' => $supports,
                'description' => '',
                'public' => true,
                'publicly_queryable' => true,
                'exclude_from_search' => false,
                'show_ui' => true,
                'show_in_menu' => true,
                'menu_position' => 5,
                'menu_icon' => null,
                'capability_type' => 'post',
                'hierarchical' => true,
                'taxonomies' => array(),
                'has_archive' => true,
                'rewrite' => array(
                    'slug' => '',
                    'with_front' => true,
                    'feeds' => true,
                    'pages' => true
                ),
                'query_var' => true,
                'can_export' => true,
                'show_in_nav_menus' => true,
                '_edit_link' => 'post.php?post=%d'
            );

            $args = wp_parse_args($args, $defaults_args);



            register_post_type($typename, $args);
        }
    }

// end functin
}
