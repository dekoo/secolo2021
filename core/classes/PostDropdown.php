<?php

/**
 * Class to create a custom post control
 */
class PostDropdown extends WP_Customize_Control {

    private $posts = false;

    public function __construct($manager, $id, $args = array(), $options = array()) {
        $postargs = wp_parse_args($options, array('numberposts' => '20'));
        $this->posts = get_posts($postargs);

        parent::__construct($manager, $id, $args);
    }

    /**
     * Render the content on the theme customizer page
     */
    public function render_content() {
        if (!empty($this->posts)) {
            ?>
            <label>
                <span class="customize-post-dropdown"><?php echo esc_html($this->label); ?></span>
                <select <?php $this->link(); ?> >
                    <option value="0">_ Selezionare Articolo</option>
            <?php
            foreach ($this->posts as $post) {
                printf('<option value="%s" %s>%s</option>', $post->ID, selected($this->value(), $post->ID, false), $post->post_title);
            }
            ?>
                </select>
            </label>
                    <?php
                }
            }

        }
        