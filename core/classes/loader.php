<?php

/**
 * @package _m
 * @version 0.0.8b
 */
class loader {

    private $settings;
    private $modules;

    /**
     *
     * @param type $options 
     */
    function __construct($options) {


        //Setup defaults options
        $defaults_options = array(
            'load_defaults_includes' => false,
            'autoload_libcore' => false,
            'autoload_module' => false,
            'autoload_widget' => false,
            'autoload_css' => false,
            'autoload_js' => false,
            'autoload_logo' => false,
            'login_logo_url' => HOMEURL,
            'nav_menu' => array(),
            'includes' => array(),
            'custom_type' => array(),
            'custom_taxonomies' => array(),
            'widget_areas' => array(),
            'require_plugins' => array(),
            'debug' => false
        );
        $this->settings = wp_parse_args($options, $defaults_options);


        $this->settings['theme_info'] = array(
            'version' => '0.0.8b',
            'author' => 'Mavida S.n.c.',
            'author_url' => 'http://www.mavida.com'
        );


        if (is_bool($this->settings['load_defaults_includes']) && $this->settings['load_defaults_includes'])
            $this->settings['includes'] = $this->defaultsIncludes();


        $this->_mHooks();
        $this->_mModulesSetup();
    }

    function _mHooks() {

        //Post thumbnails
        add_theme_support('post-thumbnails');

        //Register includes and load setup
        add_action('wp_enqueue_scripts', array($this, 'registerIncludes'));
        //add_action( 'admin_enqueue_scripts', array( $this, 'registerIncludes' ) );   
        //Register post types & taxonomies
        add_action('init', array($this, 'setupPostTypes'));
        add_action('init', array($this, 'setupTaxonomies'));

        //Register nav menu & widgets area
        add_action('init', array($this, 'setupNavMenu'));
        add_action('init', array($this, 'setupWidgetsArea'));

        //Enable widgets
        add_action('widgets_init', array($this, 'setupWidgets'));
    }

    //Register custom post type
    function setupPostTypes() {

        if ($this->settings['custom_type']) {
            require_once TEMPLATEPATH . "/core/register_post_type.php";

            $active_support = null;
            if (@$this->settings['custom_type_support']) {
                $active_support = $this->settings['custom_type_support'];
            }

            foreach ($this->settings['custom_type'] as $typename => $data) {
                $args = $labels = null;
                if(isset($data->args)):
                    $args = $data->args;
                endif;
                if(isset($data->labels)):
                    $labels = $data->labels;
                endif;
                setup_post_type($typename, $args, $active_support, $labels);
            }
        }
    }

    //Register custom taxonomies
    function setupTaxonomies() {

        if ($this->settings['custom_taxonomies']) {
            require TEMPLATEPATH . "/core/register_taxonomy.php";
            foreach ($this->settings['custom_taxonomies'] as $taxonomy => $data) {
                $types = $data->types;
                $args = $labels = null;
                if(isset($data->args)):
                    $args = $data->args;
                endif;
                if(isset($data->labels)):
                    $labels = $data->labels;
                endif;
                setup_theme_taxonomiy($taxonomy, $types, $args, $labels);
            }
        }
    }

    //Register NavMenu
    function setupNavMenu() {
        if ($this->settings['nav_menu']) {
            foreach ($this->settings['nav_menu'] as $navmenu) {
                register_nav_menu($navmenu[0], $navmenu[1]);
            }
        }
    }

    //Register Widgtes Areas
    function setupWidgetsArea() {

        if ($this->settings['widget_areas']) {
            foreach ($this->settings['widget_areas'] as $widgetarea) {

                $args = array(
                    'id' => $widgetarea[0],
                    'name' => $widgetarea[1],
                    'description' => $widgetarea[2],
                    'class' => '',
                    'before_widget' => '<div id="%1$s" class="widget ' . $widgetarea[3] . ' %2$s">',
                    'after_widget' => "</div>",
                    'before_title' => '<h2 class="widget-title">',
                    'after_title' => '</h2>'
                );

                if (function_exists('register_sidebar'))
                    register_sidebar($args);
            }
        }
    }

    function setupWidgets() {

        // carico dinamemicamenrte tutti i widget presenti nella cartella widgets-active
        // vedi init.php
        if ($this->settings['widgets']) {
            foreach ($this->settings["widgets"] as $widget_name) {
                register_widget($widget_name);
            }
        }
    }

    /**
     * 
     * Setup theme settings 
     * 
     * @todo da sistemare con autoload nativo php
     */
    function _mModulesSetup() {

        if ($this->settings['require_plugins']) {
            new RequirePlugins($this->settings['require_plugins']);
        }
    }

    /*
     * Init theme defaults scripts & options
     * $includes = array(
     *     'type_script' => array(
     *          'name of include script' => array( $src, $deps, $version, type of media(css)/init in footer(js), is_admin_include(true,false) ) 
     *     )
     * )
     */

    function defaultsIncludes() {

        $includes = array(
            'css' => array(
                'bootstrap-css' => array(THEMEASSETS . '/css/bootstrap.css', '', '2.0.4', '', false),
                'bootstrap-icon-large-css' => array(THEMEASSETS . '/css/bootstrap.icon-large.css', '', '1.0.0', '', false),
                'bootstrap-theme-css' => array(THEMEASSETS . '/css/bootstrap-theme.css', '', '1.0.0', '', false),
            ),
            'js' => array(
                'bootstrap-js' => array(THEMEASSETS . '/js/bootstrap.js', '', '2.0.4', true, false),
                'respond-js' => array(THEMEASSETS . '/js/respond.min.js', '', '1.1.0', true, false),
                'bootstrap-theme-js' => array(THEMEASSETS . '/js/bootstrap-theme.js', '', '1.0.0', true, false),
            )
        );

        return $includes;
    }

    /*
     * Add includes to defaults
     */

    function add_includes($arg_includes = array()) {

        if (!is_array($arg_includes) || empty($arg_includes))
            return;
        $add_include = $this->settings['includes'];


        if (array_key_exists('css', $arg_includes)) {
            foreach ($arg_includes['css'] as $name => $include) {
                $add_include['css'][$name] = $include;
            }
        }

        if (array_key_exists('js', $arg_includes)) {
            foreach ($arg_includes['js'] as $name => $include) {
                $add_include['js'][$name] = $include;
            }
        }
        $this->settings['includes'] = $add_include;
    }

    /*
     * Remove includes (theme defaults includes too!)
     */

    function remove_includes($arg_includes = array()) {
        //..
    }

    /*
     * Register stylesheets and scripts files for Frontend Site
     */

    function registerIncludes() {


        /**
         * autoload css 
         */
        if (is_bool($this->settings["autoload_css"]) && $this->settings["autoload_css"]) {

            $lib_dir = TEMPLATEPATH . '/css/';
            if (is_readable($lib_dir)) {
                foreach (glob($lib_dir . "*.css") as $file) {
                    if (file_exists($file)) {
                        $path_parts = pathinfo($file);
                        wp_enqueue_style($path_parts['filename'], THEMEURI . '/css/' . $path_parts['filename'] . '.css');
                    }
                }
            }
        }


        /**
         * autoload js 
         */
        if (is_bool($this->settings["autoload_js"]) && $this->settings["autoload_js"]) {

            $lib_dir = TEMPLATEPATH . '/js/';
            if (is_readable($lib_dir)) {
                foreach (glob($lib_dir . "*.js") as $file) {
                    if (file_exists($file)) {
                        $path_parts = pathinfo($file);
                        wp_enqueue_script($path_parts['filename'], THEMEURI . '/js/' . $path_parts['filename'] . '.js');
                    }
                }
            }
        }

        $to_enqueue = array();
        $adminArea = false;

        if ('admin_enqueue_scripts' == current_filter())
            $adminArea = true;

        if ($this->settings['includes']) {
            foreach ($this->settings['includes'] as $type => $options) {
                foreach ($options as $name => $args) {
                    if (!isset($args[4]))
                        continue;
                    if ($args[4] == $adminArea) {

                        switch ($type) {
                            case 'css':
                                wp_register_style($name, $args[0], $args[1], $args[2], $args[3]);
                                array_push($to_enqueue, array($name, $type));
                                break;

                            case 'js':
                                wp_register_script($name, $args[0], $args[1], $args[2], $args[3]);
                                array_push($to_enqueue, array($name, $type));
                                break;

                            default:break;
                        }
                    }
                }
            }

            $this->enqueueRegisteredIncludes($to_enqueue);
        }
    }

    /*
     * Enqueue theme registered includes
     */

    function enqueueRegisteredIncludes($to_enqueue = array()) {
        if (is_array($to_enqueue) && !empty($to_enqueue)) {
            foreach ($to_enqueue as $include) {
                switch ($include[1]) {
                    case 'css':
                        wp_enqueue_style($include[0]);
                        break;

                    case 'js':
                        wp_enqueue_script($include[0]);
                        break;

                    default:break;
                }
            }
        }
    }

}

/**
 *
 * Add an includes array to theme;
 * @param type $args 
 * @todo spostare in un altro file
 * add_m_includes(array(
  'js' => array(
  'example-js-name' => array( $src, $deps, $version, $in_footer, $is_admin_include ),
  )
  'css' => array(
  example-css-name' => array( $src, $deps, $version, $type_of_media, $is_admin_include ),
  ));
 */
if (!function_exists('add_m_responsive_includes')) {

    function add_m_includes($args) {
        global $_m;
        $_m->add_includes($args);
    }

}
