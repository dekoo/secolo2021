<?php

/**

BootstrapWalker Utility
 * 
 
 * Wordpress menu to Twitter Bootstrap Menu.

*/



class BootstrapWalker extends Walker_Nav_Menu {
    
    function check_menu_location( $args ) {
                
        $locations = get_nav_menu_locations(); 
        
        if( is_array( $args ) && isset( $args[ 'theme_location' ] ) ) 
            if( $locations[ $args['theme_location'] ] == 0 )
                break;
        elseif( is_object( $args ) && isset( $args->theme_location ) )
            if( $args->theme_location == 0 )
                return;
    }

    function start_lvl( &$output, $depth, $args ) {
        $indent = str_repeat( "\t", $depth );
        $output	.= "\n$indent<ul class=\"dropdown-menu\" aria-labelledby=\"link-item-".$args."\">\n";
    }

    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {  

        $this->check_menu_location( $args );
        
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $li_attributes = '';
        $class_names = '';
        $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = ($args->has_children) ? 'dropdown' : '';
        $classes[] = ($item->current || $item->current_item_ancestor) ? 'active' : '';
        $classes[] = 'menu-item-' . $item->ID;


        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
        $class_names = ' class="' . esc_attr( $class_names ) . '"';

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
        $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

        $output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';
        
        $link_id = str_replace( 'menu-item-', 'link-item-', $id );
        
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : ' title="'  . esc_attr( $item->post_title ) .'"';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $link_id )          ? $link_id : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $attributes .= ($args->has_children) 	    ? ' class="dropdown-toggle" data-toggle="dropdown"' : '';

        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= ($args->has_children) ? ' <b class="caret"></b></a>' : '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

    function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

        if ( !$element )
                return;
        
        $this->check_menu_location( $args );

        $id_field = $this->db_fields['id'];

        //display this element
        if ( is_array( $args[0] ) ) 
            $args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
        else if ( is_object( $args[0] ) ) 
            $args[0]->has_children = ! empty( $children_elements[$element->$id_field] ); 
        
        $cb_args = array_merge( array(&$output, $element, $depth), $args);
        call_user_func_array(array(&$this, 'start_el'), $cb_args);

        $id = $element->$id_field;

        // descend only when the depth is right and there are childrens for this element
        if ( ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]) ) {

                foreach( $children_elements[ $id ] as $child ){

                        if ( !isset($newlevel) ) {
                                $newlevel = true;
                                //start the child delimiter
                                $cb_args = array_merge( array(&$output, $depth, $id), $args);
                                call_user_func_array(array(&$this, 'start_lvl'), $cb_args );
                        }
                        $this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
                }
                        unset( $children_elements[ $id ] );
        }

        if ( isset($newlevel) && $newlevel ){
                //end the child delimiter
                $cb_args = array_merge( array(&$output, $depth), $args);
                call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
        }

        //end this element
        $cb_args = array_merge( array(&$output, $element, $depth), $args);
        call_user_func_array(array(&$this, 'end_el'), $cb_args);

    }
    
    function walk( $elements, $max_depth) {
        
        if ( !$elements )
            return;
        
        $args = array_slice(func_get_args(), 2);
        $this->check_menu_location( $args );
        $output = '';

        if ($max_depth < -1) //invalid parameter
                return $output;

        if (empty($elements)) //nothing to walk
                return $output;

        $id_field = $this->db_fields['id'];
        $parent_field = $this->db_fields['parent'];

        // flat display
        if ( -1 == $max_depth ) {
                $empty_array = array();
                foreach ( $elements as $e )
                        $this->display_element( $e, $empty_array, 1, 0, $args, $output );
                return $output;
        }

        /*
         * need to display in hierarchical order
         * separate elements into two buckets: top level and children elements
         * children_elements is two dimensional array, eg.
         * children_elements[10][] contains all sub-elements whose parent is 10.
         */
        $top_level_elements = array();
        $children_elements  = array();
        foreach ( $elements as $e ) {
            if( !isset( $e->$parent_field ) ) {
                if( WP_DEBUG ) _m_notice( __( 'Error: no menu items found, you must select menu in the location specified', '_m' ), 'notice' );
                return;
            }

            if ( 0 == $e->$parent_field )
                $top_level_elements[] = $e;
            else
                $children_elements[ $e->$parent_field ][] = $e;
        }
       
        /*
         * when none of the elements is top level
         * assume the first one must be root of the sub elements
         */
        if ( empty($top_level_elements) ) {

                $first = array_slice( $elements, 0, 1 );
                $root = $first[0];

                $top_level_elements = array();
                $children_elements  = array();
                foreach ( $elements as $e) {
                        if ( $root->$parent_field == $e->$parent_field )
                                $top_level_elements[] = $e;
                        else
                                $children_elements[ $e->$parent_field ][] = $e;
                }
        }

        foreach ( $top_level_elements as $e )
                $this->display_element( $e, $children_elements, $max_depth, 0, $args, $output );

        /*
         * if we are displaying all levels, and remaining children_elements is not empty,
         * then we got orphans, which should be displayed regardless
         */
        if ( ( $max_depth == 0 ) && count( $children_elements ) > 0 ) {
                $empty_array = array();
                foreach ( $children_elements as $orphans )
                        foreach( $orphans as $op )
                                $this->display_element( $op, $empty_array, 1, 0, $args, $output );
         }

         return $output;
    }

}