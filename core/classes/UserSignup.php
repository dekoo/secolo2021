<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class UserSignup {

    const VERSION = '1.0';
    const DATE_APPROVED = '2012-06-01';

    public $notification = false;

    function __construct() {
        
    }

    /**
     * 
     * @param type $entry
     * @param type $form
     */
    function recover_password($entry, $form) {

        $email = $entry[1];
        $user = get_user_by("email", trim($email));

        if (is_object($user)) {

            $password = wp_generate_password(8, false);
            wp_set_password($password, $user->ID);

            $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

            $user_login = stripslashes($user->user_login);
            $user_email = stripslashes($user->user_email);


            /*             * ** start user email *** */
            $message = "";

            $message .= file_get_contents(STYLESHEETPATH . "/mail-templates/mail-nuova-password.html");

            $message = str_replace("[nomeutente]", $user_login, $message);
            $message = str_replace("[password]", $password, $message);



            // $headers
            //$headers[] = 'Content-type: text/html';
            $headers[] = 'From: ' . get_option('blogname') . ' <' . get_option("admin_email") . '>';

            wp_mail($user_email, sprintf(__('[%s] La tua nuova password'), $blogname), $message, $headers);
        }
    }


    

   

    /*
     * registrazione utente sottoscrittore
     */

    function register_user($username, $email, $password) {


        $user = get_user_by("email", $email);
        if (!$user) {

            // password richiesta durante compilazione modulo
            //$password = wp_generate_password(8, false);

            $user_id = wp_create_user($username, $password, $email);
            update_user_meta($user_id, "signup_date", date("d m Y"));
            update_user_meta($user_id, "last_login", date("d m Y"));

            // forzo l'autenticazione con l'utente appena creato
            $this->force_login($user_id);


            
        } else {
            // utente già presente

            return false;
        }

        return $user_id;
    }

    /*
     * email utente dopo registrazione
     */

    function user_notification($username, $password, $email) {

        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

        //$message = sprintf(__('New user registration on your site %s:'), $blogname) . "\r\n\r\n";
        //$message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
        //$message .= sprintf(__('E-mail: %s'), $user_email) . "\r\n";
        // @wp_mail(get_option('admin_email'), sprintf(__('[%s] New User Registration'), $blogname), $message);


        /*         * ** start user email *** */

        $message = "";
        //$message .= sprintf(__('Username: %s'), $user_email) . "<br\>\r\n";
        //$message .= sprintf(__('Password: %s'), $user_pass) . "<br\>\r\n";


        $message .= file_get_contents(STYLESHEETPATH . "/mail-templates/mail-registrazione.html");

        $message = str_replace("[nomeutente]", $username, $message);
        $message = str_replace("[email]", $email, $message);
        $message = str_replace("[password]", $password, $message);


        // $headers
        //$headers[] = 'Content-type: text/html';
        $headers[] = 'From: ' . get_option('blogname') . ' <' . get_option("admin_email") . '>';

        wp_mail($email, sprintf(__('[%s] Credenziali di accesso'), $blogname), $message, $headers);

        //wp_die("email invita");
    }

    /**
     * 
     * @param type $user_id
     */
    function force_login($user_id) {

        wp_set_current_user($user_id);
        wp_set_auth_cookie($user_id);
    }

}

// end class
