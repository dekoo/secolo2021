<?php

/*
 * bxslider wrapper class
 */

class BxSlider {

    private $_options = "";
    private $_slides = "";
    private $_fancybox = true;

    function __construct() {
        
    }

    function setContentImage($args) {

        $images = get_posts($args);

        if ($images) {

            foreach ($images as $image) {
                $image_attributes = wp_get_attachment_image_src($image->ID, "bxslider"); // returns an array
                $link_attributes = wp_get_attachment_image_src($image->ID, "full"); // returns an array
                $this->_slides .= "<li><a rel='gallery' href='$link_attributes[0]'><img src='$image_attributes[0]'></a></li>";
            }
        }
    }

    /**
     * set custom content for bxslider and use template
     * 
     * @global type $post
     * @param type $args
     * @param type $template
     */
    function setContentTemplate( $args, $template ) {
        
        global $post;
        $post_temp = $post;
                
        $posts = get_posts($args);
        
        ob_start();
        foreach( $posts as $post ):
            setup_postdata($post);
            
            get_template_part(  $template );
        endforeach;
        $this->_slides .= ob_get_clean();
        
        $post = $post_temp;
        
    }

    
    /**
     * 
     * @param type $slide
     */
    function setSlide( $slides ) {
       
        $this->_slides .= $slides;
        
    }
    
    /*
     * custom bxslider options 
     */
    function setOptions( $options ) {
        $this->_options .= $options; 
    }

    function setCaptions() {
        $this->_options .= "captions: true, ";
    }
    
    function hidePager() {
        $this->_options .= "pager: false, ";
    }

    function hideControls() {
        $this->_options .= "controls: false, ";
    }

    /**
     * 
     * @param type $minSlides
     * @param type $maxSlides
     * @param type $slideWidth
     * @param type $slideMargin
     */
    function setCarouselMode( $minSlides = 2 , $maxSlides = 4 , $slideWidth = 200 , $slideMargin = 10) {
        $this->_options .= "minSlides: $minSlides, maxSlides: $maxSlides, slideWidth: $slideWidth, slideMargin: $slideMargin, ";
    }

    function setTickerMode( $minSlides = 2 , $maxSlides = 4 , $slideWidth = 200 , $slideMargin = 10) {
        $this->_options .= "ticker: true, speed: 6000, minSlides: $minSlides, maxSlides: $maxSlides, slideWidth: $slideWidth, slideMargin: $slideMargin, ";
        //$this->_options .= "minSlides: 2, maxSlides: 4, slideWidth: 200, slideMargin: 10, ticker: true";
    }

    function setSliderMode() {
        
    }

    
    function setFancybox( $value = true ) {
        $this->_fancybox = $value;
        
    }
    
    function renderHtml($class = 'bxslider') {

        
        $fancybox = ( $this->_fancybox ) ?  " $('.$class a').fancybox(); " : "";
        
        $output = "";
        $output .= "<ul class='$class'>" .  $this->_slides  . "</ul>";
        $output .= "<script type='text/javascript'>

        jQuery(function( $ ){
            $('.$class').bxSlider({ " . $this->_options . " responsive: true   });
            {$fancybox}
        })
    </script>";


        echo $output;
    }

}
