<?php
/**
 * Index.
 *
 */
get_header();

$current_page = $paged;
if ($current_page < 2):
    $current_page = 1;
endif;

?>
<section class="archive__body">
    <div class="grid">
		<div class="grid__item large--2-3">
			<div class="grid__box">	
				<div class="grid">	
					<div class="grid__item">
							<?php // get the current taxonomy term
							$term = get_queried_object();
							// vars
							$h1_cat = get_field('h1_categoria', $term);
							
							if ($current_page < 2):
								if (isset($h1_cat) && !empty($h1_cat)):
									echo'<h1>'.$h1_cat.'</h1>';
								else:	
									echo'<h1>';
									single_cat_title();
									echo'</h1>';
								endif;
									$catID = get_the_category();
									$cat_txt = category_description( $catID[0] );
									if (isset($cat_txt) && !empty($cat_txt)):
										echo'
										<div id="category_description">'.$cat_txt.'</div>'; 
										echo '<p id="all"> Leggi tutto</p>';
										echo '<p id="chiudi"> Chiudi</p>';
									endif;
								else:
									if (isset($h1_cat) && !empty($h1_cat)):
									echo'<span class="fintoh1">'.$h1_cat.'</span>';
									else:
									echo'<span class="fintoh1">';
									single_cat_title();
									echo'</span>';
									endif;
								endif; ?>
						</div>	
								<?php if (have_posts()):
									while (have_posts()): the_post(); ?>
										<div class="grid__item large--1-2">
											<div class="grid__box">
												<?php get_template_part('parts/listing-post', 'single-medium'); ?>
											</div>
										</div>
								<?php endwhile;
								$args_pagination = array(
									'mid_size' => 4,
									'prev_text' => '<',
									'next_text' => '>',
									//'screen_reader_text' => 'Pagina ' . $current_page . ' di ' . number_format($wp_query->max_num_pages, 0, ',', '.')
								);
								echo str_replace('h2', 'p', get_the_posts_pagination($args_pagination));
							endif; ?>
						
				
				</div>
			</div></div>
			<div class="grid__item large--1-3">
				<div class="grid__box">
					<aside>
						<?php get_sidebar('home'); ?>
					</aside>
				</div>
			</div>
		</div>
    </div>		</div>
</section>

<?php
get_footer();
