<?php $args = array(
		'post_type' => 'quotidiano',
		'post_status' => 'publish',
		'orderby' => 'date',
		'order' => 'DESC',
		'showposts' => 1
	);
	$query = new WP_Query($args);
	if ($query->have_posts()):?>
		<section class="featured-post bordered">
			<div class="inner">
				<span class="title">Edicola</span>
				<?php while ($query->have_posts()): $query->the_post(); ?>
					<article class="listing-single-download">    
						<a href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail('full'); ?>
							<div><br/><span>SCARICA</span><p></p></div>
							<h5>Edizione del <?php the_title(); ?></h5>
						</a>
					</article>  
				<?php endwhile; ?>
			</div>
		</section>
	<?php endif;
	wp_reset_query(); ?>

<section class="banner">
	<a href="https://www.italpress.com" target="_blank"><img src="/images/italpress_300x100.gif" width="300" height="100"  alt="Italpress"/> </a>
    <div>
        <div class="box-italpress">
            <span class="label">Le ultime notizie di Italpress</span>
            <?php $args = array(
    			'post_type' => 'post',
				'post_status' => 'publish',
				'showposts' =>3,
				'category_name' => 'italpress',
                'orderby'=> 'date', 
                'order' => 'DESC'
			);
			$query = new WP_Query($args);
			if ($query->have_posts()):
	    		while ($query->have_posts()): $query->the_post(); ?>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <h4 class="card-title"><strong><?php the_title(); ?></strong></h4>
                    </a>
				<?php endwhile;
			endif;
			wp_reset_query(); ?>
        </div>
    </div>
</section>
<section>
	<a href="/bilancio-2022/" >
		<img src="/images/banner-bilanci.jpg" target="_blank" width="300" height="79" alt="Bilancio d'esercizio anno 2022"/>
	</a>
</section>
<section class="featured-post bordered">
    <span class="title">I ristoranti del Secolo</span>
    <?php $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'category',
                    'field' => 'slug',
                    'terms' => 'i-ristoranti-del-secolo',
                )
            ),
            'showposts' => 2,
            'orderby' => 'date',
            'order' => 'DESC'
        );

        $query = new WP_Query($args);

        if ($query->have_posts()):
            while ($query->have_posts()): $query->the_post();?>
                <article>
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('full'); ?>
                        <h4><?php the_title(); ?></h4>
                    </a>
                </article>
                <?php
            endwhile;
        endif;

        wp_reset_query(); ?>
</section>