<aside><?php if( ! is_page( 'vetrina' ) && !is_page_template( 'templates/vetrina.php' ) ) { ?>
	<section class="banner">
		 <?php
		if ((!current_user_can('full_subscriber')) AND (!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
			if (!wp_is_mobile()):
				get_template_part('parts/banner/top-right');
			endif;
		endif;
		?>
	</section>
<?php } ?>
<section>
    <a href="/sostienici/" ><img src="/images/banner-campagna-2021_05.jpg" width="300" height="300"  alt="Sostienici"/></a>
</section>
<section>
    <a href="/le-app-del-secolo-ditalia/" ><img src="/images/banner-app-SITE.jpg" width="300" height="100" alt="Le App del Secolo d'Italia"/></a>
</section>
<section class="banner">
     <?php
	if ((!current_user_can('full_subscriber')) AND (!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
		if (!wp_is_mobile()):
			get_template_part('parts/banner/middle-right');
		endif;
	endif
    ?>
</section>
<section class="featured-post bordered inevidenza">
    <span class="title">IN EVIDENZA</span>
    <?php $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'posizione-editoriale',
                    'field' => 'slug',
                    'terms' => 'in-evidenza',
                )
            ),
            'showposts' => 4,
            'orderby' => 'date',
            'order' => 'DESC'
        );

        $query = new WP_Query($args);

        if ($query->have_posts()):
            while ($query->have_posts()): $query->the_post();

                ?>
                <article>
                    <a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail('listing-post-small'); ?>
						<h4><?php the_title(); ?></h4>
                    </a>    
                </article>
                <?php
            endwhile;
        endif;

        wp_reset_query(); ?>
</section>
<?php if( ! is_page( 'vetrina' ) && !is_page_template( 'templates/vetrina.php' ) ) { ?>
	<section class="banner">
		<a href="https://www.italpress.com" target="_blank"><img src="/images/italpress_300x100.gif" width="300" height="100" alt="Italpress"/> </a>
        <div>
        <div class="box-italpress">
            <span class="label">Le ultime notizie di Italpress</span>
            <?php $args = array(
    			'post_type' => 'post',
				'post_status' => 'publish',
				'showposts' =>3,
				'category_name' => 'italpress',
                'orderby'=> 'date', 
                'order' => 'DESC'
			);
			$query = new WP_Query($args);
			if ($query->have_posts()):
	    		while ($query->have_posts()): $query->the_post(); ?>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <h4 class="card-title"><strong><?php the_title(); ?></strong></h4>
                    </a>
				<?php endwhile;
			endif;
			wp_reset_query(); ?>
        </div>
    </div>
	</section>
<?php } ?>
<!--
<section class="subscribe-newsletter bordered">
    <h3>La Newsletter del Secolo</h3>
    <i class="fa fa-envelope-o"></i>
    <div class="message">
        <p>
            <span>Iscriviti</span><br/>Le notizie più interessanti sulla Destra selezionate per te
        </p>
    </div>
    <?php //dynamic_sidebar('newsletter'); ?>
</section>
-->
<section class="banner">
    <?php
	if ((!current_user_can('full_subscriber')) AND (!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
        if (!wp_is_mobile()):
        get_template_part('parts/banner/bottom-right');
		endif;
	endif;
    ?>
</section>
<!--
<section class="feed-rss">
    <img src="/images/adnkronos-logo.jpg"  alt="AdnKronos"/>
    <?php //dynamic_sidebar('bottom-sidebar'); ?>
</section>
-->
</aside>