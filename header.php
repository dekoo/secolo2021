<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=5.0; user-scalable=0;">
		<?php// if(is_front_page()){ ?>
			<!--<meta http-equiv="refresh" content="180;URL=https://www.secoloditalia.it?refresh_cens">-->
		<?php// } ?>
        <!--<link type="text/plain" rel="author" href="/humans.txt" /> -->

        <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
        <link rel='shortcut icon' type='image/x-icon' href='/favicon.ico' />

        <title><?php wp_title('|', true, 'right'); ?></title>
		
		<link rel="dns-prefetch preconnect" href="https://www.googletagmanager.com">
				
		<!-- preconnect-->
		
		<link rel="dns-prefetch" href="//fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin="">
		<link rel="preconnect" href="https://stats.secoloditalia.it">
		<link rel="preconnect" href="https://realtime.secoloditalia.it">
					
		<link rel="preconnect" href="https://cse.google.com">
		
		<link rel="preconnect" href="https://adv.rtbuzz.net">
		<link rel="preconnect" href="https://securepubads.g.doubleclick.net">
		<link rel="preconnect" href="https://static.takerate.com">
		
				
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

		<!-- webfont loader -->
		<script>
			WebFontConfig = {
					custom: {
							families: ['Open Sans:400,700'],
							urls: ["/stylesheets/font.css"]
							}
							};
			  (function() {
				  var wf         = document.createElement("script");
				  wf.async     = "true";
				  wf.src         = "/javascripts/webfontloader.min.js";
				  var s         = document.getElementsByTagName("script")[0];
				  s.parentNode.insertBefore(wf, s);
			  })();        
		</script>
		<!-- webfont loader -->

        <?php
        wp_head();
		if ( !dek_secolo_app() ):
			get_template_part('parts/banner/cmp_clickio');
			if ((!current_user_can('full_subscriber')) AND (!isset($_GET['noadv']))):
				get_template_part('parts/banner/head');
			endif;
			get_template_part('parts/common/ga4');
			get_template_part('parts/common/matomo');
			get_template_part('parts/common/plausible');
		endif;
				
        ?>
	</head>

    <body <?php body_class(); ?>>
        <div class="grid-container">
		<?php
			if ( !dek_secolo_app() ):
            ?>
            <header>
                <?php $options = get_fields('options'); ?>
                <div class="header__top">
                    <?php //echo date_i18n('l j F Y');
					if( ! is_page( 'vetrina' ) && !is_page_template( 'templates/vetrina.php' ) ) { ?>
						<div class="header__top-menu">
							<?php wp_nav_menu(array('menu' => 'top-menu', 'menu_class' => 'top-menu')); ?>
							<div class="header__top-menu-right-section">
								<div class="socials">
									<a class="tw" href="<?php echo $options['twitter']; ?>" target="_blank"><img src="/images/icon-socials/twitter-icon.svg" width="23" height="23"></a>
									<a class="fb" href="<?php echo $options['facebook']; ?>" target="_blank"> <img src="/images/icon-socials/facebook-icon.svg" width="23" height="23"></a>
									<a class="yt" href="<?php echo $options['youtube']; ?>" target="_blank"><img src="/images/icon-socials/youtube-icon.svg" width="23" height="23"></a>
								</div>
						
								<div class="googlesearch">
									<script>
										(function () {
											var cx = '014555323641547233307:q_jsxquqqvy';
											var gcse = document.createElement('script');
											gcse.type = 'text/javascript';
											gcse.async = true;
											gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
											var s = document.getElementsByTagName('script')[0];
											s.parentNode.insertBefore(gcse, s);
										})();
									</script>
									<gcse:searchbox-only></gcse:searchbox-only>
								</div>
							</div>
						</div>
					
                <div class="logo">
                    <a href="/" aria-label="logo-header">
                        <img src="/images/logo.svg" class="img-responsive" width="297" height="103">
                    </a>
                </div>
                <nav class="navbar" role="navigation">
                    <div class="navbar-header">  
                        <div class="mobile-logo">
                            <a href="/" aria-label="logo-header">
                                <img src="/images/logo.svg" class="img-responsive" width="297" height="103" alt="Logo Secolo d'Italia">
                            </a>
                        </div>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse-mob" aria-label="navbar-toggle">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="navbar-collapse">
                        <?php wp_nav_menu(array('menu' => 'main', 'menu_class' => 'nav navbar-nav', 'walker' => new BootstrapNavWalker())); ?>
                    </div><!--/.nav-collapse -->
                    <div class="collapse navbar-collapse navbar-collapse-mob">
                        <?php wp_nav_menu(array('menu' => 'main-mob', 'menu_class' => 'nav navbar-nav', 'walker' => new BootstrapNavWalker())); ?>
                    </div><!--/.nav-collapse -->
                </nav>
				<?php } else { ?>
					<div class="logo">
						<img src="/images/Secolo-logo-vetrina.png" class="img-responsive" style="margin:5% auto -5% auto;width: 70%;">
					</div>
				<?php } ?>
            </header>
		<?php
			endif;
			?>
            <!--<div class="banner text-center">

                <?php
				//if ( !dek_secolo_app() ):
            
				//	if ((!current_user_can('full_subscriber')) AND (!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
					
				//		if (!wp_is_mobile()):
				//			get_template_part('parts/banner/masthead');
				//		endif;
				//	endif;
				//endif;
                ?>
            </div>-->