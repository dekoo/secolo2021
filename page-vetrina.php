<?php
/**
 * Index.
 *
 */
get_header();

$fields = get_fields();

?>

<section class="home__head-top">
	<div class="grid">
		<div class="grid__item large--2-3">
			<div class="grid__box">
				<div class="grid">
					<?php $args = array(
                        'post_type' => 'post',
                            'post_status' => 'publish',
                            'showposts' => 5,
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'posizione-editoriale',
                                    'field' => 'slug',
                                    'terms' => 'homepage-primi-5',
                                ),
                            )
                        );
						$query = new WP_Query($args);
						if ($query->have_posts()):
								$first = true;
								while ($query->have_posts()): $query->the_post();
									if ($first):?>
										<div class="grid__item">
											<div class="grid__box">
												<?php get_template_part_parameterized('parts/listing-post', 'single', array('cpost' => $row['post'])); ?>
											</div>
										</div>
										<?php $first = false;
									else: ?>
										<div class="grid__item large--1-2 medium--1-2 small--1-2">
											<div class="grid__box">
												<?php get_template_part_parameterized('parts/listing-post', 'single-small', array('cpost' => $row['post'])); ?>
											</div>
										</div>
									<?php
									endif;
								endwhile;


							wp_reset_query();
						endif; ?>
				</div>
			</div>
        
    </div>
    <div class="grid__item large--1-3">
	    <div class="grid__box">
            <div class="banner">
			<?php
					if ((!current_user_can('full_subscriber')) AND (!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
						if (!wp_is_mobile()):
							get_template_part('parts/banner/top-right');
						else:
                        get_template_part('parts/banner/top-mobile');
						endif;
					endif
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="home__head-bottom">
    <div class="grid">
		<div class="grid__item">
        <?php $args = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'showposts' => 8,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'posizione-editoriale',
                        'field' => 'slug',
                        'terms' => 'homepage-secondi-8',
                    ),
                )
            );

            $query = new WP_Query($args);

            if ($query->have_posts()):
                while ($query->have_posts()): $query->the_post();

                    ?>
                    <div class="grid__item large--1-2">
                        <div class="grid__box">
                            <?php get_template_part_parameterized('parts/listing-post', 'single-medium', array('cpost' => $row['post'])); ?>
                        </div>
                    </div>
                    <?php
                endwhile;
            endif;
            wp_reset_query();

        if (wp_is_mobile()):

            ?>
            <div class="grid__item">
                <div class="grid__box">
                    <div class="banner">
                    <?php
						if ((!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
							get_template_part('parts/banner/middle-mobile');
						endif;
					?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
		</div>
	</div>
</section>
<?php
get_footer();
