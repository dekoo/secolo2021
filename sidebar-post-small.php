<section>
	<a href="/diretta-video-conferenza-fdi" >
		<img src="/images/banner-sidebar.jpg"  alt="CONFERENZA PROGRAMMATICA FDI"/>
	</a>
</section>
<section class="banner">
    <?php
	if ((!current_user_can('full_subscriber')) AND (!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
		if (!wp_is_mobile()):
			get_template_part('parts/banner/top-right');
		endif;
	endif;
    ?>
</section>
<section>
     <a href="/emergenza-coronavirus" >
		<img src="/images/banner-emergenza-coronavirus.jpg"  alt="Emergenza Coronavirus"/>
	</a>
</section>
<section class="featured-post bordered">
    <span class="title">IN EVIDENZA</span>
    <?php $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'posizione-editoriale',
                    'field' => 'slug',
                    'terms' => 'in-evidenza',
                )
            ),
            'showposts' => 2,
            'orderby' => 'date',
            'order' => 'DESC'
        );

        $query = new WP_Query($args);

        if ($query->have_posts()):
            while ($query->have_posts()): $query->the_post();

                ?>
                <article>
                    <div class="grid">
                        <div class="grid__item large--1-3 medium--1-3">
                            <div class="grid__box">
                                <a href="<?php the_permalink(); ?>">
                                    <?php the_post_thumbnail('listing-post-square'); ?>
                                </a>
                            </div>
                        </div>
                        <div class="grid__item large--2-3 medium--2-3">
                            <div class="grid__box">
                                <a href="<?php the_permalink(); ?>">
                                    <h4><?php the_title(); ?></h4>
                                </a>
                            </div>
                        </div>
                    </div>
                </article>
                <?php
            endwhile;
        endif;

        wp_reset_query(); ?>
</section>
<section class="banner">
    <?php
	if ((!current_user_can('full_subscriber')) AND (!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
		if (!wp_is_mobile()):
			get_template_part('parts/banner/bottom-right');
		endif;
	endif;	
    ?>
</section>