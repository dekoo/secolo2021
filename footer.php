</div>

<?php
$SingletonSession = SingletonSession::getInstance();
$fields = $SingletonSession->getTheme_fields();
$options = get_fields('options');

if ( !dek_secolo_app() ):
	?>
	<footer>
		<div class="grid-container">
			<div class="grid">
				<div class="grid__item">
					<div class="grid__box">
						<a class="logo" href="/"><img src="/images/white-logo.svg" width="202" height="32" alt="Logo Secolo d'Italia"></a>
						<?php wp_nav_menu(array('menu' => 'footer-menu', 'menu_class' => 'nav navbar-nav', 'walker' => new BootstrapNavWalker())); ?>
						<hr>
					</div>
				</div>
				<div class="grid__item large--1-6">
					<div class="grid__box">
						<?php wp_nav_menu(array('menu' => 'footer-menu-bottom', 'menu_class' => 'footer-menu-bottom')); ?>
						<a href="#" style ="color:#fff !important;" onclick="if(window.__lxG__consent__ !== undefined) {window.__lxG__consent__.showConsent()} else {alert('This function only for users from European Economic Area (EEA)')}; return false">Modifica preferenze Privacy</a>
					</div>
				</div>
				<div class="grid__item large--4-6">
					<div class="grid__box footer-text">
						<?php echo $fields['footer_text']; ?>
					</div>
				</div>
				<div class="grid__item large--1-6">
					<div class="grid__box">
						Seguici su:
						<div class="socials">
							<a class="tw" href="<?php echo $options['twitter']; ?>" target="_blank"><img src="/images/icon-socials/twitter-icon.svg" width="23" height="23" alt="icona twitter"></a>
                            <a class="fb" href="<?php echo $options['facebook']; ?>" target="_blank"> <img src="/images/icon-socials/facebook-icon.svg" width="23" height="23" alt="icona facebook"></a>
                            <a class="yt" href="<?php echo $options['youtube']; ?>" target="_blank"><img src="/images/icon-socials/youtube-icon.svg" width="23" height="23" alt="icona youtube"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<?php
		wp_footer();
		if ((!current_user_can('full_subscriber')) AND (!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
			if(wp_is_mobile()):
				get_template_part('parts/banner/anchor-sticky');
			endif;
		endif;

endif;
?>

<!-- 
<script src="/ads.js" type="text/javascript"></script>
<script type="text/javascript">

if(document.getElementById('tIGcLRgzlhpD')){
  tIGcLRgzlhpD='No';
} else {
  tIGcLRgzlhpD='Yes';
}

if(typeof ga !=='undefined'){
  ga('send','event','Blocking Ads',tIGcLRgzlhpD,{'nonInteraction':1});
} else if(typeof _gaq !=='undefined'){
  _gaq.push(['_trackEvent','Blocking Ads',tIGcLRgzlhpD,undefined,undefined,true]);
}

</script>
-->

<!-- Begin comScore Tag -->
<script>

async function checkconsent(){

	let promise = new Promise((resolve, reject) => {

        __tcfapi('getTCData', 2, function(data,success)
		{
	
			if (data['eventStatus'] !== 'cmpuishown')
			{
				if(data['vendor']['consents']['77'] === true) {
					 resolve(1);
					} 
					else {
					 resolve(0);
					}
			}

			resolve(-1);

		},[77]);
     
     });

    let result = await promise;

    return result;
	
}

var _comscore = _comscore || [];
checkconsent().then(cs_ucfr => { 
    console.log("my_cs_ucfr " + cs_ucfr);
	if(cs_ucfr >= 0) {cs_ucfr = cs_ucfr;}else {cs_ucfr='';}
	  _comscore.push({ c1: "2", c2: "30439819", cs_ucfr: cs_ucfr});
	  (function() {
		var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
		s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
		el.parentNode.insertBefore(s, el);
	  })();
});

</script>

<noscript>
  <img src="https://sb.scorecardresearch.com/p?c1=2&c2=30439819&cs_ucfr=1&cv=2.0&cj=1" />
</noscript>
<!-- End comScore Tag -->



<!--
<?php $author = get_user_by('id', $post->post_author);
		$autore = $author->user_login;
?>
<script>
var isAdBlockEnabled;

  (function AdBlockEnabled() {
    var ad = document.createElement('ins');
    ad.className = 'AdSense';
    ad.style.display = 'block';
    ad.style.position = 'absolute';
    ad.style.top = '-1px';
    ad.style.height = '1px';
    document.body.appendChild(ad);
    isAdBlockEnabled = !ad.clientHeight;
    document.body.removeChild(ad);
    return isAdBlockEnabled;
  })()
</script>
<script>


  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-11230173-1', 'auto');
  <?php if (is_single())
	{	
	?>
		ga('set', {'dimension1': '<?php echo $autore; ?>', 'dimension2': isAdBlockEnabled});
		
  <?php 
	}
	else{	
	?>
		ga('set', 'dimension2', isAdBlockEnabled);
	<?php
	}
	?>
  ga('set', 'anonymizeIp', true);
  ga('send', 'pageview');

</script>
-->

</body>
</html>
