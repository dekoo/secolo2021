<?php
$loop = filter_input(INPUT_GET, 'loop');
if (!empty($loop)):

    if (have_posts()):
        while (have_posts()): the_post();
            get_template_part_parameterized('parts/single', '', array('sidebar_name' => 'post', 'next_post' => true));
        endwhile;
    endif;
    die();
endif;

/**
 * Index.
 *
 */
get_header();

if (have_posts()):
    while (have_posts()): the_post();

        ?>

        <section class="post__body">
            <div class="grid">
                <?php
                get_template_part_parameterized('parts/single-inbreve', '', array('sidebar_name' => 'post'));
				if (wp_is_mobile()):

                    ?>
                    <div class="grid__item">
                        <div class="grid__box">
                            <?php
                            if (!isset($sidebar_name)):
                                $sidebar_name = 'post-small';
                            endif;
                            get_sidebar('post');

                            ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </section>

        <?php
    endwhile;
endif;

get_footer();
