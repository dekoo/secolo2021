<?php
/**
 * Index.
 *
 */
get_header();

$fields = get_fields();
?>
<section class="home__head-top">
	<div class="grid">
		<div class="grid__item large--2-3">
			<div class="grid__box">
				<div class="grid">
					<?php $args = array(
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'showposts' => 5,
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'posizione-editoriale',
                                    'field' => 'slug',
                                    'terms' => 'homepage-primi-5',
                                ),
                            )
                        );
						$query = new WP_Query($args);

							if ($query->have_posts()):
								$first = true;
								while ($query->have_posts()): $query->the_post();
									if ($first):

										?>
										<div class="grid__item">
											<div class="grid__box">
												<?php get_template_part_parameterized('parts/listing-post', 'single'); ?>
											</div>
											<?php
											if (wp_is_mobile()): ?>
												<div class="grid__item">
													<div class="grid__box">
														<div class="banner">
														<?php
															if ((!current_user_can('full_subscriber'))AND(!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
																get_template_part('parts/banner/top-mobile');
															endif;
														?>
														</div>
													</div>
												</div>
												<?php endif; ?>
										</div>
										<?php
										$first = false;
									else:

										?>
										<div class="grid__item large--1-2 medium--1-2 small--1-2">
											<div class="grid__box">
												<?php get_template_part_parameterized('parts/listing-post', 'single-medium'); ?>
											</div>
										</div>
										
									<?php
									endif;
								endwhile;
							endif;

							wp_reset_query(); 
							if (wp_is_mobile()): ?>
							<div class="grid__item">
								<div class="grid__box">
									<div class="banner">
									<?php
										if ((!current_user_can('full_subscriber'))AND(!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
											get_template_part('parts/banner/interscroller-mobile');
										endif;
									?>
									</div>
								</div>
							</div>
							<?php endif; ?>
				</div>
			</div>
        
		</div>
		
		<div class="grid__item large--1-3">
			<div class="grid__box">
				<div class="banner top-right-adv">
					<?php if ((!current_user_can('full_subscriber')) AND (!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
						if (!wp_is_mobile()):
							get_template_part('parts/banner/top-right');
						else:
						get_template_part('parts/banner/top-mobile');
						endif;
					endif	?>
				</div>
			</div>
		</div>
    </div>
</section>
<section class="home__head-bottom">
    <div class="grid">
		<div class="grid__item">
			<?php $args = array(
					'post_type' => 'post',
					'post_status' => 'publish',
					'showposts' => 8,
					'tax_query' => array(
						array(
							'taxonomy' => 'posizione-editoriale',
							'field' => 'slug',
							'terms' => 'homepage-secondi-8',
						),
					)
				);

				$query = new WP_Query($args);

				if ($query->have_posts()):
					while ($query->have_posts()): $query->the_post();

						?>
						<div class="grid__item large--1-2">
							<div class="grid__box">
								<?php get_template_part_parameterized('parts/listing-post', 'single-medium'); ?>
							</div>
						</div>
						<?php
					endwhile;
				endif;

				wp_reset_query();
				
			if (wp_is_mobile()): ?>
				<div class="grid__item">
					<div class="grid__box">
						<div class="banner">
						<?php
							if ((!current_user_can('full_subscriber'))AND(!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
								get_template_part('parts/banner/middle-mobile');
							endif;
						?>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>
<section class="home__body">
    <div class="grid">
		<div class="grid__item">
			<div class="grid__item large--2-3">
				<div class="grid__box">
					<div class="grid">
						<?php $args = array(
								'post_type' => 'post',
								'post_status' => 'publish',
								'showposts' =>12,
								'tax_query' => array(
									array(
										'taxonomy' => 'posizione-editoriale',
										'field' => 'slug',
										'terms' => 'homepage-secondari',
									)
								)
							);

							$query = new WP_Query($args);

							if ($query->have_posts()):
								
								while ($query->have_posts()): $query->the_post(); ?>
											<div class="grid__item large--1-2 medium--1-2 small--1-2">
												<div class="grid__box">
													<?php get_template_part_parameterized('parts/listing-post', 'single-medium', array('bordered' => true)); ?>
												</div>
											</div>
									
								<?php endwhile;
							endif;

							wp_reset_query(); 
							if (wp_is_mobile()): ?>
							<div class="grid__item">
								<div class="grid__box">
									<div class="banner">
									<?php
										if ((!current_user_can('full_subscriber'))AND(!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
											get_template_part('parts/banner/bottom-mobile');
										endif;
									?>
									</div>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="grid__item large--1-3">
				<div class="grid__box">
					<section class="home__sidebar">
						<aside>
							<?php get_sidebar('home'); ?>
						</aside>
					</section>
				</div>
			</div>
		</div>
</section>
<section class="home__body__cat">	
	<span class="title-section">politica</span>
	<div class="grid">
		<div class="grid__item">		
			<?php $args = array(
					'post_type' => 'post',
					'post_status' => 'publish',
					'showposts' => 4,
					'category_name' => 'politica'
				);

				$query = new WP_Query($args);

				if ($query->have_posts()):
					while ($query->have_posts()): $query->the_post();

						?>
						<div class="grid__item large--1-4">
							<div class="grid__box">
								<?php get_template_part_parameterized('parts/listing-post', 'single-small'); ?>
							</div>
						</div>
						<?php
					endwhile;
				endif;

				wp_reset_query(); ?>
		</div>
	</div>
</section>
<section class="home__body__two">
	<div class="grid">
		<div class="grid__item">
			<div class="grid__item large--2-3">
				<div class="grid__box">
					<div class="grid">
						<?php $args = array(
								'post_type' => 'post',
								'post_status' => 'publish',
								'offset' => 12,
								'showposts' =>10,
								'tax_query' => array(
									array(
										'taxonomy' => 'posizione-editoriale',
										'field' => 'slug',
										'terms' => 'homepage-secondari',
									)
								)
							);

							$query = new WP_Query($args);

							if ($query->have_posts()):
								
								while ($query->have_posts()): $query->the_post(); ?>
											<div class="grid__item large--1-2 medium--1-2 small--1-2">
												<div class="grid__box">
													<?php get_template_part_parameterized('parts/listing-post', 'single-medium', array('bordered' => true)); ?>
												</div>
											</div>
									
								<?php endwhile;
							endif;

							wp_reset_query(); ?>
					</div>
				</div>
			</div>
			<div class="grid__item large--1-3">
				<div class="grid__box">
					<section class="home__sidebar">
						<aside>
							<?php get_sidebar('home-two'); ?>
						</aside>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_template_part_parameterized('parts/listing-post', 'adnkronos', array('bordered' => true)); ?>



<section class="home__body__tree">
	<div class="grid">
		<div class="grid__item">
			<?php $args = array(
					'post_type' => 'post',
					'post_status' => 'publish',
					'offset' =>22,
					'showposts' =>9,
					'tax_query' => array(
						array(
							'taxonomy' => 'posizione-editoriale',
							'field' => 'slug',
							'terms' => 'homepage-secondari',
						)
					)
				);
				$query = new WP_Query($args);
				if ($query->have_posts()):
					while ($query->have_posts()): $query->the_post(); ?>
						<div class="grid__item large--1-3 medium--1-2 small--1-1">
							<div class="grid__box">
								<?php get_template_part_parameterized('parts/listing-post', 'single-medium', array('bordered' => true)); ?>
							</div>
						</div>
					<?php endwhile;
				endif;
				wp_reset_query(); ?>
		</div>
	</div>
</section>
<section class="home__body__cat">
	<span class="title-section">cronaca</span>
	<div class="grid">
		<div class="grid__item">		
			<?php $args = array(
					'post_type' => 'post',
					'post_status' => 'publish',
					'showposts' => 4,
					'category_name' => 'cronaca'
				);

				$query = new WP_Query($args);

				if ($query->have_posts()):
					while ($query->have_posts()): $query->the_post(); ?>
						<div class="grid__item large--1-4 medium--1-3 small--1-2">
							<div class="grid__box">
								<?php get_template_part_parameterized('parts/listing-post', 'single-small'); ?>
							</div>
						</div>
						<?php
					endwhile;
				endif;

				wp_reset_query(); ?>
		</div>
	</div>
</section>
<?php
get_footer();
