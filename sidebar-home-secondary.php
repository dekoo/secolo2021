<section class="featured-post bordered">
    <span class="title">IN EVIDENZA</span>
    <?php $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'posizione-editoriale',
                    'field' => 'slug',
                    'terms' => 'in-evidenza',
                ),
            ),
            'showposts' => 6,
            'orderby' => 'date',
            'order' => 'DESC'
        );

        $query = new WP_Query($args);

        if ($query->have_posts()):
            while ($query->have_posts()): $query->the_post();

                ?>
                <article>
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('listing-post-small'); ?>
                        <h4><?php the_title(); ?></h4>
                    </a>
                </article>
                <?php
            endwhile;
        endif;

        wp_reset_query(); ?>
</section>
<section class="featured-post bordered">
    <span class="title">Idee a destra</span>
    <?php $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'category',
                    'field' => 'slug',
                    'terms' => 'idee-a-destra',
                )
            ),
            'showposts' => 4,
            'orderby' => 'date',
            'order' => 'DESC'
        );

        $query = new WP_Query($args);

        if ($query->have_posts()):
            while ($query->have_posts()): $query->the_post();

                ?>
                <article>
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('listing-post-small'); ?>
                        <h4><?php the_title(); ?></h4>
                    </a>
                </article>
                <?php
            endwhile;
        endif;

        wp_reset_query(); ?>
</section>