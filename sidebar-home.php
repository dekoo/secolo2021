<!--
<div class="goto-newsletter">
    <h3>Resta sempre aggiornato</h3>
    <i class="fa fa-bell"></i>
    <div>
        <p>
            <a href="https://notifiche.secoloditalia.it/"><strong>Iscriviti</strong>,<br/>ti avviseremo quando ci sono <strong>notizie importanti</strong> da non perdere</a>
        </p>
    </div>
</div>
-->
<?php if( ! is_page( 'vetrina' ) && !is_page_template( 'templates/vetrina.php' ) ) { ?>
<section class="banner">
    <?php
	if ((!current_user_can('full_subscriber')) AND (!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
    
	if (!wp_is_mobile()):
        get_template_part('parts/banner/middle-right');
    endif;

	endif;
    ?>
</section>
<?php } ?>

<?php
	if (is_page( 'emergenza-coronavirus' ) ){
         //nuovo contenuto
		?> 
		<section style="border:solid 1px #01467c">	
			<p style="
				background: url(/images/banner-coronavirus.jpg) no-repeat top right, #01467c;
				height:70px; color:#FFF; padding: 18px 90px 10px 20px;
				color: #FFF;
				display: block;
				font-weight: bold;
				font-size: 17px;
				line-height: 17px;
				text-transform: uppercase;
			">Donazioni emergenza covid-19</p>
			<article>
				<figure style="padding:0 15px 0 15px;">
					<a href="/2020/03/dalla-fondazione-alleanza-nazionale-prima-tranche-di-500mila-euro-per-lospedale-di-bergamo/">
						<img src="/images/bergamo.jpg">
					</a>
				</figure>
				<h4>
					<a href="/2020/03/dalla-fondazione-alleanza-nazionale-prima-tranche-di-500mila-euro-per-lospedale-di-bergamo/" style="color: #01467c; padding:0 15px 10px 15px; display:block; border-bottom:solid 1px #eee">Dalla Fondazione Alleanza nazionale prima tranche di 500mila euro per l’ospedale di Bergamo</a>
				</h4>
			</article>
			<article>
				<figure style="padding:0 15px 0 15px;">
					<a href="/2020/03/meloni-dai-parlamentari-di-fdi-lo-stipendio-per-la-guerra-al-coronavirus/">
						<img src="/images/Meloni-Locandina.jpg">
					</a>
				</figure>
				<h4>
					<a href="/2020/03/meloni-dai-parlamentari-di-fdi-lo-stipendio-per-la-guerra-al-coronavirus/" style="color: #01467c; padding:0 15px 10px 15px; display:block;">Meloni: «Dai parlamentari di Fdi lo stipendio per la guerra al coronavirus»</a>
				</h4>
			</article>
		</section>
		<section class="banner">
			<?php
			if ((!current_user_can('full_subscriber')) AND (!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
			if (!wp_is_mobile()):
				get_template_part('parts/banner/bottom-right');
			endif;
			endif;
			?>
		</section>
		<section class="banner">
			<a href="https://www.italpress.com" target="_blank"><img src="/images/italpress_300x100.gif" width="300" height="100"  alt="Italpress"/> </a>
		</section>
		<?php
        }
	else{ ?>
	<section>
		<a href="/sostienici/" ><img src="/images/banner-campagna-2021_05.jpg" width="300" height="300" alt="Sostienici"/></a>
	</section>
	<section>
		<a href="/le-app-del-secolo-ditalia/" ><img src="/images/banner-app-SITE.jpg"  width="300" height="100" alt="Le App del Secolo d'Italia"/></a>
	</section>
<!--
<section>
     <a href="category/ventennale-tatarella/" ><img src="/images/ventennale-tatarella.jpg"  alt="Ventennale della morte di Giuseppe Tatarella"/></a><br/><br/><br/>
</section>
-->
		<section class="banner">
			<?php
			if ((!current_user_can('full_subscriber')) AND (!isset($_GET['noadv'])) AND ( !has_tag( '_noadv_' ) )) :
			if (!wp_is_mobile()):
				get_template_part('parts/banner/bottom-right');
			endif;
			endif;
			?>
		</section>
		
		<!--<section class="banner">
			<a href="https://www.italpress.com" target="_blank"><img src="/images/italpress_300x100.gif"  alt="Italpress"/> </a>
		</section>-->
		
		<!--
		<section class="featured-post bordered inbreve-block-sidebar">
			<h3>News dalla politica</h3>													
			    <?php $args = array(
							'post_type' => 'inbreve',
							'post_status' => 'publish',
							'showposts' => 4,
							'orderby' => 'date',
							'order' => 'DESC'
						);

						$query = new WP_Query($args);

						if ($query->have_posts()):
							while ($query->have_posts()): $query->the_post();
								?>
									<article>
										<a href="<?php the_permalink(); ?>">
											<?php the_title(); ?>
										</a>
									</article>
								<?php
							endwhile;
						endif;

						wp_reset_query(); ?>
		</section>
-->
<!--
<section class="subscribe-newsletter bordered">
    <h3>La Newsletter del Secolo</h3>
    <i class="fa fa-envelope-o"></i>
    <div class="message">
        <p>
            <span>Iscriviti</span><br/>Le notizie più interessanti sulla Destra selezionate per te
        </p>
    </div>
    <?php // dynamic_sidebar('newsletter'); ?>
</section>
-->

<!--<section class="featured-post bordered">
    <h3>I ristoranti del Secolo</h3>
    <?php $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'category',
                    'field' => 'slug',
                    'terms' => 'i-ristoranti-del-secolo',
                )
            ),
            'showposts' => 2,
            'orderby' => 'date',
            'order' => 'DESC'
        );

        $query = new WP_Query($args);

        if ($query->have_posts()):
            while ($query->have_posts()): $query->the_post();

                ?>
                <article>
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('full'); ?>
                        <h4><?php the_title(); ?></h4>
                    </a>
                </article>
                <?php
            endwhile;
        endif;

        wp_reset_query(); ?>
</section>-->
<?php
     }

?>
<!--<section class="featured-post bordered">
    <h3>Edicola</h3>
    <?php $args = array(
            'post_type' => 'quotidiano',
            'post_status' => 'publish',
            'orderby' => 'date',
            'order' => 'DESC',
            'showposts' => 1
        );

        $query = new WP_Query($args);
        if ($query->have_posts()):
            while ($query->have_posts()): $query->the_post();

                ?>
                <article class="listing-single-download">    
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('full'); ?>
                        <div><br/><span>SCARICA</span><p></p></div>
                        <h5>Edizione del <?php the_title(); ?></h5>
                    </a>
                </article>  
                <?php
            endwhile;
        endif;

        wp_reset_query(); ?>
</section>-->



<!--<section class="feed-rss">
    <img src="/images/adnkronos-logo.jpg"  alt="AdnKronos"/>
    <?php // dynamic_sidebar('bottom-sidebar'); ?>
</section>-->
